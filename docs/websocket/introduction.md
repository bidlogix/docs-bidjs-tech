---
id: introduction
title: Websockets
sidebar_label: Introduction
slug: /websocket
---

## What is the purpose?

We provide a Websocket API to provide a real-time stream of events that occur on an auction. This replaces our previous long-poll implementation, and can handle 40x the number of connected users!

This websocket API is also exposed for consumption by custom or advanced implementations. Most standard clients will not require any custom usage of this API.

If you're intending to implement custom functionality around the Websocket API, we recommend that you have basic understanding of Websocket APIs. For this purpose, we recommend reading up on the following guides:

* [Mozilla.org's "Websocket API Reference for Browsers"](https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API)
* [Mozilla.org's "Writing Websocket Client Applications"](https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API/Writing_WebSocket_client_applications)

## Connecting to the API

The way that you connect to a Websocket API will depend on the platform you are using.

All of the browsers supported by BidJS currently have Websocket clients built-in, so you can use [native Javascript to connect](https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API).

For testing purposes, you can also use a command line tool such as [wscat](https://github.com/websockets/wscat), or the web tool [echo](https://www.websocket.org/echo.html).

Further documentation will be written in Javascript, but almost any language or platform can do this, in appropriate syntax.

### Connection Example

A Websocket is a connection that remains open whilst information is passed back and forth. Before sending any messages, that connection must be opened.

The Websocket Domain URLs will vary depending on the region of your main BidJS deployment.

The format is `wss://broadcast.<<REGION>>.bidjs.com` where `<<REGION>>` is the region of your BidJS deployment, e.g. `eu-west-2`, `ap-southeast-2` etc.

The following code will establish a connection to the websocket API.

``` javascript
const socket = new Websocket(wss://{YOUR_WEBSOCKET_DOMAIN})
```

Please note that a connection can only stay open for 10 minutes without any activity. To keep a connection open longer than this, please send the `Ping` message (as described in the [messages](./messages.md) documentation) once within each 10 minute period, if there is no other activity.

After connecting, you can set up listeners for the different types of event that happen on the connection. e.g.:

``` javascript
socket.addEventListener('open', function(event) {
    // do something when the connection is established
})

socket.addEventListener('close', function(event) {
    // do something when the connection is closed
    // event format: { code: INT, reason: STRING }
})

socket.addEventListener('message', function(event) {
    // do something when a message is received
    // for format, see messages section below
})
```

Before any messages are received, you'll need to subscribe to either one Auction's or multiple Auctions' messages

To subscribe to one Auction you must send a message with the auction's UUID, as follows.

``` javascript
socket.send(
    JSON.stringify(
        {
            action: "SubscribeToAuction",
            data: ["6567d5e7-78a3-11ea-b499-0606952ea652"]
        }
    )
)
```

To subscribe to multiple Auctions you must send a message with an Array of Auction UUIDs, as follows.

``` javascript
socket.send(
    JSON.stringify(
        {
            action: "SubscribeToAuction",
            data: ["6567d5e7-78a3-11ea-b499-0606952ea652", "12822fa-3215-6548-8asf-80517440f967"]
        }
    )
)
```

You have now connected and any actions which require broadcasting to customers will now be send as messages, and picked up by your `message` listener.

For further information including details about backwards compatibility, we recommend reading the [messages](./messages.md) documentation.

## Getting initial state

Whilst the events are useful, they are not so useful without knowing the initial state of the auction on page load.

Fortunately, we have created a new endpoint which will enable you to get this information.

These are at the following URLs:

* `https://<<APPLICATION_URL>>/auction-007/api/v2/auctions/<<AUCTION_UUID>>`

In this context, `<<APPLICATION_URL>>` will look something like `hove.eu-west-2.bidjs.com`, and will be included with your initial bidjs configuration. Please see our [installation instructions](../developer/gettingStarted/installation.md) for more information on the `<<APPLICATION_URL>>`.

For more information about the data returned by this request, please see the [models](./models.md) documentation.

### Required Headers

Please note that to return the correct information, you should also send the following header with your requests:

```bash
x-forwarded-client-id: <<CLIENT_ID>>
```

Where `<<CLIENT_ID>>` is the id supplied to you with your bidjs configuration. E.g. `client1`
