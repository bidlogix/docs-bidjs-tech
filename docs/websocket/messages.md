---
id: messages
title: Messages
sidebar_label: Messages
---

This document outlines the different messages the Websocket API sends & receives.

The majority of messages are currently sent from the server to the client (this is referred to as a [Subscribed messages](#subscriptions-server-to-client)).

There are also a small number of messages sent from the client to the server ([Published messages](#published-client-to-server)).

### Message Format
All messages use the following format:
``` javascript
{
    "action": STRING,
    "data": STRING|OBJECT
}
```

Where a particular model is defined (e.g. `SALE_STATUS_MODEL`), this model is defined in our [models](./models.md) document.

## Published (Client to Server)
Published messages are messages sent from the client to the server

### SubscribeToAuction
Subscribes the client to subsequent messages relating to an auction. Currently single or multiple auctions can be subscribed to at the same time, per websocket connection.
``` javascript
{
    "action": "SubscribeToAuction",
    "data": ["abc-123", "def-456"] // this is the UUIDs of the auctions. You can specify here either one or multiple UUIDs of the auctions
}
```

#### Response
``` javascript
{
    "action": "AUCTION_SUBSCRIBED",
    "data": "abc-123" // this is the UUID of the auction
}

{
    "action": "AUCTION_SUBSCRIBED",
    "data": "def-456" // this is the UUID of the auction
}
```

#### Backwards compatibility
Currently we still support previous SubscribeToAuction message that can only have one auction UUID at the same time, per websocket connection.

``` javascript
{
    "action": "SubscribeToAuction",
    "data": "abc-123" // this is the UUIDs of the auction
}
```

#### Response
``` javascript
{
    "action": "AUCTION_SUBSCRIBED",
    "data": "abc-123" // this is the UUID of the auction
}
```  

### Ping
This is effectively an empty message that you can send to keep a connection open beyond the 10 minute idle timeout period.

Please note, if messages are being sent from the server to the client, the connection will not count as "idle", so will not terminate. You only need to send this when no other messages are being sent.

``` javascript
{
    "action": "Ping"
}
```

#### Response
``` javascript
{
    "action": "PONG"
}
```

### WebcastBidHover
Signals to the clerk that the user is (or is no longer) hovering over the bid button. This displays on the webcast clerking screen.

``` javascript
{
    "action": "WebcastBidHoverOn", // or WebcastBidHoverOff
    "auctionUuid": STRING,
    "data": {
        "userUuid": STRING // this is the UUID of the user
}
```

## Subscriptions (Server to Client)
Subscribed messages are messages sent from the server to the client.
These are either sent to everyone subscribed to the Auction, or to individual connections, depending on the context.

No authentication is required, and there is no difference between payloads for authenticated and unauthenticated users, as there is no Personally Identifiable Information transmitted.

Please see the [models](models.md) documentation for further information on the data being broadcast.

Note that a `?` beside a property means that it is optional, and may or may not be included depending on the context of the action.

### BID_PLACED / BID_CANCELLED / BID_REINSTATED
Informs of a change in the bid, or a new bid for a listing

``` javascript
{
    "action": "BID_PLACED" // or other, as above
    "data": {
        "auctionUuid": STRING,
        "bid": BID_MODEL,
        "sale": SALE_MODEL,
        "saleStatus": SALE_STATUS_MODEL
    }
}
```

### REGISTRANT_UPDATED
Informs of a change in the registration state of a user for the auction

``` javascript
{
    "action": "REGISTRANT_UPDATED"
    "data": {
        "auctionUuid": STRING,
        "registrant": REGISTRANT_MODEL
    }
}
```

### SALE_ADDED / SALE_WITHDRAWN
Informs of either a new listing being added to the Auction, or a listing being Removed from the Auction.

``` javascript
{
    "action": "SALE_ADDED", // or SALE_WITHDRAWN
    "data": {
        "auctionUuid": STRING,
        "clientId": STRING,
        "attachments": {
            [attachmentUuid]: ATTACHMENT_MODEL,
            ...
        },
        "listings": {
            [listingUuid]: LISTING_MODEL,
            ...
        },
        "sales": {
            [listingUuid]: SALE_MODEL,
            ...
        },
        "saleStatuses": {
            [listingUuid]: SALE_STATUS_MODEL,
            ...
        }
    }
}
```

### SALE_COMPLETED / SALE_STARTED
Informs of a sale either being completed (sold, passed etc) or of a sale being started (being relisted, re-offered)

``` javascript
{
    "action": "SALE_COMPLETED", // or SALE_STARTED
    "data": {
        "auctionUuid": STRING,
        "clientId": STRING,
        "inPlay": WEBCAST_INPLAY_MODEL?,
        "sale": SALE_MODEL?,
        "saleStatus": SALE_STATUS_MODEL
    }
}
```

### SALE_INCREMENT_SET / SALE_NEXT_BID_SET
Informs of a sale having the increment or Next Bid amount changed

``` javascript
{
    "action": "SALE_INCREMENT_SET", // or SALE_NEXT_BID_SET
    "data": {
        "auctionUuid": STRING,
        "sale": SALE_MODEL
    }
}
```

### SALE_UNITS_BIDDING_TYPE_SET
Informs of a change in the sale units for a listing. E.g. price per item rather than per lot.

``` javascript
{
    "action": "SALE_UNITS_BIDDING_TYPE_SET",
    "data": {
        "auctionUuid": STRING,
        "clientId": STRING,
        "sale": SALE_MODEL,
        "saleStatus": SALE_STATUS_MODEL
    }
}
```

### OFFER_PENDING
Informs of a new offer on a listing

``` javascript
{
    "action": "OFFER_PENDING",
    "data": {
        "auctionUuid": STRING,
        "clientId": STRING,
        "sale": SALE_MODEL,
        "saleStatus": SALE_STATUS_MODEL
    }
}
```

### WEBCAST_FEED_SET
#### Webcast
Informs of a change in the Webcast video feed. Usually either a new feed being published or one being removed

``` javascript
{
    "action": "WEBCAST_FEED_SET",
    "data": WEBCAST_FEED_MODEL
}
```

### WEBCAST_INPLAY_SET / WEBCAST_INPLAY_STATE_SET / WEBCAST_INPLAY_WARNING
#### Webcast
Informs of a change in the Webcast InPlay item. Namely a change in which listing is inPlay, whether bidding is paused, and the "fair warning" notification.

``` javascript
{
    "action": "WEBCAST_INPLAY_SET", // or other, as above
    "data": WEBCAST_INPLAY_MODEL
}
```

### WEBCAST_MESSAGE_SET
#### Webcast
Informs of a new message

``` javascript
{
    "action": "WEBCAST_MESSAGE_SET",
    "data": {
        auctionUuid: STRING,
        message: MESSAGE_OBJECT
    }
}
```
