---
id: models
title: Models
sidebar_label: Models
---

We define here the models and the properties within, used by our Websocket API.

## Attachment Model
``` javascript
{
    "uuid": STRING,
    "basePath": STRING,
    "label": STRING,
    "size": INT,
    "versionAndPublicId": STRING
}
```

## Auction Model
``` javascript
{
    "uuid": STRING,
    "activeListings": [LISTING_UUIDs],
    "endsAt": TIMESTAMP,
    "id": STRING,
    "live": BOOLEAN,
    "logoUuid": IMAGE_UUID?,
    "startsAt": TIMESTAMP,
    "title": STRING,
    "webcast": BOOLEAN
}
```

## Bid Model
``` javascript
{
    "uuid": STRING,
    "amount": INT, // in lowest currency value, e.g. pence / cents
    "autobid": BOOLEAN,
    "cancelled": BOOLEAN,
    "listingUuid": LISTING_UUID,
    "live": BOOLEAN,
    "onlineBidderOrigin": BOOLEAN, // is floor bidder, or online bidder
    "placedAt": TIMESTAMP,
    "userUuid": USER_UUID
}
```

## Listing Model
``` javascript
{
    "uuid": STRING,
    "buyersPremium": INT,
    "buyersPremiumTaxRate": INT,
    "categoryUuid": CATEGORY_UUID,
    "charges": INT,
    "chargesTaxRate": INT,
    "contactSeller": BOOLEAN,
    "countryCode": STRING,
    "description": STRING,
    "documents": [ATTACHMENT_UUIDs]
    "hammerTaxRate": INT,
    "id": INT,
    "images": [ATTACHMENT_UUIDs],
    "address": {
        "address" STRING?,
        "locationLatitude": INT?,
        "locationLongitude": INT?,
    },
    "lotNumber": STRING,
    "summary": STRING,
    "title": STRING,
    "type": STRING
}
```

## Message Model
``` javascript
{
    "uuid": STRING,
    "listingUuid": STRING, // the item targeted by the item message
    "popup": BOOLEAN,
    "text": STRING,
    "typeId": INT, // 0 - auction message, 1 - item message
    "userUuid": STRING // the user uuid targeted by the message
}
```

## Registrant Model
``` javascript
{
    "uuid": STRING,
    "approved": BOOLEAN,
    "city": STRING,
    "countryCode": STRING,
    "county": STRING,
    "id": INT,
    "paddleNumber": INT,
    "statusLabel": STRING,
    "userUuid": STRING
}
```

## Sale Model
``` javascript
{
    "biddingTypeId": INT,
    "bids": [BID_UUIDs],
    "buyNowValue": INT,
    "buyersPremium": INT,
    "buyersPremiumTaxRate": INT,
    "charges": INT,
    "chargesTaxRate": INT,
    "currencyCode": STRING,
    "hammerTaxRate": INT,
    "incrementValue": INT, // in lowest currency value, e.g. pence / cents
    "listingUuid": LISTING_UUID,
    "nextBidCalculation": STRING, // only used in per Item bidding etc, to explain cost per items
    "nextBidValue": INT, // in lowest currency value, e.g. pence / cents
    "numberOfUnits": INT,
    "purchaseTypeId": INT, // 1 - None, 2 - Make Offer, 3 - Buy Now, 4 - Transfer, 5 - Tender, 6 - Tender PCT
    "reserve": INT,
    "unitOfMeasurement": STRING
}
```

## Sale Status Model
``` javascript
{
    "complete": BOOLEAN,
    "endsAt": LONG,
    "highestBidUuid": BID_UUID,
    "highestOfferUuid": OFFER_UUID,
    "inOvertimeBidding": BOOLEAN
    "listingUuid": LISTING_UUID,
    "preview": BOOLEAN,
    "sold": BOOLEAN,
    "suspended": BOOLEAN,
    "withdrawn": BOOLEAN
}
```

## Webcast Feed Model
``` javascript
{
    "apiKey": STRING,
    "auctionUuid": AUCTION_UUID,
    "sessionId": STRING,
    "token": STRING
}
```

## Webcast InPlay Model
``` javascript
{
    "auctionUuid": AUCTION_UUID,
    "listingUuid": LISTING_UUID,
    "warningCount": INT, // 0 - no sell warning, 1 - first warning, 2 - second warning, 3 - final warning
    "paused": BOOLEAN
}
```
