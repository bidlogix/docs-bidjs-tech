---
id: registration
title: Registration Webhook Events
sidebar_label: Registration
slug: /webhooks/events/registration
--- 

### Event Name

```text
AUCTION_REGISTRATION
```

The Registration webhook is triggered when an authenticated user registers for an auction or Marketplace.

Depending on your auctioneer settings, this user will be put into one of several statuses. See [Registrant Status Codes](/api/general%20info/status-codes#registrant-status-codes).

Essential data about the registrant will be reported by the webhook, such as the user UUID, registrant status, name, etc (See the Schema below for the full list of data retrieved).

This webhook could be used in conjunction with our [Registrant Status Update API](/api/update-registrant-status) to automate the registration processes, which could include deposit requests and KYC checks.

### Event Schema

```json
{
  "auctioneerUuid": "string",
  "userUuid": "string",
  "username": "string",
  "registrationUuid": "string",
  "auctionUuid": "string",
  "paddleNumber": "string",
  "status": "string",
  "countryCode": "string",
  "countryName": "string",
  "email": "string",
  "fullName": "string",
  "spendingLimit": "string"
}
```

### Create new `eventType` in Svix

Run the following curl command to create a new `AUCTION_REGISTRATION` `eventType` in Svix.

You can customise the `description` fields to suit your needs.

Simply change `AUTH_TOKEN` for the Svix API key.  

See [How to Create your Consumer App](/webhooks#how-to-create-your-consumer-app) for more information.

```bash
curl -X 'POST' \
  'https://api.eu.svix.com/api/v1/event-type/' \
  -H 'Authorization: Bearer AUTH_TOKEN' \
  -H 'Accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "name": "AUCTION_REGISTRATION",
  "description": "Event triggered when a new user registers to an auction",
  "archived": false,
  "schemas": {
      "1":{
          "description":"A user has registered",
          "properties":{
              "auctioneerUuid":{
                  "description":"The auctioneer uuid",
                  "type":"string"
              },
              "userUuid":{
                  "description":"The user uuid",
                  "type":"string"
              },
              "username":{
                  "description":"The username",
                  "type":"string"
              },
              "registrationUuid":{
                  "description":"The registration uuid",
                  "type":"string"
              },
              "auctionUuid":{
                  "description":"The auction uuid",
                  "type":"string"
              },
              "paddleNumber":{
                  "description":"The paddle number",
                  "type":"string"
              },
              "status":{
                  "description":"The status of the registration",
                  "type":"string"
              },
              "countryCode":{
                  "description":"The country code",
                  "type":"string"
              },
              "countryName":{
                  "description":"The country name",
                  "type":"string"
              },
              "email":{
                  "description":"The email address of the user",
                  "type":"string"
              },
              "fullName":{
                  "description":"The full name of the user",
                  "type":"string"
              },
              "spendingLimit":{
                  "description":"The spending limit",
                  "type":"string"
              }
        },
          "required":["auctionUuid", "auctioneerUuid", "countryCode", "countryName", "email", "fullName", "paddleNumber", "registrationUuid", "spendingLimit", "status", "userUuid", "username"],
          "title":"User makes registration request for an auction",
          "type":"object"
    }
  }
}'
```
