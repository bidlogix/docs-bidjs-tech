# Events List

| Event Name                                              | Event                   | Description                                             |
|:--------------------------------------------------------|:------------------------|:--------------------------------------------------------|
| [Auction Status Change](/webhooks/events/auctionStatus) | `AUCTION_STATUS_CHANGE` | Event triggered when an auction status changes          |
| [Item Status Change](/webhooks/events/itemStatus)       | `ITEM_STATUS_CHANGE`    | Event triggered when an auction item status changes     |
| [Registration](/webhooks/events/registration)           | `AUCTION_REGISTRATION`     | Event triggered when a new user registers to an auction |
