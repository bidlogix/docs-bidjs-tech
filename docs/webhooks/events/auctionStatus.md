---
id: auctionStatus
title: Auction Status Change Webhook Events
sidebar_label: Auction Status Change
slug: /webhooks/events/auctionStatus
--- 

### Event

```text
AUCTION_STATUS_CHANGE
```

The Auction Status Change webhook is triggered when an auction's status is changed within the BidJS Admin and upon creation of an auction.

The webhook will report the auction's new status and auction UUID, which could enable customers to automate processes such as email marketing triggers or updating third-party websites.

Auction status types can be found here [Auction Status Codes.](/api/general%20info/status-codes#auction-status-codes)

### Event Schema

```json
{
  "auctioneerUuid": "string",
  "auctionUuid": "string",
  "status": "string"
}
```

### Create new `eventType` in Svix

Run the following curl command to create a new `AUCTION_STATUS_CHANGE` `eventType` in Svix.

You can customise the `description` fields to suit your needs.

Simply change `AUTH_TOKEN` for the Svix API key.  

See [How to Create your Consumer App](/webhooks#how-to-create-your-consumer-app) for more information.

```bash
curl -X 'POST' \
  'https://api.eu.svix.com/api/v1/event-type/' \
  -H 'Authorization: Bearer AUTH_TOKEN' \
  -H 'Accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "name": "AUCTION_STATUS_CHANGE",
  "description": "Event triggered when an auction status changes",
  "archived": false,
  "schemas": {
      "1":{
          "description":"The status of an auction has changed",
          "properties":{
              "auctioneerUuid":{
                  "description":"The auctioneer uuid",
                  "type":"string"
              },
              "auctionUuid":{
                  "description":"The auction uuid",
                  "type":"string"
              },
              "status":{
                  "description":"The registration status",
                  "type":"string"
              }
          },
          "required":["auctionUuid", "auctioneerUuid", "status"],
          "title":"Auction status changed",
          "type":"object"
      }
  }
}'
```
