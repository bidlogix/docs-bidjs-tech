---
id: itemStatus
title: Item Status Change Webhook Events
sidebar_label: Item Status Change
slug: /webhooks/events/itemStatus
---

### Event

```text
ITEM_STATUS_CHANGE
```

The Item Status Change webhook is triggered when an item's status is changed.  A status change event is sent when;

* item end date is reached in a timed auction
* item is sold in a webcast
* item is passed in a webcast

The webhook will report the item's new status, item UUID, auctioneer UUID and auction UUID, which could enable customers to automate processes such as email marketing triggers or updating third-party websites.

Auction item status types can be found here [Item Status Codes.](/api/general%20info/status-codes#item-status-codes)

### Event Schema

```json
{
  "itemUuid": "string",
  "auctioneerUuid": "string",
  "auctionUuid": "string",
  "status": "string"
}
```

### Create new `eventType` in Svix

Run the following curl command to create a new `ITEM_STATUS_CHANGE` `eventType` in Svix.

You can customise the `description` fields to suit your needs.

Simply change `AUTH_TOKEN` for the Svix API key.

See [How to Create your Consumer App](/webhooks#how-to-create-your-consumer-app) for more information.

```bash
curl -X 'POST' \
  'https://api.eu.svix.com/api/v1/event-type/' \
  -H 'Authorization: Bearer AUTH_TOKEN' \
  -H 'Accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "name": "ITEM_STATUS_CHANGE",
  "description": "Event triggered when an item status changes",
  "archived": false,
  "schemas": {
      "1":{
          "description":"The status of an item has changed",
          "properties":{
              "itemUuid":{
                  "description":"The item uuid",
                  "type":"string"
              },
              "auctioneerUuid":{
                  "description":"The auctioneer uuid",
                  "type":"string"
              },
              "auctionUuid":{
                  "description":"The auction uuid",
                  "type":"string"
              },
              "status":{
                  "description":"The registration status",
                  "type":"string"
              }
          },
          "required":["itemUuid", "auctionUuid", "auctioneerUuid", "status"],
          "title":"Item status changed",
          "type":"object"
      }
  }
}'
```
