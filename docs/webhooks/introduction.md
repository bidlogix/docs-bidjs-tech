---
id: introduction
title: Harnessing Real-time Communication with Webhooks
sidebar_label: Introduction
slug: /webhooks
--- 

Webhooks are a powerful and versatile tool for enhancing the functionality of web applications. They enable real-time communication between different software systems by allowing one application to notify another about specific events or updates. Unlike traditional polling methods, webhooks initiate data transfer automatically, ensuring immediate responses to important events.

In essence, webhooks serve as a mechanism for pushing data from one application to another, rather than relying on the latter to pull data at regular intervals. This proactive approach streamlines data synchronization, improves efficiency, and minimizes latency.

## Table of Contents

[How to integrate webhooks with Bidjs](#how-to-integrate-webhooks-with-bidjs)  
[How to Create your Consumer App](#how-to-create-your-consumer-app)  
[How to Add an Endpoint](#how-to-add-an-endpoint)  
[How to access your Endpoint ID](#how-to-access-your-endpoint-id)  

---

## How to integrate webhooks with Bidjs

In order to receive webhooks events from BidJS you need to create an account with *[Svix](https://www.svix.com/)* and supply BidJS support with your Consumer App ID (which starts 'app_') and API Access Key, both of which are generated by Svix.

---

### How to Create your Consumer App

* To create an account go to [link](https://www.svix.com).

* You will need to login and generate an API Access Key in order to successfully create your Consumer App.

  * To do this, navigate to API Access and click on Create key. Svix will ask you to name your key and click Create.

  * Important: The Svix API key is generated/displayed only once, so be sure to make a record of it so it can be supplied to BidJS Support and to successfully create your Consumer App.
  ​

* Next, navigate to Consumer Applications and click on Add Application.

  * Svix will ask you to name your Consumer App and run a code snippet in your terminal, which includes your Svix API Key as per below.

  * Once this is successfully executed your Consumer App will appear within Consumer Applications.

```bash
curl -X POST "https://api.eu.svix.com/api/v1/app/" \ -H "Accept: application/json" \ -H "Content-Type: application/json" \ -H "Authorization: Bearer Your_Svix_API_Key" \ -d '{"name": "My App"}'
```

---

### How to access your Consumer App ID

* Within your Svix account, go to Consumer Applications.

* Hover over the Consumer App ID and click to copy the ID (see image below)

![Consumer App ID](/img/webhooks/consumer-app-id.png)

---

## Dealing with events

Every event will reach your Consumer App in real-time, allowing you to direct each event to a specific endpoint.

In order to do this you will need to pre-configure and `eventType` within your Svix Consumer App. Alternatively, you can use the Svix API to create an endpoint for each event type.

Visit the [Events](/webhooks/events) section for a list of events that can be subscribed to and their snippets in order to create an `eventType` for each event from the command line.

### How to Add an Endpoint

Some clients may wish to use a third-party tool (such as Zapier or Mailchimp) that supports Webhooks in order to create automation within their workflow.  
​
If this is something you would like to do you will need to configure the third-party tool, as is required, and add the endpoint supplied into the Svix Consumer App for the specific BidJS webhook.

* Go to Consumer Applications and click on your Consumer Application.

* Within the Consumer App view click on Preview App Portal

![Preview App Portal](/img/webhooks/preview-app-portal.png)

* Within the App Portal click on **Add Endpoint**

![Add Endpoint](/img/webhooks/add-endpoint.png)

* Enter the desired endpoint and its description (optional).

* If you have any `eventType` pre-configured they will appear in the dropdown menu. Select the `eventType` you wish to apply to the endpoint.

![Add Endpoint](/img/webhooks/add-endpoint-eventType.png)

* The endpoint will now appear within the App Portal.

---

### How to access your Endpoint ID

* Within your Svix account, go to Consumer Applications and select the Consumer App you wish to apply your endpoint to.

* Go to Preview App Portal.

* Within Endpoints, click on the endpoint inputted previously.

* Hover over your Endpoint ID fragment and click to copy (see image below).

![Endpoint ID](/img/webhooks/endpoint-id.png)
