---
id: category-data
title: Categories List
sidebar_label: Categories List
---

## Description

The Categories List call reports on category reference data within the bidlogix system. This will provide a list of categories activated for this client ID (webapp) including information such as the IDs of the categories, any parent/child categories, etc.

The API must be structured correctly and have the correct security in place. See our document **Troubleshooting**.

See our documents **Status Codes** and **Common Data** for more information on results.

## Making a request

### URL

 ```bash
https://your_bidlogix_domain/auction-mgt/bdxapi/categories?clientId=your_bidlogix_client_id
 ```

### HTTP Method

```bash
GET
```

### Headers

| Key          |                  Value |
| :----------- | ---------------------: |
| BDXAPI_NAME  | Bidlogix API Signature |
| Content-Type |       application/json |

### Path Parameters

| Parameter |                                          Value |
| :-------- | ---------------------------------------------: |
| clientId | your bidlogix client ID |

Also see [API Introduction](./general%20info/apiInfo.md).

## Examples

Call

```bash
https://hove.eu-west-2.bidjs.com//auction-mgt/bdxapi/categories?clientId=302
```

Response

``` json
{
    "@class": ".DataPushPayload",
    "pushCode": "CATEGORY_REFERENCE",
    "models": {
        "categoryReferenceModel": {
            "@class": ".BackboneCollectionModel",
            "collection": [
                {
                    "@class": ".reference.CategoryTreeModel",
                    "id": 3,
                    "name": "Bidlogix Demo Tree&reg;",
                    "code": null,
                    "selected": false,
                    "parents": [
                        {
                            "@class": ".CategoryModel",
                            "id": 396,
                            "name": "Bidlogix Demo",
                            "code": null,
                            "selected": false,
                            "treeName": "Bidlogix Demo Tree&reg;",
                            "treeId": 3,
                            "parentName": null,
                            "parentId": null,
                            "children": [
                                {
                                    "@class": ".CategoryModel",
                                    "id": 397,
                                    "name": "Demo Category - Instruments",
                                    "code": null,
                                    "selected": false,
                                    "treeName": "Bidlogix Demo Tree&reg;",
                                    "treeId": 3,
                                    "parentName": "Bidlogix Demo",
                                    "parentId": 396,
                                    "children": null
                                },
                                {
                                    "@class": ".CategoryModel",
                                    "id": 398,
                                    "name": "Demo Category - Silverware",
                                    "code": null,
                                    "selected": false,
                                    "treeName": "Bidlogix Demo Tree&reg;",
                                    "treeId": 3,
                                    "parentName": "Bidlogix Demo",
                                    "parentId": 396,
                                    "children": null
                                },
                                {
                                    "@class": ".CategoryModel",
                                    "id": 399,
                                    "name": "Demo Category - Machinery",
                                    "code": null,
                                    "selected": false,
                                    "treeName": "Bidlogix Demo Tree&reg;",
                                    "treeId": 3,
                                    "parentName": "Bidlogix Demo",
                                    "parentId": 396,
                                    "children": null
                                },
                                {
                                    "@class": ".CategoryModel",
                                    "id": 406,
                                    "name": "Demo Category - Pocketwatches",
                                    "code": null,
                                    "selected": false,
                                    "treeName": "Bidlogix Demo Tree&reg;",
                                    "treeId": 3,
                                    "parentName": "Bidlogix Demo",
                                    "parentId": 396,
                                    "children": null
                                },
                                {
                                    "@class": ".CategoryModel",
                                    "id": 407,
                                    "name": "Demo Category - Property",
                                    "code": null,
                                    "selected": false,
                                    "treeName": "Bidlogix Demo Tree&reg;",
                                    "treeId": 3,
                                    "parentName": "Bidlogix Demo",
                                    "parentId": 396,
                                    "children": null
                                },
                                {
                                    "@class": ".CategoryModel",
                                    "id": 408,
                                    "name": "Demo Category - Tractors",
                                    "code": null,
                                    "selected": false,
                                    "treeName": "Bidlogix Demo Tree&reg;",
                                    "treeId": 3,
                                    "parentName": "Bidlogix Demo",
                                    "parentId": 396,
                                    "children": null
                                },
                                {
                                    "@class": ".CategoryModel",
                                    "id": 545,
                                    "name": "Demo Category - Antiques",
                                    "code": null,
                                    "selected": false,
                                    "treeName": "Bidlogix Demo Tree&reg;",
                                    "treeId": 3,
                                    "parentName": "Bidlogix Demo",
                                    "parentId": 396,
                                    "children": null
                                },
                                {
                                    "@class": ".CategoryModel",
                                    "id": 546,
                                    "name": "Demo Category - Cars",
                                    "code": null,
                                    "selected": false,
                                    "treeName": "Bidlogix Demo Tree&reg;",
                                    "treeId": 3,
                                    "parentName": "Bidlogix Demo",
                                    "parentId": 396,
                                    "children": null
                                },
                                {
                                    "@class": ".CategoryModel",
                                    "id": 1866,
                                    "name": "Demo Category - Livestock",
                                    "code": null,
                                    "selected": false,
                                    "treeName": "Bidlogix Demo Tree&reg;",
                                    "treeId": 3,
                                    "parentName": "Bidlogix Demo",
                                    "parentId": 396,
                                    "children": null
                                }
                            ]
                        },
                        {
                            "@class": ".CategoryModel",
                            "id": 400,
                            "name": "Bidlogix Marketplace Property",
                            "code": null,
                            "selected": false,
                            "treeName": "Bidlogix Demo Tree&reg;",
                            "treeId": 3,
                            "parentName": null,
                            "parentId": null,
                            "children": [
                                {
                                    "@class": ".CategoryModel",
                                    "id": 401,
                                    "name": "Bidlogix demo comercial",
                                    "code": null,
                                    "selected": false,
                                    "treeName": "Bidlogix Demo Tree&reg;",
                                    "treeId": 3,
                                    "parentName": "Bidlogix Marketplace Property",
                                    "parentId": 400,
                                    "children": null
                                },
                                {
                                    "@class": ".CategoryModel",
                                    "id": 402,
                                    "name": "Bidlogix demo domestic",
                                    "code": null,
                                    "selected": false,
                                    "treeName": "Bidlogix Demo Tree&reg;",
                                    "treeId": 3,
                                    "parentName": "Bidlogix Marketplace Property",
                                    "parentId": 400,
                                    "children": null
                                },
                                {
                                    "@class": ".CategoryModel",
                                    "id": 403,
                                    "name": "Bidlogix demo other",
                                    "code": null,
                                    "selected": false,
                                    "treeName": "Bidlogix Demo Tree&reg;",
                                    "treeId": 3,
                                    "parentName": "Bidlogix Marketplace Property",
                                    "parentId": 400,
                                    "children": null
                                },
                                {
                                    "@class": ".CategoryModel",
                                    "id": 5245,
                                    "name": "Bidlogix demo generic",
                                    "code": null,
                                    "selected": false,
                                    "treeName": "Bidlogix Demo Tree&reg;",
                                    "treeId": 3,
                                    "parentName": "Bidlogix Marketplace Property",
                                    "parentId": 400,
                                    "children": null
                                }
                            ]
                        },
                        {
                            "@class": ".CategoryModel",
                            "id": 3178,
                            "name": "Bidlogix Marketplace Vehicles",
                            "code": null,
                            "selected": false,
                            "treeName": "Bidlogix Demo Tree&reg;",
                            "treeId": 3,
                            "parentName": null,
                            "parentId": null,
                            "children": [
                                {
                                    "@class": ".CategoryModel",
                                    "id": 3179,
                                    "name": "Bidlogix demo - cars",
                                    "code": null,
                                    "selected": false,
                                    "treeName": "Bidlogix Demo Tree&reg;",
                                    "treeId": 3,
                                    "parentName": "Bidlogix Marketplace Vehicles",
                                    "parentId": 3178,
                                    "children": null
                                },
                                {
                                    "@class": ".CategoryModel",
                                    "id": 3180,
                                    "name": "Bidlogix demo - vans",
                                    "code": null,
                                    "selected": false,
                                    "treeName": "Bidlogix Demo Tree&reg;",
                                    "treeId": 3,
                                    "parentName": "Bidlogix Marketplace Vehicles",
                                    "parentId": 3178,
                                    "children": null
                                },
                                {
                                    "@class": ".CategoryModel",
                                    "id": 3181,
                                    "name": "Bidlogix demo scrap",
                                    "code": null,
                                    "selected": false,
                                    "treeName": "Bidlogix Demo Tree&reg;",
                                    "treeId": 3,
                                    "parentName": "Bidlogix Marketplace Vehicles",
                                    "parentId": 3178,
                                    "children": null
                                },
                                {
                                    "@class": ".CategoryModel",
                                    "id": 5523,
                                    "name": "Bidlogix demo - sports cars",
                                    "code": null,
                                    "selected": false,
                                    "treeName": "Bidlogix Demo Tree&reg;",
                                    "treeId": 3,
                                    "parentName": "Bidlogix Marketplace Vehicles",
                                    "parentId": 3178,
                                    "children": null
                                },
                                {
                                    "@class": ".CategoryModel",
                                    "id": 5524,
                                    "name": "Bidlogix demo  vintage cars",
                                    "code": null,
                                    "selected": false,
                                    "treeName": "Bidlogix Demo Tree&reg;",
                                    "treeId": 3,
                                    "parentName": "Bidlogix Marketplace Vehicles",
                                    "parentId": 3178,
                                    "children": null
                                },
                                {
                                    "@class": ".CategoryModel",
                                    "id": 5525,
                                    "name": "Bidlogix demo - saloon cars",
                                    "code": null,
                                    "selected": false,
                                    "treeName": "Bidlogix Demo Tree&reg;",
                                    "treeId": 3,
                                    "parentName": "Bidlogix Marketplace Vehicles",
                                    "parentId": 3178,
                                    "children": null
                                }
                            ]
                        },
                        {
                            "@class": ".CategoryModel",
                            "id": 3174,
                            "name": "Demo Marketplace Antiques",
                            "code": null,
                            "selected": false,
                            "treeName": "Bidlogix Demo Tree&reg;",
                            "treeId": 3,
                            "parentName": null,
                            "parentId": null,
                            "children": [
                                {
                                    "@class": ".CategoryModel",
                                    "id": 3175,
                                    "name": "Bidlogix demo tables",
                                    "code": null,
                                    "selected": false,
                                    "treeName": "Bidlogix Demo Tree&reg;",
                                    "treeId": 3,
                                    "parentName": "Demo Marketplace Antiques",
                                    "parentId": 3174,
                                    "children": null
                                },
                                {
                                    "@class": ".CategoryModel",
                                    "id": 3176,
                                    "name": "Bidlogix demo chairs",
                                    "code": null,
                                    "selected": false,
                                    "treeName": "Bidlogix Demo Tree&reg;",
                                    "treeId": 3,
                                    "parentName": "Demo Marketplace Antiques",
                                    "parentId": 3174,
                                    "children": null
                                },
                                {
                                    "@class": ".CategoryModel",
                                    "id": 3177,
                                    "name": "Bidlogix demo lamps",
                                    "code": null,
                                    "selected": false,
                                    "treeName": "Bidlogix Demo Tree&reg;",
                                    "treeId": 3,
                                    "parentName": "Demo Marketplace Antiques",
                                    "parentId": 3174,
                                    "children": null
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    },
    "webAppContext": "302"
}
```

## Errors

| code | example |
| :--- | -------: |
| 401 UNAUTHORIZED | \{ error : "Log in" \} |
| 422 Unprocessable Entry | \{ error : ["\<field_name\>"] \} |
