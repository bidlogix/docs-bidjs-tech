---
id: get-registrants
title: Registrants Get
sidebar_label: Registrants Get
---


## Description 
Get registrant information for a given auction.  

## Making a request

 ### URL 
 ```
/auctions/{auctionId}/registrants
 ```

### HTTP Method
```
GET
```

### Headers 
| Key          |                  Value |
| :----------- | ---------------------: |
| BDXAPI_NAME  | Bidlogix API Signature |
| Content-Type |       application/json |


### Path Parameters 
| Parameter |                                          Value |
| :-------- | ---------------------------------------------: |
| auctionId      | The auction ID for the given auction      |


### Call Details
[Click here to see our documentation](/apiDefinitions)

:::note Notes
This call will retrieve all registrant information for this auction. This does not return bid details for each registrant.

:::