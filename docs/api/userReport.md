---
id: user-report
title: User Bidding Report
sidebar_label: User Bidding Report
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

## Description 
The User Bidding Report call reports the bidding history of a specific user. The report is ordered from most recent bid to least recent bid. It can be filtered by category so that only bids on items in the requested category are returned. 

The API must be structured correctly and have the correct security in place. See our document **Troubleshooting**. 

See our documents **Status Codes** and **Common Data** for more information on results. 



## Making a request

 ### URL 
 ```
https://your_bidlogix_domain/auction-mgt/bdxapi/reporting/user/{userId}/{categoryId}/{pageSize}/{page}?clientId=your_bidlogix_client_id
 ```

### HTTP Method
```
GET
```

### Headers 
| Key          |                  Value |
| :----------- | ---------------------: |
| BDXAPI_NAME  | Bidlogix API Signature |
| Content-Type |       application/json |


### Path Parameters and Adaptations
| Parameter |                                          Value |
| :-------- | ---------------------------------------------: |
| userId | ID of the user you wish to retrieve information for |
| categoryId | the ID of the category you wish to retrieve information for. Set to `-1` to retrieve all |
| pageSize | records to return per page |
| page | the page to start from |
| clientId | your bidlogix Client ID |

Also see [API Introduction](./general%20info/apiInfo.md).


## Examples 
<Tabs
  defaultValue="minimum"
  groupId='installation-type'
  values={[
    {label: 'Example 1 - single category', value: 'minimum'},
    {label: 'Example 2 - all categories', value: 'all'}
  ]}>
<TabItem value="minimum">

Call
``` 
https://hove.eu-west-2.bidjs.com/auction-mgt/bdxapi/reporting/user/196/5523/200/0?clientId=302
```
Response 
``` json
{
    "@class": ".DataPushPayload",
    "pushCode": "REPORTING_USER",
    "models": {
        "userReport": {
            "@class": ".reporting.UserReportModel",
            "reportMeta": {
                "@class": ".ReportMetaModel",
                "reportType": 2,
                "reportObject": "UserReportModel",
                "reportMade": 1634737032496
            },
            "reportTitle": "User Report",
            "userId": 196,
            "username": "bidder3_demo",
            "categoryId": 5523,
            "count": 1,
            "email": "test@bidlogixtest.com",
            "externalRef": "D232",
            "lastLogOn": "20-Oct-21 13:24:01",
            "items": [
                {
                    "@class": ".UserBiddingReportModel",
                    "auctionId": 3304,
                    "auctionTitle": "title4",
                    "catId": 5523,
                    "lotId": 364349,
                    "lotNumber": "1",
                    "itemTitle": "Black Mercedes Soft Top Sports Car",
                    "userAmount": 500,
                    "winningAmount": 6000100,
                    "placed": 1630491653000,
                    "curId": 2
                },
                {
                    "@class": ".UserBiddingReportModel",
                    "auctionId": 3304,
                    "auctionTitle": "title4",
                    "catId": 5523,
                    "lotId": 364349,
                    "lotNumber": "1",
                    "itemTitle": "Black Mercedes Soft Top Sports Car",
                    "userAmount": 300,
                    "winningAmount": 6000100,
                    "placed": 1630491555000,
                    "curId": 2
                },
                {
                    "@class": ".UserBiddingReportModel",
                    "auctionId": 3304,
                    "auctionTitle": "title4",
                    "catId": 5523,
                    "lotId": 364349,
                    "lotNumber": "1",
                    "itemTitle": "Black Mercedes Soft Top Sports Car",
                    "userAmount": 200,
                    "winningAmount": 6000100,
                    "placed": 1630491421000,
                    "curId": 2
                }
            ]
        }
    },
    "webAppContext": "302"
}
```

</TabItem>

<TabItem value="all">

Call 
``` 
https://hove.eu-west-2.bidjs.com/auction-mgt/bdxapi/reporting/user/196/-1/20/0?clientId=302
```

Response 
``` json
{
    "@class": ".DataPushPayload",
    "pushCode": "REPORTING_USER",
    "models": {
        "userReport": {
            "@class": ".reporting.UserReportModel",
            "reportMeta": {
                "@class": ".ReportMetaModel",
                "reportType": 2,
                "reportObject": "UserReportModel",
                "reportMade": 1634737172713
            },
            "reportTitle": "User Report",
            "userId": 196,
            "username": "bidder3_demo",
            "categoryId": -1,
            "count": 597,
            "email": "test@bidlogixtest.com",
            "externalRef": "D232",
            "lastLogOn": "20-Oct-21 13:24:01",
            "items": [
                {
                    "@class": ".UserBiddingReportModel",
                    "auctionId": 3861,
                    "auctionTitle": "test training auction",
                    "catId": 397,
                    "lotId": 401262,
                    "lotNumber": "1",
                    "itemTitle": "Title goes here",
                    "userAmount": 1220,
                    "winningAmount": 1220,
                    "placed": 1634735947000,
                    "curId": 2
                },
                {
                    "@class": ".UserBiddingReportModel",
                    "auctionId": 3769,
                    "auctionTitle": "Demo Timed Car Auction October",
                    "catId": 546,
                    "lotId": 390859,
                    "lotNumber": "2",
                    "itemTitle": "Car 2 &ndash; none, opening bid",
                    "userAmount": 850,
                    "winningAmount": 850,
                    "placed": 1634656689000,
                    "curId": 2
                },
                {
                    "@class": ".UserBiddingReportModel",
                    "auctionId": 3769,
                    "auctionTitle": "Demo Timed Car Auction October",
                    "catId": 546,
                    "lotId": 390860,
                    "lotNumber": "3",
                    "itemTitle": "Car 3 &ndash; none, opening bid",
                    "userAmount": 2300,
                    "winningAmount": 2300,
                    "placed": 1634632268000,
                    "curId": 2
                },
                {
                    "@class": ".UserBiddingReportModel",
                    "auctionId": 3871,
                    "auctionTitle": "sample auction 1",
                    "catId": 5245,
                    "lotId": 407720,
                    "lotNumber": "3",
                    "itemTitle": "picture 3",
                    "userAmount": 5,
                    "winningAmount": 5,
                    "placed": 1634564151000,
                    "curId": 2
                },
                {
                    "@class": ".UserBiddingReportModel",
                    "auctionId": 3871,
                    "auctionTitle": "sample auction 1",
                    "catId": 5245,
                    "lotId": 407719,
                    "lotNumber": "2",
                    "itemTitle": "picture 2",
                    "userAmount": 5,
                    "winningAmount": 10,
                    "placed": 1634564146000,
                    "curId": 2
                },
                {
                    "@class": ".UserBiddingReportModel",
                    "auctionId": 3871,
                    "auctionTitle": "sample auction 1",
                    "catId": 5245,
                    "lotId": 407718,
                    "lotNumber": "1",
                    "itemTitle": "famous picture 1",
                    "userAmount": 5,
                    "winningAmount": 20,
                    "placed": 1634564142000,
                    "curId": 2
                },
                {
                    "@class": ".UserBiddingReportModel",
                    "auctionId": 3861,
                    "auctionTitle": "test training auction",
                    "catId": 397,
                    "lotId": 401263,
                    "lotNumber": "2",
                    "itemTitle": "Title goes here",
                    "userAmount": 1200,
                    "winningAmount": 1200,
                    "placed": 1634305581000,
                    "curId": 2
                },
                {
                    "@class": ".UserBiddingReportModel",
                    "auctionId": 3861,
                    "auctionTitle": "test training auction",
                    "catId": 397,
                    "lotId": 401262,
                    "lotNumber": "1",
                    "itemTitle": "Title goes here",
                    "userAmount": 1200,
                    "winningAmount": 1220,
                    "placed": 1634305579000,
                    "curId": 2
                },
                {
                    "@class": ".UserBiddingReportModel",
                    "auctionId": 3769,
                    "auctionTitle": "Demo Timed Car Auction October",
                    "catId": 546,
                    "lotId": 390859,
                    "lotNumber": "2",
                    "itemTitle": "Car 2 &ndash; none, opening bid",
                    "userAmount": 750,
                    "winningAmount": 850,
                    "placed": 1633946218000,
                    "curId": 2
                },
                {
                    "@class": ".UserBiddingReportModel",
                    "auctionId": 3769,
                    "auctionTitle": "Demo Timed Car Auction October",
                    "catId": 546,
                    "lotId": 390858,
                    "lotNumber": "1",
                    "itemTitle": "Car 1 &ndash; none",
                    "userAmount": 2400,
                    "winningAmount": 2400,
                    "placed": 1633945867000,
                    "curId": 2
                },
                {
                    "@class": ".UserBiddingReportModel",
                    "auctionId": 3638,
                    "auctionTitle": "Demo",
                    "catId": 407,
                    "lotId": 374582,
                    "lotNumber": "7",
                    "itemTitle": "3 bedroom house, double garage &ndash; none",
                    "userAmount": 10,
                    "winningAmount": 10,
                    "placed": 1633527726000,
                    "curId": 2
                },
                {
                    "@class": ".UserBiddingReportModel",
                    "auctionId": 3638,
                    "auctionTitle": "Demo",
                    "catId": 407,
                    "lotId": 374578,
                    "lotNumber": "3",
                    "itemTitle": "Office Space, 10th floor &ndash; none, opening bid",
                    "userAmount": 260000,
                    "winningAmount": 260000,
                    "placed": 1633526292000,
                    "curId": 2
                },
                {
                    "@class": ".UserBiddingReportModel",
                    "auctionId": 3789,
                    "auctionTitle": "Test Training - BID TABLE",
                    "catId": 1866,
                    "lotId": 393091,
                    "lotNumber": "9",
                    "itemTitle": "Suckling Piglets",
                    "userAmount": 150,
                    "winningAmount": 160,
                    "placed": 1633513281000,
                    "curId": 2
                },
                {
                    "@class": ".UserBiddingReportModel",
                    "auctionId": 3789,
                    "auctionTitle": "Test Training - BID TABLE",
                    "catId": 1866,
                    "lotId": 393091,
                    "lotNumber": "9",
                    "itemTitle": "Suckling Piglets",
                    "userAmount": 130,
                    "winningAmount": 160,
                    "placed": 1633513279000,
                    "curId": 2
                },
                {
                    "@class": ".UserBiddingReportModel",
                    "auctionId": 3789,
                    "auctionTitle": "Test Training - BID TABLE",
                    "catId": 1866,
                    "lotId": 393091,
                    "lotNumber": "9",
                    "itemTitle": "Suckling Piglets",
                    "userAmount": 110,
                    "winningAmount": 160,
                    "placed": 1633513276000,
                    "curId": 2
                },
                {
                    "@class": ".UserBiddingReportModel",
                    "auctionId": 3789,
                    "auctionTitle": "Test Training - BID TABLE",
                    "catId": 1866,
                    "lotId": 393088,
                    "lotNumber": "6",
                    "itemTitle": "Dairy Cattle",
                    "userAmount": 130,
                    "winningAmount": 130,
                    "placed": 1633513241000,
                    "curId": 2
                },
                {
                    "@class": ".UserBiddingReportModel",
                    "auctionId": 3789,
                    "auctionTitle": "Test Training - BID TABLE",
                    "catId": 1866,
                    "lotId": 393088,
                    "lotNumber": "6",
                    "itemTitle": "Dairy Cattle",
                    "userAmount": 110,
                    "winningAmount": 130,
                    "placed": 1633513236000,
                    "curId": 2
                },
                {
                    "@class": ".UserBiddingReportModel",
                    "auctionId": 3789,
                    "auctionTitle": "Test Training - BID TABLE",
                    "catId": 1866,
                    "lotId": 393087,
                    "lotNumber": "5",
                    "itemTitle": "Store Cattle",
                    "userAmount": 110,
                    "winningAmount": 120,
                    "placed": 1633513229000,
                    "curId": 2
                },
                {
                    "@class": ".UserBiddingReportModel",
                    "auctionId": 3789,
                    "auctionTitle": "Test Training - BID TABLE",
                    "catId": 1866,
                    "lotId": 393083,
                    "lotNumber": "1",
                    "itemTitle": "Ewes",
                    "userAmount": 130,
                    "winningAmount": 130,
                    "placed": 1633513191000,
                    "curId": 2
                },
                {
                    "@class": ".UserBiddingReportModel",
                    "auctionId": 3789,
                    "auctionTitle": "Test Training - BID TABLE",
                    "catId": 1866,
                    "lotId": 393086,
                    "lotNumber": "4",
                    "itemTitle": "Rams",
                    "userAmount": 170,
                    "winningAmount": 170,
                    "placed": 1633513107000,
                    "curId": 2
                }
            ]
        }
    },
    "webAppContext": "302"
}
```
</TabItem>
</Tabs>

## Errors 
| code | example |
| :--- | -------: |
| 401 UNAUTHORIZED | \{ error : "Log in" \} |
| 422 Unprocessable Entry | \{ error : ["\<field_name\>"] \} |


