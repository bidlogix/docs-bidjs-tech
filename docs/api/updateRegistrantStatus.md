---
id: update-registrant-status
title: Registrant Status Update 
sidebar_label: Registrant Status Update
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

## Description

Edit a registrant status

## Making a request

### URL

 ```
/v2/auctions/{auctionUuid}/registrants/{registrantUuid}
 ```

### HTTP Method

```bash
PATCH
```

### Headers

| Key          |                  Value |
| :----------- | ---------------------: |
| BDXAPI_NAME  | Bidlogix API Signature |
| Content-Type |       application/json |

### Path Parameters

| Parameter |                                          Value |
| :-------- | ---------------------------------------------: |
| auctioneerUuid | ID of the auction you wish to update registrants for |
| registrantUuid | ID of the registrant you wish to update status for |

Also see [API Introduction](./general%20info/apiInfo.md).  

### Call Details

[Click here to see our documentation](/apiDefinitions)
