---
id: create-items
title: Items Create
sidebar_label: Items Create
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

## Description 
Create items within an already defined auction. This API call adds items to the auction - there may or may not be items in the auction already.

The API is very flexible and only the item type is needed to create a new item (all data can of course be reviewed and changed within the bidlogix admin system). Having said that the API allows any or all of the fields to be set in the API call, although errors will be generated if values of the wrong type are sent (e.g. sending a string instead of a numeric value).

Apart from this the error handling is very lenient and default values will be provided where no value is sent in the API.

## Making a request

 ### URL 
 This call creates items in a `draft` status: 
 ```
https://your_bidlogix_domain/auction-mgt/bdxapi/items/{auctionId}?clientId=your_bidlogix_client_id
 ```


To create items straight into a `live` status, use this call:
 ```
https://your_bidlogix_domain/auction-mgt/bdxapi/items-live/{auctionId}?clientId=your_bidlogix_client_id
 ```

### HTTP Method
```
POST
```

### Headers 
| Key          |                  Value |
| :----------- | ---------------------: |
| BDXAPI_NAME  | Bidlogix API Signature |
| Content-Type |       application/json |


### Path Parameters 
| Parameter |                                          Value |
| :-------- | ---------------------------------------------: |
| auctionId | ID of the auction you wish to create items for |

Also see [API Introduction](./general%20info/apiInfo.md).

### API Model Request  

<details> 
    <summary> Minimum required fields </summary> 
        
    <table>
        <thead>
            <tr>
                <th align="left">Name</th>
                <th>Type</th>
                <th>Value Range</th>
                <th>Required</th>
                <th align="right">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td align="left">type</td>
                <td>String</td>
                <td>Listing, Lot, WebcastItem</td>
                <td>Y</td>
                <td align="right">-</td>
            </tr>
            <tr>
                <td align="left">@class</td>
                <td>String</td>
                <td>.ItemApiModel</td>
                <td>Y</td>
                <td align="right">-</td>
            </tr>
            <tr>
                <td align="left">vendorId</td>
                <td>Long</td>
                <td>Valid bidlogix vendor ID. If not supplied, it will default to first Vendor available alphabetically.</td>
                <td>Y</td>
                <td align="right">first defined</td>
            </tr>
        </tbody>
    </table>

Errors in sent values will be corrected automatically. Validation tools and life cycle workflows within the bidlogix system are designed to check bidding data before items are made live. The API is mainly intended for asset database content - titles and images can be easily uploaded into auctions within the bidlogix platform.

Lot numbers will default to the index of the item within the list of items, and the title will default to 'Item for lot number x'.
</details>        

<details> 
    <summary> All fields </summary>
    <table>
        <thead>
            <tr>
                <th align="left">Name</th>
                <th>Type</th>
                <th>Value Range</th>
                <th>Required</th>
                <th align="right">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td align="left">type</td>
                <td>String</td>
                <td>Listing, Lot, WebcastItem</td>
                <td>Y</td>
                <td align="right">-</td>
            </tr>
            <tr>
                <td align="left">lotNumber</td>
                <td>String</td>
                <td>If not supplied the lot number is generated as the index of the item in the list. Lot numbers cannot contain duplicate values.</td>
                <td>N</td>
                <td align="right">item index</td>
            </tr>
            <tr>
                <td align="left">endTime</td>
                <td>String</td>
                <td>Must be in milliseconds. Required for webcast.</td>
                <td>N</td>
                <td align="right">auction end time</td>
            </tr>
            <tr>
                <td align="left">title</td>
                <td>String</td>
                <td>The title of the item - required for all items</td>
                <td>Y</td>
                <td align="right">Item for lot number x</td>
            </tr>
            <tr>
                <td align="left">summary</td>
                <td>String</td>
                <td>A short summary of the item or alternate information such as estimate of the sale price.</td>
                <td>N</td>
                <td align="right">-</td>
            </tr>
            <tr>
                <td align="left">description</td>
                <td>String</td>
                <td>A full description displayed on the item details page.</td>
                <td>N</td>
                <td align="right">-</td>
            </tr>
            <tr>
                <td align="left">images</td>
                <td>List</td>
                <td>see Common Data document.  Note: attachments must have a unique filename. If it is attempted to upload two attachment files with the same filename, the second will be ignored.</td>
                <td>N</td>
                <td align="right">-</td>
            </tr>
            <tr>
                <td align="left">documents</td>
                <td>List</td>
                <td>see Common Data document.  Note: attachments must have a unique filename. If it is attempted to upload two attachment files with the same filename, the second will be ignored.</td>
                <td>N</td>
                <td align="right">-</td>
            </tr>
            <tr>
                <td align="left">videos</td>
                <td>List</td>
                <td>see Common Data document. Set the Youtube video ID as the URL of the model object to attach Youtube videos to the item.  Note: attachments must have a unique filename. If it is attempted to upload two attachment files with the same filename, the second will be ignored.</td>
                <td>N</td>
                <td align="right">-</td>
            </tr>
            <tr>
                <td align="left">location</td>
                <td>AddressModel</td>
                <td>see Common Data document</td>
                <td>N</td>
                <td align="right">from auction</td>
            </tr>
            <tr>
                <td align="left">buyersPremium</td>
                <td>Long</td>
                <td>0 - 100</td>
                <td>N</td>
                <td align="right">from auction</td>
            </tr>
            <tr>
                <td align="left">contactSellerEnabled</td>
                <td>Boolean</td>
                <td>Enables the contact vendor feature per item.</td>
                <td>N</td>
                <td align="right">false</td>
            </tr>
            <tr>
                <td align="left">currencyId</td>
                <td>CurrencyModel</td>
                <td>see Common Data document</td>
                <td>N</td>
                <td align="right">from auction</td>
            </tr>
            <tr>
                <td align="left">vendorId</td>
                <td>Long</td>
                <td>Valid bidlogix vendor ID. If not supplied, it will default to first Vendor available alphabetically.</td>
                <td>Y</td>
                <td align="right">first defined</td>
            </tr>
            <tr>
                <td align="left">categoryId</td>
                <td>Long</td>
                <td>Valid bidlogix category ID</td>
                <td>Y</td>
                <td align="right">first defined</td>
            </tr>
            <tr>
                <td align="left">charges</td>
                <td>Big Decimal</td>
                <td>-</td>
                <td>N</td>
                <td align="right">0</td>
            </tr>
            <tr>
                <td align="left">buyersPremium</td>
                <td>Big Decimal</td>
                <td>-</td>
                <td>N</td>
                <td align="right">from auction</td>
            </tr>
            <tr>
                <td align="left">chargesTaxRate</td>
                <td>Big Decimal</td>
                <td>-</td>
                <td>N</td>
                <td align="right">from auction</td>
            </tr>
            <tr>
                <td align="left">hammerTaxRate</td>
                <td>Big Decimal</td>
                <td>-</td>
                <td>N</td>
                <td align="right">from auction</td>
            </tr>
            <tr>
                <td align="left">bpTaxRate</td>
                <td>Big Decimal</td>
                <td>-</td>
                <td>N</td>
                <td align="right">from auction</td>
            </tr>
            <tr>
                <td align="left">purchaseOption</td>
                <td>String</td>
                <td>Listing: 'NONE', 'MAKE_OFFER', 'BUY_NOW', 'TRANSFER', 'TENDER', 'TENDER_PCT'. Lot: 'NONE', 'MAKE_OFFER', 'BUY_NOW'. Webcast: ‘NONE’</td>
                <td></td>
                <td align="right">-</td>
            </tr>
            <tr>
                <td align="left">minimumAcceptableOffer</td>
                <td>Long</td>
                <td>Applies only if purchaseOption=MAKE_OFFER or BUY_NOW. If purchaseOption=MAKE_OFFER then offers including and below this amount will be automatically rejected. If purchaseOption=BUY_NOW then this field represents the buy now purchase price.</td>
                <td>Y for BUY_NOW</td>
                <td align="right">-</td>
            </tr>
            <tr>
                <td align="left">displayPrice</td>
                <td>Long</td>
                <td>An optional value to display on listings with a private treaty, or items for TRANSFER.</td>
                <td></td>
                <td align="right">-</td>
            </tr>
            <tr>
                <td align="left">unitOfMeasurement</td>
                <td>String</td>
                <td>Required for listings with the purchase options of TENDER or TENDER_PCT. For TENDER, the value should be the unit of measurement of the quantity provided e.g. 'Tonne'. For TENDER_PCT, the value is a label describing the nature of the % value being offered e.g. 'Discount on LME spot price'.</td>
                <td>Y for TENDER, TENDER_PCT</td>
                <td align="right">-</td>
            </tr>
            <tr>
                <td align="left">openingBid</td>
                <td>Long</td>
                <td>The amount to show as the starting bid.</td>
                <td></td>
                <td align="right">0</td>
            </tr>
            <tr>
                <td align="left">bidIncrement</td>
                <td>Long</td>
                <td>The increment - this may be overridden if the auctioneer is using increment tables! Not applicable to Listings.</td>
                <td></td>
                <td align="right">10</td>
            </tr>
            <tr>
                <td align="left">reservePrice</td>
                <td>Long</td>
                <td>A price the item must fetch before being sold</td>
                <td></td>
                <td align="right">0</td>
            </tr>
            <tr>
                <td align="left">numberOfUnits</td>
                <td>Long</td>
                <td>The number of units available. Bidding is per item i.e. all units.</td>
                <td>Y for TENDER, TENDER_PCT</td>
                <td align="right">1</td>
            </tr>
            <tr>
                <td align="left">biddingType</td>
                <td>Long</td>
                <td>Can be set to per item or per lot. ‘2’ - per item, ‘3’ - per lot. Quantity must be set to more than '1' for this setting to take affect.</td>
                <td></td>
                <td align="right">per lot</td>
            </tr>
        </tbody>
    </table>

</details>   

## Examples 
<Tabs
  defaultValue="minimum"
  groupId='installation-type'
  values={[
    {label: 'Example 1 - minimum required for call', value: 'minimum'},
    {label: 'Example 2 - minimal with images', value: 'withImages'}
  ]}>
<TabItem value="minimum">

Call
``` json
{ 
 "@class":"com.bidlogix.scaffold.models.BackboneCollectionModel",
   "collection":[ 
      { 
         "type":"Listing",
         "@class":".ItemApiModel"
      },
      {
         "type":"Listing",
         "@class":".ItemApiModel"
      }
   ]
}
```
Response 
``` json
{
    "@class": ".BackboneCollectionModel",
    "collection": [
        {
            "@class": ".auctionapi.ItemApiModel",
            "id": 404914,
            "type": "Listing",
            "auctionIndex": 13,
            "auctionId": 3789,
            "auctioneerId": 3,
            "title": "Title for Item 14",
            "summary": null,
            "description": null,
            "lotNumber": "14",
            "statusId": 2,
            "endTime": 1634108400000,
            "endTimeReadable": "2021-10-13T08:00:00.000+01:00",
            "locale": null,
            "listingUuid": "be10b1b5-d6ba-42e6-ac96-2933735f08c1",
            "auctionUuid": "da87f4b5-75c0-421d-a7ab-121bdb7e2515",
            "hammerTaxRate": 20.00,
            "bpTaxRate": 20.00,
            "charges": 0,
            "chargesTaxRate": 20.00,
            "buyersPremium": 20.00,
            "displayPrice": null,
            "purchaseOption": "NONE",
            "contactSellerEnabled": false,
            "minimumAcceptableOffer": null,
            "unitOfMeasurement": null,
            "vendorId": 147,
            "vendor": {
                "@class": ".VendorModel",
                "id": 147,
                "auctioneer": null,
                "name": "Vendor name",
                "companyContact": null,
                "contacts": [],
                "contactable": false,
                "activated": false,
                "summary": null,
                "description": null,
                "timeZone": null,
                "administrators": []
            },
            "categoryId": 397,
            "category": {
                "@class": ".CategoryModel",
                "id": 397,
                "name": "Demo Category - Instruments",
                "code": null,
                "selected": false,
                "treeName": "Bidlogix Demo Tree&reg;",
                "treeId": 3,
                "parentName": "Bidlogix Demo",
                "parentId": 396,
                "children": null
            },
            "currencyId": 14,
            "currency": {
                "@class": ".CurrencyModel",
                "id": 14,
                "name": "US Dollar",
                "code": "USD",
                "selected": false,
                "symbol": "&#36;"
            },
            "openingBid": null,
            "bidIncrement": null,
            "reservePrice": null,
            "numberOfUnits": 1,
            "numberOfUnitsRemaining": null,
            "biddingType": null,
            "emailOptions": {
                "@class": ".ItemEmailOptionsApiModel",
                "sendTransferEmailNotification": null
            },
            "documents": null,
            "images": null,
            "videos": null,
            "location": {
                "@class": ".AddressModel",
                "id": 426178,
                "version": 0,
                "addressType": 0,
                "addressLine1": "Middle Street",
                "addressLine2": "",
                "addressLine3": null,
                "addressLine4": null,
                "city": "Brighton",
                "county": "",
                "postcode": "BN1 1AL",
                "countryId": 232,
                "countryCode": "uk",
                "countryName": "United Kingdom",
                "continentId": 1,
                "continentCode": "EUR",
                "latitude": "",
                "longitude": ""
            },
            "attachmentType": null,
            "attachmentUrl": null
        },
        {
            "@class": ".auctionapi.ItemApiModel",
            "id": 404915,
            "type": "Listing",
            "auctionIndex": 14,
            "auctionId": 3789,
            "auctioneerId": 3,
            "title": "Title for Item 15",
            "summary": null,
            "description": null,
            "lotNumber": "15",
            "statusId": 2,
            "endTime": 1634108400000,
            "endTimeReadable": "2021-10-13T08:00:00.000+01:00",
            "locale": null,
            "listingUuid": "2a562c06-c99c-41c5-8dd9-cdc342232bd9",
            "auctionUuid": "da87f4b5-75c0-421d-a7ab-121bdb7e2515",
            "hammerTaxRate": 20.00,
            "bpTaxRate": 20.00,
            "charges": 0,
            "chargesTaxRate": 20.00,
            "buyersPremium": 20.00,
            "displayPrice": null,
            "purchaseOption": "NONE",
            "contactSellerEnabled": false,
            "minimumAcceptableOffer": null,
            "unitOfMeasurement": null,
            "vendorId": 147,
            "vendor": {
                "@class": ".VendorModel",
                "id": 147,
                "auctioneer": null,
                "name": "Test Category",
                "companyContact": null,
                "contacts": [],
                "contactable": false,
                "activated": false,
                "summary": null,
                "description": null,
                "timeZone": null,
                "administrators": []
            },
            "categoryId": 397,
            "category": {
                "@class": ".CategoryModel",
                "id": 397,
                "name": "Demo Category - Instruments",
                "code": null,
                "selected": false,
                "treeName": "Bidlogix Demo Tree&reg;",
                "treeId": 3,
                "parentName": "Bidlogix Demo",
                "parentId": 396,
                "children": null
            },
            "currencyId": 14,
            "currency": {
                "@class": ".CurrencyModel",
                "id": 14,
                "name": "US Dollar",
                "code": "USD",
                "selected": false,
                "symbol": "&#36;"
            },
            "openingBid": null,
            "bidIncrement": null,
            "reservePrice": null,
            "numberOfUnits": 1,
            "numberOfUnitsRemaining": null,
            "biddingType": null,
            "emailOptions": {
                "@class": ".ItemEmailOptionsApiModel",
                "sendTransferEmailNotification": null
            },
            "documents": null,
            "images": null,
            "videos": null,
            "location": {
                "@class": ".AddressModel",
                "id": 426178,
                "version": 0,
                "addressType": 0,
                "addressLine1": "Middle Street",
                "addressLine2": "",
                "addressLine3": null,
                "addressLine4": null,
                "city": "Brighton",
                "county": "",
                "postcode": "BN1 1AL",
                "countryId": 232,
                "countryCode": "uk",
                "countryName": "United Kingdom",
                "continentId": 1,
                "continentCode": "EUR",
                "latitude": "",
                "longitude": ""
            },
            "attachmentType": null,
            "attachmentUrl": null
        }
    ]
}
```

</TabItem>

<TabItem value="withImages">

Call 
``` json
{
  "@class": "com.bidlogix.scaffold.models.BackboneCollectionModel",
  "collection": [
    {
      "type": "Listing",
      "@class": ".ItemApiModel",
      "images": [
        {
          "@class": ".UrlAttachmentApiModel",
          "url": "full image url"
        },
        {
          "@class": ".UrlAttachmentApiModel",
          "url": "full image url"
        }
      ]
    }
  ]
}
```

Response 
``` json
{
    "@class": ".BackboneCollectionModel",
    "collection": [
        {
            "@class": ".auctionapi.ItemApiModel",
            "id": 404916,
            "type": "Listing",
            "auctionIndex": 15,
            "auctionId": 3789,
            "auctioneerId": 3,
            "title": "Title for Item 16",
            "summary": null,
            "description": null,
            "lotNumber": "16",
            "statusId": 2,
            "endTime": 1634108400000,
            "endTimeReadable": "2021-10-13T08:00:00.000+01:00",
            "locale": null,
            "listingUuid": "ba179ba8-43d1-46dd-824c-880c96decac9",
            "auctionUuid": "da87f4b5-75c0-421d-a7ab-121bdb7e2515",
            "hammerTaxRate": 20.00,
            "bpTaxRate": 20.00,
            "charges": 0,
            "chargesTaxRate": 20.00,
            "buyersPremium": 20.00,
            "displayPrice": null,
            "purchaseOption": "NONE",
            "contactSellerEnabled": false,
            "minimumAcceptableOffer": null,
            "unitOfMeasurement": null,
            "vendorId": 147,
            "vendor": {
                "@class": ".VendorModel",
                "id": 147,
                "auctioneer": null,
                "name": "Vendor name",
                "companyContact": null,
                "contacts": [],
                "contactable": false,
                "activated": false,
                "summary": null,
                "description": null,
                "timeZone": null,
                "administrators": []
            },
            "categoryId": 397,
            "category": {
                "@class": ".CategoryModel",
                "id": 397,
                "name": "Demo Category - Instruments",
                "code": null,
                "selected": false,
                "treeName": "Bidlogix Demo Tree&reg;",
                "treeId": 3,
                "parentName": "Bidlogix Demo",
                "parentId": 396,
                "children": null
            },
            "currencyId": 14,
            "currency": {
                "@class": ".CurrencyModel",
                "id": 14,
                "name": "US Dollar",
                "code": "USD",
                "selected": false,
                "symbol": "&#36;"
            },
            "openingBid": null,
            "bidIncrement": null,
            "reservePrice": null,
            "numberOfUnits": 1,
            "numberOfUnitsRemaining": null,
            "biddingType": null,
            "emailOptions": {
                "@class": ".ItemEmailOptionsApiModel",
                "sendTransferEmailNotification": null
            },
            "documents": null,
            "images": [
                {
                    "@class": ".UrlAttachmentApiModel",
                    "type": null,
                    "url": "full image url",
                    "label": null,
                    "publicId": null,
                    "errInfo": null
                },
                {
                    "@class": ".UrlAttachmentApiModel",
                    "type": null,
                    "url": "full image url",
                    "label": null,
                    "publicId": null,
                    "errInfo": null
                }
            ],
            "videos": null,
            "location": {
                "@class": ".AddressModel",
                "id": 426178,
                "version": 0,
                "addressType": 0,
                "addressLine1": "Middle Street",
                "addressLine2": "",
                "addressLine3": null,
                "addressLine4": null,
                "city": "Brighton",
                "county": "",
                "postcode": "BN1 1AL",
                "countryId": 232,
                "countryCode": "uk",
                "countryName": "United Kingdom",
                "continentId": 1,
                "continentCode": "EUR",
                "latitude": "",
                "longitude": ""
            },
            "attachmentType": null,
            "attachmentUrl": null
        }
    ]
}
```
</TabItem>
</Tabs>

## Errors 
Errors are handled where possible by returning an ErrorModel with the field 'failed' set to true. The error model will contain a detailed message indicating the first errored field. The errors are generally related to incorrect types being sent in the fields above. For example the following fails because a 'string' value is sent in the 'charges' field which should contain a big decimal value.


For example sending the following body:
``` json
{ 
    "@class":"com.bidlogix.scaffold.models.BackboneCollectionModel",
    "collection":[ 
        { 
            "type":"Listing",
            "@class":".ItemApiModel",
            "charges":"dfsdf"
        }
    ]
}
```

Yields the following response: 
``` json
{
    "message": "Error message string",
    "status": "Status code string"
}
```
Along with **Status Code: 400 (Bad Request)**

:::note Notes
A 400 error response will be returned if the lot numbers sent already exist in the system (with the duplicates listed in the body of the error response).
Webcast items must include an end time that is set **after** the start date/time for the live webcast. If the end date/time is not entered for a webcast item, it will default to the start date/time for the auction, meaning it will instantly close on auction day and bidders will not be able to bid on these items. 
Also note that the **Contact Seller** response will always be **'false'**, even if the contactSeller parameter has been correctly set to **'true'**.
Finally, note that if the auction has staggering end times enabled, items created via API will **not** respect this. Staggering of items must be set per item if created via API. 
:::

## Attachments 
[Create Items Example Call](https://drive.google.com/file/d/1zo36vmDRcLi3ugYbrVArBjxa01_COd03/view?usp=sharing)

