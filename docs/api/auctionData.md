---
id: auction-data
title: Auctions List
sidebar_label: Auctions List
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

## Description 
The Auctions List call reports on auction reference data within the Bidlogix system. For example, this will provide a list of auctions that are currently live in bidlogix, including the number of items in any auction, its start time, default currency, etc. 

The API must be structured correctly and have the correct security in place. See our document **Troubleshooting**. 

See our documents **Status Codes** and **Common Data** for more information on results. 

## Making a request

 ### URL 
 ```
https://your_bidlogix_domain/auction-mgt/bdxapi/auctions/{auctioneerId}[/{type}]?clientId=your_bidlogix_client_id
 ```

### HTTP Method
```
GET
```

### Headers 
| Key          |                  Value |
| :----------- | ---------------------: |
| BDXAPI_NAME  | Bidlogix API Signature |
| Content-Type |       application/json |


### Path Parameters 
| Parameter |                                          Value |
| :-------- | ---------------------------------------------: |
| auctioneerId | ID of the auction you wish to create items for |
| type | optional parameter for auction type i.e. one of [timed, webcast, market] |
| clientId | your bidlogix client ID |

Also see [API Introduction](./general%20info/apiInfo.md).


## Examples 
<Tabs
  defaultValue="minimum"
  groupId='installation-type'
  values={[
    {label: 'Example 1', value: 'minimum'},
    {label: 'Example 2 (with type included)', value: 'withType'}
  ]}>
<TabItem value="minimum">

Call
``` 
https://hove.eu-west-2.bidjs.com/auction-mgt/bdxapi/auctions/3?clientId=302
```
Response 
``` json
{
    "@class": ".DataPushPayload",
    "pushCode": "AUCTION_REFERENCE",
    "models": {
        "auctionReferenceModel": {
            "@class": ".BackboneCollectionModel",
            "collection": [
                {
                    "@class": ".auctionapi.AuctionApiModel",
                    "id": 5,
                    "title": "Marketplace",
                    "type": "MarketAuction",
                    "auctioneer": {
                        "@class": ".AuctioneerApiModel",
                        "id": 3,
                        "name": "Demonstration Auctioneer",
                        "liveAuctionIds": []
                    },
                    "currency": {
                        "id": 2,
                        "symbol": "&#163;",
                        "code": "GBP",
                        "name": "British Pound Sterling",
                        "selected": false
                    },
                    "itemCount": 165,
                    "statusId": 4,
                    "auctionStartTime": 1536588000000,
                    "primaryDate": 4692970800000,
                    "locale": "1_302",
                    "live": false,
                    "webcasty": false,
                    "marketplace": true,
                    "salesTaxRequired": false,
                    "buyersPremiumRequired": true,
                    "hammerTaxRate": 0.00,
                    "bpTaxRate": 0.00,
                    "chargesTaxRate": 0.00,
                    "autoIncrementsActiveForTimedAuctions": true,
                    "autoIncrementsActiveForWebcast": false,
                    "location": {
                        "@class": ".AddressModel",
                        "id": 20,
                        "version": 0,
                        "addressType": 0,
                        "addressLine1": "",
                        "addressLine2": "",
                        "addressLine3": null,
                        "addressLine4": null,
                        "city": "",
                        "county": "",
                        "postcode": "",
                        "countryId": 232,
                        "countryCode": "uk",
                        "countryName": "United Kingdom",
                        "continentId": 1,
                        "continentCode": "EUR",
                        "latitude": "",
                        "longitude": ""
                    },
                    "items": null,
                    "uuid": "7392d921-4df9-40b3-b3cf-f403635b9db0"
                },
                {
                    "@class": ".auctionapi.AuctionApiModel",
                    "id": 1905,
                    "title": "Tractors",
                    "type": "EventAuction",
                    "auctioneer": {
                        "@class": ".AuctioneerApiModel",
                        "id": 3,
                        "name": "Demonstration Auctioneer",
                        "liveAuctionIds": []
                    },
                    "currency": {
                        "id": 2,
                        "symbol": "&#163;",
                        "code": "GBP",
                        "name": "British Pound Sterling",
                        "selected": false
                    },
                    "itemCount": 0,
                    "statusId": 2,
                    "auctionStartTime": 1607083200000,
                    "primaryDate": 1608120000000,
                    "locale": "1_302",
                    "live": false,
                    "webcasty": false,
                    "marketplace": false,
                    "salesTaxRequired": true,
                    "buyersPremiumRequired": true,
                    "hammerTaxRate": 20.00,
                    "bpTaxRate": 20.00,
                    "chargesTaxRate": 20.00,
                    "autoIncrementsActiveForTimedAuctions": true,
                    "autoIncrementsActiveForWebcast": false,
                    "location": {
                        "@class": ".AddressModel",
                        "id": 203623,
                        "version": 0,
                        "addressType": 0,
                        "addressLine1": "69 Middle Street",
                        "addressLine2": "",
                        "addressLine3": null,
                        "addressLine4": null,
                        "city": "Brighton",
                        "county": "",
                        "postcode": "BN1 1AL",
                        "countryId": 232,
                        "countryCode": "uk",
                        "countryName": "United Kingdom",
                        "continentId": 1,
                        "continentCode": "EUR",
                        "latitude": "",
                        "longitude": ""
                    },
                    "items": null,
                    "uuid": "7392d921-4df9-40b3-b3cf-f403635b9db1"
                },
                {
                    "@class": ".auctionapi.AuctionApiModel",
                    "id": 3769,
                    "title": "Demo Timed Car Auction October",
                    "type": "EventAuction",
                    "auctioneer": {
                        "@class": ".AuctioneerApiModel",
                        "id": 3,
                        "name": "Demonstration Auctioneer",
                        "liveAuctionIds": []
                    },
                    "currency": {
                        "id": 2,
                        "symbol": "&#163;",
                        "code": "GBP",
                        "name": "British Pound Sterling",
                        "selected": false
                    },
                    "itemCount": 53,
                    "statusId": 4,
                    "auctionStartTime": 1633351500000,
                    "primaryDate": 1638277200000,
                    "locale": "1_302",
                    "live": false,
                    "webcasty": false,
                    "marketplace": false,
                    "salesTaxRequired": true,
                    "buyersPremiumRequired": true,
                    "hammerTaxRate": 20.00,
                    "bpTaxRate": 20.00,
                    "chargesTaxRate": 20.00,
                    "autoIncrementsActiveForTimedAuctions": true,
                    "autoIncrementsActiveForWebcast": false,
                    "location": {
                        "@class": ".AddressModel",
                        "id": 425263,
                        "version": 0,
                        "addressType": 0,
                        "addressLine1": "",
                        "addressLine2": "",
                        "addressLine3": null,
                        "addressLine4": null,
                        "city": "",
                        "county": "",
                        "postcode": "",
                        "countryId": 232,
                        "countryCode": "uk",
                        "countryName": "United Kingdom",
                        "continentId": 1,
                        "continentCode": "EUR",
                        "latitude": "",
                        "longitude": ""
                    },
                    "items": null,
                    "uuid": "7392d921-4df9-40b3-b3cf-f403635b9db2"
                },
                {
                    "@class": ".auctionapi.AuctionApiModel",
                    "id": 3861,
                    "title": "test training auction",
                    "type": "WebcastAuction",
                    "auctioneer": {
                        "@class": ".AuctioneerApiModel",
                        "id": 3,
                        "name": "Demonstration Auctioneer",
                        "liveAuctionIds": []
                    },
                    "currency": {
                        "id": 2,
                        "symbol": "&#163;",
                        "code": "GBP",
                        "name": "British Pound Sterling",
                        "selected": false
                    },
                    "itemCount": 4,
                    "statusId": 4,
                    "auctionStartTime": 1634724000000,
                    "primaryDate": 1634724000000,
                    "locale": "1_302",
                    "live": false,
                    "webcasty": true,
                    "marketplace": false,
                    "salesTaxRequired": true,
                    "buyersPremiumRequired": true,
                    "hammerTaxRate": 20.00,
                    "bpTaxRate": 20.00,
                    "chargesTaxRate": 20.00,
                    "autoIncrementsActiveForTimedAuctions": true,
                    "autoIncrementsActiveForWebcast": false,
                    "location": {
                        "@class": ".AddressModel",
                        "id": 431283,
                        "version": 0,
                        "addressType": 0,
                        "addressLine1": "Middle Street",
                        "addressLine2": "",
                        "addressLine3": null,
                        "addressLine4": null,
                        "city": "Brighton",
                        "county": "",
                        "postcode": "BN1 1AL",
                        "countryId": 232,
                        "countryCode": "uk",
                        "countryName": "United Kingdom",
                        "continentId": 1,
                        "continentCode": "EUR",
                        "latitude": "",
                        "longitude": ""
                    },
                    "items": null,
                    "uuid": "7392d921-4df9-40b3-b3cf-f403635b9db3"
                },
                {
                    "@class": ".auctionapi.AuctionApiModel",
                    "id": 3876,
                    "title": "Demo Webcast",
                    "type": "WebcastAuction",
                    "auctioneer": {
                        "@class": ".AuctioneerApiModel",
                        "id": 3,
                        "name": "Demonstration Auctioneer",
                        "liveAuctionIds": []
                    },
                    "currency": {
                        "id": 2,
                        "symbol": "&#163;",
                        "code": "GBP",
                        "name": "British Pound Sterling",
                        "selected": false
                    },
                    "itemCount": 50,
                    "statusId": 4,
                    "auctionStartTime": 1634670000000,
                    "primaryDate": 1634670000000,
                    "locale": "1_302",
                    "live": false,
                    "webcasty": true,
                    "marketplace": false,
                    "salesTaxRequired": true,
                    "buyersPremiumRequired": true,
                    "hammerTaxRate": 20.00,
                    "bpTaxRate": 20.00,
                    "chargesTaxRate": 20.00,
                    "autoIncrementsActiveForTimedAuctions": true,
                    "autoIncrementsActiveForWebcast": false,
                    "location": {
                        "@class": ".AddressModel",
                        "id": 436733,
                        "version": 0,
                        "addressType": 0,
                        "addressLine1": "",
                        "addressLine2": "",
                        "addressLine3": null,
                        "addressLine4": null,
                        "city": "",
                        "county": "",
                        "postcode": "",
                        "countryId": 232,
                        "countryCode": "uk",
                        "countryName": "United Kingdom",
                        "continentId": 1,
                        "continentCode": "EUR",
                        "latitude": "",
                        "longitude": ""
                    },
                    "items": null,
                    "uuid": "7392d921-4df9-40b3-b3cf-f403635b9db4"
                }
            ]
        }
    },
    "webAppContext": "302"
}
```

</TabItem>

<TabItem value="withType">

Call 
``` 
https://hove.eu-west-2.bidjs.com/auction-mgt/bdxapi/auctions/3/market?clientId=302
```

Response 
``` json
{
    "@class": ".DataPushPayload",
    "pushCode": "AUCTION_REFERENCE",
    "models": {
        "auctionReferenceModel": {
            "@class": ".BackboneCollectionModel",
            "collection": [
                {
                    "@class": ".auctionapi.AuctionApiModel",
                    "id": 5,
                    "title": "Marketplace",
                    "type": "MarketAuction",
                    "auctioneer": {
                        "@class": ".AuctioneerApiModel",
                        "id": 3,
                        "name": "Demonstration Auctioneer",
                        "liveAuctionIds": []
                    },
                    "currency": {
                        "id": 2,
                        "symbol": "&#163;",
                        "code": "GBP",
                        "name": "British Pound Sterling",
                        "selected": false
                    },
                    "itemCount": 165,
                    "statusId": 4,
                    "auctionStartTime": 1536588000000,
                    "primaryDate": 4692970800000,
                    "locale": "1_302",
                    "live": false,
                    "webcasty": false,
                    "marketplace": true,
                    "salesTaxRequired": false,
                    "buyersPremiumRequired": true,
                    "hammerTaxRate": 0.00,
                    "bpTaxRate": 0.00,
                    "chargesTaxRate": 0.00,
                    "autoIncrementsActiveForTimedAuctions": true,
                    "autoIncrementsActiveForWebcast": false,
                    "location": {
                        "@class": ".AddressModel",
                        "id": 20,
                        "version": 0,
                        "addressType": 0,
                        "addressLine1": "",
                        "addressLine2": "",
                        "addressLine3": null,
                        "addressLine4": null,
                        "city": "",
                        "county": "",
                        "postcode": "",
                        "countryId": 232,
                        "countryCode": "uk",
                        "countryName": "United Kingdom",
                        "continentId": 1,
                        "continentCode": "EUR",
                        "latitude": "",
                        "longitude": ""
                    },
                    "items": null,
                    "uuid": "7392d921-4df9-40b3-b3cf-f403635b9db5"
                }
            ]
        }
    },
    "webAppContext": "302"
}
```
</TabItem>
</Tabs>

## Errors 
| code | example |
| :--- | -------: |
| 401 UNAUTHORIZED | \{ error : "Log in" \} |
| 422 Unprocessable Entry | \{ error : ["\<field_name\>"] \} |

