---
id: update-items-by-uuid
title: Items Full Update 
sidebar_label: Items Full Update
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

## Description 
Edit most item fields no matter what status the item is in.

## Making a request

 ### URL 
 ```
/v2/items/{itemUuid}
 ```

### HTTP Method
```
PATCH
```

### Headers 
| Key          |                  Value |
| :----------- | ---------------------: |
| BDXAPI_NAME  | Bidlogix API Signature |
| Content-Type |       application/json |


### Path Parameters 
| Parameter |                                          Value |
| :-------- | ---------------------------------------------: |
| itemUuid | UUID of the item you wish to update details for |

Also see [API Introduction](./general%20info/apiInfo.md).

### API Model Request  

<details> 
    <summary> All fields </summary>
    <table>
        <thead>
            <tr>
                <th align="left">Name</th>
                <th>Type</th>
                <th>Value Range</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td align="left">title</td>
                <td>String</td>
                <td>The title of the item - required for all items</td>
            </tr>
            <tr>
                <td align="left">summary</td>
                <td>String</td>
                <td>A short summary of the item or alternate information such as estimate of the sale price.</td>
            </tr>
            <tr>
                <td align="left">description</td>
                <td>String</td>
                <td>A full description displayed on the item details page.</td>
            </tr>
            <tr>
                <td align="left">lotNumber</td>
                <td>String</td>
                <td>Lot numbers cannot contain duplicate values.</td>
            </tr>
            <tr>
                <td align="left">purchaseOption</td>
                <td>String</td>
                <td>Listing: 'NONE', 'MAKE_OFFER', 'BUY_NOW', 'TRANSFER', 'TENDER', 'TENDER_PCT'. Lot: 'NONE', 'MAKE_OFFER', 'BUY_NOW'. Webcast: ‘NONE’</td>
            </tr>
            <tr>
                <td align="left">unitOfMeasurement</td>
                <td>String</td>
                <td>Required for listings with the purchase options of TENDER or TENDER_PCT. For TENDER, the value should be the unit of measurement of the quantity provided e.g. 'Tonne'. For TENDER_PCT, the value is a label describing the nature of the % value being offered e.g. 'Discount on LME spot price'.</td>
            </tr>
            <tr>
                <td align="left">endTime</td>
                <td>String</td>
                <td>Must be in milliseconds.</td>
            </tr>
            <tr>
                <td align="left">displayPrice</td>
                <td>Long</td>
                <td>An optional value to display on listings with a private treaty, or items for TRANSFER.</td>
            </tr>
            <tr>
                <td align="left">minimumAcceptableOffer</td>
                <td>Long</td>
                <td>Applies only if purchaseOption=MAKE_OFFER or BUY_NOW. If purchaseOption=MAKE_OFFER then offers including and below this amount will be automatically rejected. If purchaseOption=BUY_NOW then this field represents the buy now purchase price.</td>
            </tr>
            <tr>
                <td align="left">openingBid</td>
                <td>Long</td>
                <td>The amount to show as the starting bid.</td>
            </tr>
            <tr>
                <td align="left">bidIncrement</td>
                <td>Long</td>
                <td>The increment - may be ignored if the auctioneer is using increment tables! Not applicable to Listings.</td>
            </tr>
            <tr>
                <td align="left">reservePrice</td>
                <td>Long</td>
                <td>A price the item must fetch before being sold</td>
            </tr>
            <tr>
                <td align="left">biddingType</td>
                <td>Long</td>
                <td>Can be set to ONE_MONEY, PER_ITEM or PER_LOT. Quantity must be set to more than '1' for PER_ITEM to take affect.</td>
            </tr>
            <tr>
                <td align="left">buyersPremium</td>
                <td>BigDecimal</td>
                <td>0 - 100</td>
            </tr>
            <tr>
                <td align="left">charges</td>
                <td>Big Decimal</td>
                <td>0 - 100</td>
            </tr>
            <tr>
                <td align="left">chargesTaxRate</td>
                <td>Big Decimal</td>
                <td>0 - 100</td>
            </tr>
            <tr>
                <td align="left">hammerTaxRate</td>
                <td>Big Decimal</td>
                <td>0 - 100</td>
            </tr>
            <tr>
                <td align="left">bpTaxRate</td>
                <td>Big Decimal</td>
                <td>0 - 100</td>
            </tr>
            <tr>
                <td align="left">numberOfUnits</td>
                <td>BigDecimal</td>
                <td>The number of units available. Bidding is per item i.e. all units.</td>
            </tr>
            <tr>
                <td align="left">numberOfUnitsRemaining</td>
                <td>BigDecimal</td>
                <td>The number of units remaining. Applies to BulkBuyItem only.</td>
            </tr>
            <tr>
                <td align="left">contactSellerEnabled</td>
                <td>Boolean</td>
                <td>Enables the contact vendor feature per item.</td>
            </tr>
            <tr>
                <td align="left">publicVisible</td>
                <td>Boolean</td>
                <td>Controls item visibility in the auction.</td>
            </tr>
            <tr>
                <td align="left">status</td>
                <td>String</td>
                 <td>Item status, can be either DRAFT, LIVE or WITHDRAWN.</td>
            </tr>
            <tr>
                <td align="left">location</td>
                <td>AddressModel</td>
                <td>see Common Data document</td>
            </tr>
            <tr>
                <td align="left">currencyUuid</td>
                <td>UUID</td>
                <td>Valid currency UUID</td>
            </tr>
            <tr>
                <td align="left">vendorUuid</td>
                <td>UUID</td>
                <td>Valid vendor UUID.</td>
            </tr>
            <tr>
                <td align="left">categoryUuid</td>
                <td>UUID</td>
                <td>Valid category UUID</td>
            </tr>
        </tbody>
    </table>
</details>   

### Examples
<Tabs
  defaultValue="minimum"
  groupId='installation-type'
  values={[
    {label: 'Example 1 - all fields changed', value: 'minimum'}
  ]}>
<TabItem value="minimum">

Call
``` json
{
    "title": "Update Listing Test - New Title",
    "summary": "Update Listing Test - New Summary",
    "description": "Update Listing Test - New Description",
    "lotNumber": "1a",
    "purchaseOption": "NONE",
    "unitOfMeasurement": "beans",
    "endTime": 32503680000000,
    "displayPrice": 10,
    "minimumAcceptableOffer": 11,
    "openingBid": 12,
    "bidIncrement": 2,
    "reservePrice": 13,
    "biddingType": "PER_LOT",
    "buyersPremium": 10,
    "bpTaxRate": 11,
    "charges": 12,
    "chargesTaxRate": 13,
    "hammerTaxRate": 14,
    "numberOfUnits": 15,
    "numberOfUnitsRemaining": 2,
    "contactSellerEnabled": true,
    "publicVisible": true,
    "status": "LIVE",
    "location": {
        "addressLine1": "new listing location line 1",
        "addressLine2": "new listing location line 2",
        "city": "new listing location city",
        "county": "new listing location county",
        "postcode": "new listing location postcode",
        "latitude": "new listing location latitude",
        "longitude": "new listing location longitude",
        "countryCode": "UK"
    },
    "vendorUuid": "97fdce91-8ac0-11ee-ab70-0242c0a88005",
    "categoryUuid": "97fc7dc0-8ac0-11ee-ab70-0242c0a88005",
    "currencyUuid": "999d5d83-8ac0-11ee-ab70-0242c0a88005"
}
```
Response 
``` json
{
    "message": "Item with uuid 2b22bfc9-8947-11ee-9a87-0242ac150002 has been successfully updated"
}
```

</TabItem>

</Tabs>

## Errors 
Errors are handled where possible by returning an ErrorModel with the field 'failed' set to true. The error model will contain a detailed message indicating the first errored field. The errors are generally related to incorrect types being sent in the fields above. For example the following fails because a 'string' value is sent in the 'charges' field which should contain a big decimal value.

### Examples
<Tabs
  defaultValue="minimum"
  groupId='installation-type'
  values={[
    {label: 'Example 1 - unrecognised value', value: 'minimum'}
  ]}>
<TabItem value="minimum">

Call
``` json
{
    "purchaseOption": "UNRECOGNISED"
}
```
Response 
``` json
{
    "message": "Purchase Option must be one of NONE, MAKE_OFFER, BUY_NOW, TRANSFER, TENDER, TENDER_PCT"
}
```
Along with **Status Code: 400 (Bad Request)**

</TabItem>

</Tabs>

### Call Details
[Click here to see our documentation](/apiDefinitions)
