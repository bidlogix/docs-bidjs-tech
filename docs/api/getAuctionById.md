---
id: get-auction-by-id
title: Auction Get by ID
sidebar_label: Auction Get by ID
---


## Description

Get auction information using the auction ID.

## Making a request

### URL

 ```bash
/auctions/{auctionId}
 ```

### HTTP Method

```bash
GET
```

### Headers

| Key          |                  Value |
| :----------- | ---------------------: |
| BDXAPI_NAME  | Bidlogix API Signature |
| Content-Type |       application/json |

### Path Parameters

| Parameter |                                          Value |
| :-------- | ---------------------------------------------: |
| auctionId      | The auction ID for the given auction      |

### Call Details

[Click here to see our documentation](/apiDefinitions)

:::note Notes
This call will retrieve all information for the given auction. This does not return details of items within the auction.

:::
