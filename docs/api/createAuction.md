---
id: create-auction
title: Auction Create
sidebar_label: Auction Create
---

## Description 
Create a timed or webcast auction within the Bidlogix system. 

## Making a request

 ### URL 
 ```
/auctions
 ```

### HTTP Method
```
POST
```

### Headers 
| Key          |                  Value |
| :----------- | ---------------------: |
| BDXAPI_NAME  | Bidlogix API Signature |
| Content-Type |       application/json |



### Call Details
[Click here to see our documentation](/apiDefinitions)

