---
id: get-sales-values
title: Sales Value Report
sidebar_label: Sales Value Report
---


## Description 
Get the item sales value report for the given item(s).   

## Making a request

 ### URL 
 ```
/reports/itemsalesvalue
 ```

### HTTP Method
```
POST
```

### Headers 
| Key          |                  Value |
| :----------- | ---------------------: |
| BDXAPI_NAME  | Bidlogix API Signature |
| Content-Type |       application/json |


### Path Parameters 
None. The item(s) to report are set within the call body. 


### Call Details
[Click here to see our documentation](/apiDefinitions)

:::note Notes
This call will retrieve the sale value for all items listed within the body of the call as a single value. This call does not return individual sale values for each item. 

:::