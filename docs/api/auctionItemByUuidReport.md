---
id: item-uuid-report
title: Items Report By UUID
sidebar_label: Items Report By UUID
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

## Description
The auction Items Report by UUID call reports details on specific item(s). This includes information such as the item's category id, title and lot number, end time, etc.

The API must be structured correctly and have the correct security in place. See our document **Troubleshooting**.

See our documents **Status Codes** and **Common Data** for more information on results.

:::note Please Note
Items Report by UUID is limited to 97 UUIDs in the URL when requesting multiple items.
:::

## Making a request

### URL
 ```
/reports/items/{clientId}/{itemUuids}
 ```

### HTTP Method
```
GET
```

### Headers
| Key          |                  Value |
|:-------------| ---------------------: |
| bdxapi_name  | Bidlogix API Signature |
| Content-Type |       application/json |

### Path Parameters and Adaptations
| Parameter   |                                                                                       Value |
|:------------|--------------------------------------------------------------------------------------------:|
| clientId    |                                                                     your bidlogix Client ID |
| itemUuids   |                                    UUID of the item(s) you wish to retrieve information for |

### Optional Query Parameters
| Parameter     |                                                                                       Value |
|:--------------|--------------------------------------------------------------------------------------------:|
| includeImages |                              this is set to `true` or `false` to enable image URL retrieval |


Fetch reports for multiple items by providing a comma seperated set of UUIDs in the itemUuids path parameter

Appending the query parameter: `?includeImages=true` to the end of the request, will result in a response that includes URLs to the images of the items. This will include the cloudinary image URLs in the resulting report.


Also see [API Introduction](./general%20info/apiInfo.md).

## Examples
<Tabs
defaultValue="minimum"
groupId='installation-type'
values={[
{label: 'Example 1 - single item', value: 'minimum'},
{label: 'Example 2 - multiple items', value: 'multiMinimum'},
{label: 'Example 3 - single item with images', value: 'withImages'},
{label: 'Example 4 - multiple items with images', value: 'multiWithImages'}
]}>
<TabItem value="minimum">

Call
``` 
https://api.eu-west-2.staging.bidjs.com/reports/items/302/375608a7-98fd-425f-a686-362eb4f8fe18
```
Response
``` json
{
  "message": {
    "@class": ".DataPushPayload",
    "pushCode": "REPORTING_AUCTION",
    "models": {
      "auctionReport": {
        "@class": ".reporting.ItemsMinimalReportModel",
        "reportMeta": {
          "@class": ".ReportMetaModel",
          "reportType": 5,
          "reportObject": "ItemsMinimalReportModel",
          "reportMade": 1700734552498
        },
        "reportTitle": "Items Report false",
        "items": [
          {
            "@class": ".ItemMinimalReportModel",
            "id": 919689,
            "title": "Car 1 &ndash; none",
            "lotNumber": "1",
            "imageUrls": null,
            "purchaseOption": "None",
            "currencyId": 2,
            "unitOfMeasurement": "",
            "numberOfUnits": 1,
            "endTime": 1701327600000,
            "actualEndTime": null,
            "status": 4,
            "derivedStatus": 4,
            "categoryId": 546,
            "categoryUuid": "88b7962f-9350-11ea-9b07-0aad12b14bdc",
            "vendorId": 3,
            "vendorUuid": "8a72f15f-9350-11ea-9b07-0aad12b14bdc",
            "charges": 0,
            "uuid": "375608a7-98fd-425f-a686-362eb4f8fe18"
          }
        ]
      }
    },
    "webAppContext": "302"
  }
}
```

</TabItem>

<TabItem value="multiMinimum">

Call
``` 
https://api.eu-west-2.staging.bidjs.com/reports/items/302/375608a7-98fd-425f-a686-362eb4f8fe18,4d2625ad-1bfd-46a8-93fc-408506763563
```

Response
``` json
{
  "message": {
    "@class": ".DataPushPayload",
    "pushCode": "REPORTING_AUCTION",
    "models": {
      "auctionReport": {
        "@class": ".reporting.ItemsMinimalReportModel",
        "reportMeta": {
          "@class": ".ReportMetaModel",
          "reportType": 5,
          "reportObject": "ItemsMinimalReportModel",
          "reportMade": 1700735406356
        },
        "reportTitle": "Items Report false",
        "items": [
          {
            "@class": ".ItemMinimalReportModel",
            "id": 919689,
            "title": "Car 1 &ndash; none",
            "lotNumber": "1",
            "imageUrls": null,
            "purchaseOption": "None",
            "currencyId": 2,
            "unitOfMeasurement": "",
            "numberOfUnits": 1,
            "endTime": 1701327600000,
            "actualEndTime": null,
            "status": 4,
            "derivedStatus": 4,
            "categoryId": 546,
            "categoryUuid": "88b7962f-9350-11ea-9b07-0aad12b14bdc",
            "vendorId": 3,
            "vendorUuid": "8a72f15f-9350-11ea-9b07-0aad12b14bdc",
            "charges": 0,
            "uuid": "375608a7-98fd-425f-a686-362eb4f8fe18"
          },
          {
            "@class": ".ItemMinimalReportModel",
            "id": 919694,
            "title": "Car 6 &ndash; bid increment",
            "lotNumber": "6",
            "imageUrls": null,
            "purchaseOption": "Make Offer",
            "currencyId": 2,
            "unitOfMeasurement": null,
            "numberOfUnits": 1,
            "endTime": 1701328200000,
            "actualEndTime": null,
            "status": 4,
            "derivedStatus": 7,
            "categoryId": 546,
            "categoryUuid": "88b7962f-9350-11ea-9b07-0aad12b14bdc",
            "vendorId": 3,
            "vendorUuid": "8a72f15f-9350-11ea-9b07-0aad12b14bdc",
            "charges": 0,
            "uuid": "4d2625ad-1bfd-46a8-93fc-408506763563"
          }
        ]
      }
    },
    "webAppContext": "302"
  }
}
```
</TabItem>

<TabItem value="withImages">

Call
``` 
https://api.eu-west-2.staging.bidjs.com/reports/items/302/375608a7-98fd-425f-a686-362eb4f8fe18?includeImages=true
```

Response
``` json
{
  "message": {
    "@class": ".DataPushPayload",
    "pushCode": "REPORTING_AUCTION",
    "models": {
      "auctionReport": {
        "@class": ".reporting.ItemsMinimalReportModel",
        "reportMeta": {
          "@class": ".ReportMetaModel",
          "reportType": 5,
          "reportObject": "ItemsMinimalReportModel",
          "reportMade": 1700735508699
        },
        "reportTitle": "Items Report true",
        "items": [
          {
            "@class": ".ItemMinimalReportModel",
            "id": 919689,
            "title": "Car 1 &ndash; none",
            "lotNumber": "1",
            "imageUrls": [
              "https://res.cloudinary.com/bidlogix-staging/image/upload/v1696249843/live_bdx/bdx/1_sziqhn.jpg",
              "https://res.cloudinary.com/bidlogix-staging/image/upload/v1696249843/live_bdx/bdx/1-3_ml13kr.jpg",
              "https://res.cloudinary.com/bidlogix-staging/image/upload/v1696249843/live_bdx/bdx/1-4-car44_yvunmh.jpg",
              "https://res.cloudinary.com/bidlogix-staging/image/upload/v1696249844/live_bdx/bdx/1-4-cracarcar_arf0ar.jpg"
            ],
            "purchaseOption": "None",
            "currencyId": 2,
            "unitOfMeasurement": "",
            "numberOfUnits": 1,
            "endTime": 1701327600000,
            "actualEndTime": null,
            "status": 4,
            "derivedStatus": 4,
            "categoryId": 546,
            "categoryUuid": "88b7962f-9350-11ea-9b07-0aad12b14bdc",
            "vendorId": 3,
            "vendorUuid": "8a72f15f-9350-11ea-9b07-0aad12b14bdc",
            "charges": 0,
            "uuid": "375608a7-98fd-425f-a686-362eb4f8fe18"
          }
        ]
      }
    },
    "webAppContext": "302"
  }
}
```
</TabItem>

<TabItem value="multiWithImages">

Call
``` 
https://api.eu-west-2.staging.bidjs.com/reports/items/302/375608a7-98fd-425f-a686-362eb4f8fe18,4d2625ad-1bfd-46a8-93fc-408506763563?includeImages=true
```

Response
``` json
{
  "message": {
    "@class": ".DataPushPayload",
    "pushCode": "REPORTING_AUCTION",
    "models": {
      "auctionReport": {
        "@class": ".reporting.ItemsMinimalReportModel",
        "reportMeta": {
          "@class": ".ReportMetaModel",
          "reportType": 5,
          "reportObject": "ItemsMinimalReportModel",
          "reportMade": 1700735656275
        },
        "reportTitle": "Items Report true",
        "items": [
          {
            "@class": ".ItemMinimalReportModel",
            "id": 919689,
            "title": "Car 1 &ndash; none",
            "lotNumber": "1",
            "imageUrls": [
              "https://res.cloudinary.com/bidlogix-staging/image/upload/v1696249843/live_bdx/bdx/1_sziqhn.jpg",
              "https://res.cloudinary.com/bidlogix-staging/image/upload/v1696249843/live_bdx/bdx/1-3_ml13kr.jpg",
              "https://res.cloudinary.com/bidlogix-staging/image/upload/v1696249843/live_bdx/bdx/1-4-car44_yvunmh.jpg",
              "https://res.cloudinary.com/bidlogix-staging/image/upload/v1696249844/live_bdx/bdx/1-4-cracarcar_arf0ar.jpg"
            ],
            "purchaseOption": "None",
            "currencyId": 2,
            "unitOfMeasurement": "",
            "numberOfUnits": 1,
            "endTime": 1701327600000,
            "actualEndTime": null,
            "status": 4,
            "derivedStatus": 4,
            "categoryId": 546,
            "categoryUuid": "88b7962f-9350-11ea-9b07-0aad12b14bdc",
            "vendorId": 3,
            "vendorUuid": "8a72f15f-9350-11ea-9b07-0aad12b14bdc",
            "charges": 0,
            "uuid": "375608a7-98fd-425f-a686-362eb4f8fe18"
          },
          {
            "@class": ".ItemMinimalReportModel",
            "id": 919694,
            "title": "Car 6 &ndash; bid increment",
            "lotNumber": "6",
            "imageUrls": [
              "https://res.cloudinary.com/bidlogix-staging/image/upload/v1696249845/live_bdx/bdx/6_zgzrly.jpg"
            ],
            "purchaseOption": "Make Offer",
            "currencyId": 2,
            "unitOfMeasurement": null,
            "numberOfUnits": 1,
            "endTime": 1701328200000,
            "actualEndTime": null,
            "status": 4,
            "derivedStatus": 7,
            "categoryId": 546,
            "categoryUuid": "88b7962f-9350-11ea-9b07-0aad12b14bdc",
            "vendorId": 3,
            "vendorUuid": "8a72f15f-9350-11ea-9b07-0aad12b14bdc",
            "charges": 0,
            "uuid": "4d2625ad-1bfd-46a8-93fc-408506763563"
          }
        ]
      }
    },
    "webAppContext": "302"
  }
}
```
</TabItem>
</Tabs>

## Errors
| code | example |
| :--- | -------: |
| 401 UNAUTHORIZED | \{ error : "Log in" \} |
| 422 Unprocessable Entry | \{ error : ["\<field_name\>"] \} |
| 400 BAD REQUEST | ``` json {"@class":".ErrorModel", "failed":true, "field":null, "code":"REPORTING_API", "message":"Invalid ids parameter - only numbers and commas allowed; actual value passed=invalid_argument", "loggedIn":false} ``` |

