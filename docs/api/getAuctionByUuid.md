---
id: get-auction-by-uuid
title: Auction Get by UUID
sidebar_label: Auction Get by UUID
---


## Description

Get auction information using the auction UUID.

## Making a request

### URL

 ```bash
/v2/auctions/{auctionUuid}
 ```

### HTTP Method

```bash
GET
```

### Headers

| Key          |                  Value |
| :----------- | ---------------------: |
| BDXAPI_NAME  | Bidlogix API Signature |
| Content-Type |       application/json |

### Path Parameters

| Parameter |                                          Value |
| :-------- | ---------------------------------------------: |
| auctionUuid      | The auction UUID for the given auction      |

### Call Details

[Click here to see our documentation](/apiDefinitions)

:::note Notes
This call will retrieve all information for the given auction. This does not return details of items within the auction.

:::
