---
id: auction-report
title: Auction Report
sidebar_label: Auction Report
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

## Description 
The Auction Reports call reports on bidding within a specific auction. The report shows item information including the winning bidders (winner) and under bidders (losers), item ID, category, lot number, item title, etc. 

The API must be structured correctly and have the correct security in place. See our document **Troubleshooting**. 

See our documents **Status Codes** and **Common Data** for more information on results. 

## Making a request

 ### URL 
 ```
https://your_bidlogix_domain/auction-mgt/bdxapi/reporting/auction/{auctionId}/{sorting}?clientId=your_bidlogix_client_id
 ```

### HTTP Method
```
GET
```

### Headers 
| Key          |                  Value |
| :----------- | ---------------------: |
| BDXAPI_NAME  | Bidlogix API Signature |
| Content-Type |       application/json |


### Path Parameters and Adaptations
| Parameter |                                          Value |
| :-------- | ---------------------------------------------: |
| auctionId | ID of the auction you wish to retrieve information for |
| clientId | your bidlogix Client ID |
| sorting | the field to sort the result by - only value currently supported is `category`|
| &includeImages | this is set to `true` or `false` to enable image URL retrieval |

Appending the parameter: `&includeImages=true` to the end of the request, will result in a response that includes URLs to the images of the items. This will include the cloudinary image URLs in the resulting report.

:::note Please Note
The returned `winner` field is the highest bid. **Make Offer** and **Buy Now** purchases will **not** show in the auction report results. 
There is also a situation whereby any items that are **unsold** but have a single bid that has been cancelled during the auction will **not** show in the auction report results. 
:::

Also see [API Introduction](./general%20info/apiInfo.md).


## Examples 
<Tabs
  defaultValue="minimum"
  groupId='installation-type'
  values={[
    {label: 'Example 1 - no images', value: 'minimum'},
    {label: 'Example 2 - with images', value: 'withImages'}
  ]}>
<TabItem value="minimum">

Call
``` 
https://hove.eu-west-2.bidjs.com/auction-mgt/bdxapi/reporting/auction/3861/category?clientId=302
```
Response 
``` json
{
    "@class": ".DataPushPayload",
    "pushCode": "REPORTING_AUCTION",
    "models": {
        "auctionReport": {
            "@class": ".reporting.AuctionReportModel",
            "reportMeta": {
                "@class": ".ReportMetaModel",
                "reportType": 1,
                "reportObject": "AuctionReportModel",
                "reportMade": 1634735952099
            },
            "reportTitle": "Auction Bids Report",
            "webAppId": 302,
            "auctionId": 3861,
            "auctionUuid": "8892d921-4df9-40b3-b3cf-f403635b9db5",
            "auctionTitle": "test training auction",
            "auctionDescription": "description goes here",
            "items": [
                {
                    "@class": ".ItemReportModel",
                    "category": {
                        "@class": ".CategoryModel",
                        "id": 545,
                        "name": "Demo Category - Antiques",
                        "code": null,
                        "selected": false,
                        "treeName": "Bidlogix Demo Tree&reg;",
                        "treeId": 3,
                        "parentName": "Bidlogix Demo",
                        "parentId": 396,
                        "children": null
                    },
                    "id": 401264,
                    "title": "Title goes here",
                    "lotNumber": "3",
                    "imageUrls": null,
                    "purchaseOption": "NONE",
                    "currencyId": 2,
                    "unitOfMeasurement": null,
                    "numberOfUnits": null,
                    "endTime": 1635242400000,
                    "actualEndTime": null,
                    "status": 4,
                    "derivedStatus": null,
                    "winner": null,
                    "losers": null
                },
                {
                    "@class": ".ItemReportModel",
                    "category": {
                        "@class": ".CategoryModel",
                        "id": 545,
                        "name": "Demo Category - Antiques",
                        "code": null,
                        "selected": false,
                        "treeName": "Bidlogix Demo Tree&reg;",
                        "treeId": 3,
                        "parentName": "Bidlogix Demo",
                        "parentId": 396,
                        "children": null
                    },
                    "id": 401266,
                    "title": "Title goes here",
                    "lotNumber": "5",
                    "imageUrls": null,
                    "purchaseOption": "NONE",
                    "currencyId": 2,
                    "unitOfMeasurement": null,
                    "numberOfUnits": null,
                    "endTime": 1635242400000,
                    "actualEndTime": null,
                    "status": 5,
                    "derivedStatus": null,
                    "winner": null,
                    "losers": null
                },
                {
                    "@class": ".ItemReportModel",
                    "category": {
                        "@class": ".CategoryModel",
                        "id": 397,
                        "name": "Demo Category - Instruments",
                        "code": null,
                        "selected": false,
                        "treeName": "Bidlogix Demo Tree&reg;",
                        "treeId": 3,
                        "parentName": "Bidlogix Demo",
                        "parentId": 396,
                        "children": null
                    },
                    "id": 401262,
                    "title": "Title goes here",
                    "lotNumber": "1",
                    "imageUrls": null,
                    "purchaseOption": "NONE",
                    "currencyId": 2,
                    "unitOfMeasurement": null,
                    "numberOfUnits": null,
                    "endTime": 1635242400000,
                    "actualEndTime": null,
                    "status": 4,
                    "derivedStatus": null,
                    "winner": {
                        "@class": ".RegistrantReportModel",
                        "bidOrOfferId": 2066987,
                        "type": "bid",
                        "username": "clerk123",
                        "fullname": "Clerk Admin",
                        "email": "clerk123@email.com",
                        "status": 1,
                        "paddleNumber": 5003,
                        "externalUserRef": "",
                        "userId": 29,
                        "amount": 1230.0000,
                        "reserve": true,
                        "commissionBid": false,
                        "placed": 1634735949000,
                        "numberOfUnitsWanted": null,
                        "shippingType": null,
                        "numberOfOffers": null
                    },
                    "losers": [
                        {
                            "@class": ".RegistrantReportModel",
                            "bidOrOfferId": 2066986,
                            "type": "bid",
                            "username": "bidder3_demo",
                            "fullname": "test3 name3 test surname",
                            "email": "test@bidlogixtest.com",
                            "status": 1,
                            "paddleNumber": 5001,
                            "externalUserRef": "D232",
                            "userId": 196,
                            "amount": 1220.0000,
                            "reserve": false,
                            "commissionBid": false,
                            "placed": 1634735947000,
                            "numberOfUnitsWanted": null,
                            "shippingType": null,
                            "numberOfOffers": null
                        },
                        {
                            "@class": ".RegistrantReportModel",
                            "bidOrOfferId": 2066985,
                            "type": "bid",
                            "username": "clerk123",
                            "fullname": "Clerk Admin",
                            "email": "clerk123@email.com",
                            "status": 1,
                            "paddleNumber": 5003,
                            "externalUserRef": "",
                            "userId": 29,
                            "amount": 1210.0000,
                            "reserve": true,
                            "commissionBid": false,
                            "placed": 1634735945000,
                            "numberOfUnitsWanted": null,
                            "shippingType": null,
                            "numberOfOffers": null
                        },
                        {
                            "@class": ".RegistrantReportModel",
                            "bidOrOfferId": 2048875,
                            "type": "bid",
                            "username": "bidder3_demo",
                            "fullname": "test3 name3 test surname",
                            "email": "test@bidlogixtest.com",
                            "status": 1,
                            "paddleNumber": 5001,
                            "externalUserRef": "D232",
                            "userId": 196,
                            "amount": 1200.0000,
                            "reserve": false,
                            "commissionBid": false,
                            "placed": 1634305579000,
                            "numberOfUnitsWanted": null,
                            "shippingType": null,
                            "numberOfOffers": null
                        }
                    ]
                },
                {
                    "@class": ".ItemReportModel",
                    "category": {
                        "@class": ".CategoryModel",
                        "id": 397,
                        "name": "Demo Category - Instruments",
                        "code": null,
                        "selected": false,
                        "treeName": "Bidlogix Demo Tree&reg;",
                        "treeId": 3,
                        "parentName": "Bidlogix Demo",
                        "parentId": 396,
                        "children": null
                    },
                    "id": 401263,
                    "title": "Title goes here",
                    "lotNumber": "2",
                    "imageUrls": null,
                    "purchaseOption": "NONE",
                    "currencyId": 2,
                    "unitOfMeasurement": null,
                    "numberOfUnits": null,
                    "endTime": 1635242400000,
                    "actualEndTime": null,
                    "status": 4,
                    "derivedStatus": null,
                    "winner": {
                        "@class": ".RegistrantReportModel",
                        "bidOrOfferId": 2048876,
                        "type": "bid",
                        "username": "bidder3_demo",
                        "fullname": "test3 name3 test surname",
                        "email": "test@bidlogixtest.com",
                        "status": 1,
                        "paddleNumber": 5001,
                        "externalUserRef": "D232",
                        "userId": 196,
                        "amount": 1200.0000,
                        "reserve": false,
                        "commissionBid": false,
                        "placed": 1634305581000,
                        "numberOfUnitsWanted": null,
                        "shippingType": null,
                        "numberOfOffers": null
                    },
                    "losers": null
                }
            ]
        }
    },
    "webAppContext": "302"
}
```

</TabItem>

<TabItem value="withImages">

Call 
``` 
https://hove.eu-west-2.bidjs.com/auction-mgt/bdxapi/reporting/auction/3861/category?clientId=302&includeImages=true
```

Response 
``` json
{
    "@class": ".DataPushPayload",
    "pushCode": "REPORTING_AUCTION",
    "models": {
        "auctionReport": {
            "@class": ".reporting.AuctionReportModel",
            "reportMeta": {
                "@class": ".ReportMetaModel",
                "reportType": 1,
                "reportObject": "AuctionReportModel",
                "reportMade": 1634736072044
            },
            "reportTitle": "Auction Bids Report",
            "webAppId": 302,
            "auctionId": 3861,
            "auctionUuid": "8892d921-4df9-40b3-b3cf-f403635b9db6",
            "auctionTitle": "test training auction",
            "auctionDescription": "description goes here",
            "items": [
                {
                    "@class": ".ItemReportModel",
                    "category": {
                        "@class": ".CategoryModel",
                        "id": 545,
                        "name": "Demo Category - Antiques",
                        "code": null,
                        "selected": false,
                        "treeName": "Bidlogix Demo Tree&reg;",
                        "treeId": 3,
                        "parentName": "Bidlogix Demo",
                        "parentId": 396,
                        "children": null
                    },
                    "id": 401264,
                    "title": "Title goes here",
                    "lotNumber": "3",
                    "imageUrls": [
                        "https://media.bidjs.com//image/upload/v1634305214/bdx/3_aqbmfi.jpg"
                    ],
                    "purchaseOption": "None",
                    "currencyId": 2,
                    "unitOfMeasurement": "",
                    "numberOfUnits": 1,
                    "endTime": 1635242400000,
                    "actualEndTime": null,
                    "status": 4,
                    "derivedStatus": 4,
                    "winner": null,
                    "losers": []
                },
                {
                    "@class": ".ItemReportModel",
                    "category": {
                        "@class": ".CategoryModel",
                        "id": 545,
                        "name": "Demo Category - Antiques",
                        "code": null,
                        "selected": false,
                        "treeName": "Bidlogix Demo Tree&reg;",
                        "treeId": 3,
                        "parentName": "Bidlogix Demo",
                        "parentId": 396,
                        "children": null
                    },
                    "id": 401266,
                    "title": "Title goes here",
                    "lotNumber": "5",
                    "imageUrls": [
                        "https://media.bidjs.com//image/upload/v1634305214/bdx/5_d5xdbw.jpg"
                    ],
                    "purchaseOption": "None",
                    "currencyId": 2,
                    "unitOfMeasurement": "",
                    "numberOfUnits": 1,
                    "endTime": 1635242400000,
                    "actualEndTime": null,
                    "status": 5,
                    "derivedStatus": 5,
                    "winner": null,
                    "losers": []
                },
                {
                    "@class": ".ItemReportModel",
                    "category": {
                        "@class": ".CategoryModel",
                        "id": 397,
                        "name": "Demo Category - Instruments",
                        "code": null,
                        "selected": false,
                        "treeName": "Bidlogix Demo Tree&reg;",
                        "treeId": 3,
                        "parentName": "Bidlogix Demo",
                        "parentId": 396,
                        "children": null
                    },
                    "id": 401262,
                    "title": "Title goes here",
                    "lotNumber": "1",
                    "imageUrls": [
                        "https://media.bidjs.com//image/upload/v1634305321/bdx/13_a5kuie.jpg"
                    ],
                    "purchaseOption": "None",
                    "currencyId": 2,
                    "unitOfMeasurement": "",
                    "numberOfUnits": 1,
                    "endTime": 1635242400000,
                    "actualEndTime": null,
                    "status": 4,
                    "derivedStatus": 4,
                    "winner": {
                        "@class": ".RegistrantReportModel",
                        "bidOrOfferId": 2066987,
                        "type": "bid",
                        "username": "clerk123",
                        "fullname": "Clerk Admin",
                        "email": "clerk123@email.com",
                        "status": 1,
                        "paddleNumber": 5003,
                        "externalUserRef": "",
                        "userId": 29,
                        "amount": 123000,
                        "reserve": true,
                        "commissionBid": false,
                        "placed": 1634735949000,
                        "numberOfUnitsWanted": null,
                        "shippingType": null,
                        "numberOfOffers": null
                    },
                    "losers": [
                        {
                            "@class": ".RegistrantReportModel",
                            "bidOrOfferId": 2048875,
                            "type": "bid",
                            "username": "bidder3_demo",
                            "fullname": "test3 name3 test surname",
                            "email": "test@bidlogixtest.com",
                            "status": 1,
                            "paddleNumber": 5001,
                            "externalUserRef": "D232",
                            "userId": 196,
                            "amount": 120000,
                            "reserve": false,
                            "commissionBid": false,
                            "placed": 1634305579000,
                            "numberOfUnitsWanted": null,
                            "shippingType": null,
                            "numberOfOffers": null
                        },
                        {
                            "@class": ".RegistrantReportModel",
                            "bidOrOfferId": 2066985,
                            "type": "bid",
                            "username": "clerk123",
                            "fullname": "Clerk Admin",
                            "email": "clerk123@email.com",
                            "status": 1,
                            "paddleNumber": 5003,
                            "externalUserRef": "",
                            "userId": 29,
                            "amount": 121000,
                            "reserve": true,
                            "commissionBid": false,
                            "placed": 1634735945000,
                            "numberOfUnitsWanted": null,
                            "shippingType": null,
                            "numberOfOffers": null
                        },
                        {
                            "@class": ".RegistrantReportModel",
                            "bidOrOfferId": 2066986,
                            "type": "bid",
                            "username": "bidder3_demo",
                            "fullname": "test3 name3 test surname",
                            "email": "test@bidlogixtest.com",
                            "status": 1,
                            "paddleNumber": 5001,
                            "externalUserRef": "D232",
                            "userId": 196,
                            "amount": 122000,
                            "reserve": false,
                            "commissionBid": false,
                            "placed": 1634735947000,
                            "numberOfUnitsWanted": null,
                            "shippingType": null,
                            "numberOfOffers": null
                        }
                    ]
                },
                {
                    "@class": ".ItemReportModel",
                    "category": {
                        "@class": ".CategoryModel",
                        "id": 397,
                        "name": "Demo Category - Instruments",
                        "code": null,
                        "selected": false,
                        "treeName": "Bidlogix Demo Tree&reg;",
                        "treeId": 3,
                        "parentName": "Bidlogix Demo",
                        "parentId": 396,
                        "children": null
                    },
                    "id": 401263,
                    "title": "Title goes here",
                    "lotNumber": "2",
                    "imageUrls": [
                        "https://media.bidjs.com//image/upload/v1634305214/bdx/2_auggpp.jpg"
                    ],
                    "purchaseOption": "None",
                    "currencyId": 2,
                    "unitOfMeasurement": "",
                    "numberOfUnits": 1,
                    "endTime": 1635242400000,
                    "actualEndTime": null,
                    "status": 4,
                    "derivedStatus": 4,
                    "winner": {
                        "@class": ".RegistrantReportModel",
                        "bidOrOfferId": 2048876,
                        "type": "bid",
                        "username": "bidder3_demo",
                        "fullname": "test3 name3 test surname",
                        "email": "test@bidlogixtest.com",
                        "status": 1,
                        "paddleNumber": 5001,
                        "externalUserRef": "D232",
                        "userId": 196,
                        "amount": 120000,
                        "reserve": false,
                        "commissionBid": false,
                        "placed": 1634305581000,
                        "numberOfUnitsWanted": null,
                        "shippingType": null,
                        "numberOfOffers": null
                    },
                    "losers": []
                }
            ]
        }
    },
    "webAppContext": "302"
}
```
</TabItem>
</Tabs>

## Errors 
| code | example |
| :--- | -------: |
| 401 UNAUTHORIZED | \{ error : "Log in" \} |
| 422 Unprocessable Entry | \{ error : ["\<field_name\>"] \} |


