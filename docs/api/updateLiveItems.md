---
id: update-live-items
title: Items (Live) Update
sidebar_label: Items (Live) Update
---

## Description 
Edit an item's title, summary and description when it is in `Live` status. 

## Making a request

 ### URL 
 ```
/liveItems/{itemId}
 ```

### HTTP Method
```
POST
```

### Headers 
| Key          |                  Value |
| :----------- | ---------------------: |
| BDXAPI_NAME  | Bidlogix API Signature |
| Content-Type |       application/json |


### Path Parameters 
| Parameter |                                          Value |
| :-------- | ---------------------------------------------: |
| itemId | ID of the item you wish to update details for |

Also see [API Introduction](./general%20info/apiInfo.md).

### Call Details
[Click here to see our documentation](/apiDefinitions)

:::note Notes
Item's title is mandatory field for this API.
Item can be only updated if it is in a Live state.

:::