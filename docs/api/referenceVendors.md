---
id: get-vendors
title: Vendors List
sidebar_label: Vendors List
---


## Description 
Retrieve vendors for a given client ID and auctioneer ID. 

## Making a request

 ### URL 
 ```
/vendors/{clientId}/{auctioneerId}
 ```

### HTTP Method
```
GET
```

### Headers 
| Key          |                  Value |
| :----------- | ---------------------: |
| BDXAPI_NAME  | Bidlogix API Signature |
| Content-Type |       application/json |


### Path Parameters 
| Parameter |                                          Value |
| :-------- | ---------------------------------------------: |
| clientId      | The client ID for the given client         |
| auctioneerId  | The auctioneer ID for the given auctioneer |


### Call Details
[Click here to see our documentation](/apiDefinitions)

:::note Notes
This call will retrieve the vendor ID and name for all vendors under this auctioneer ID. 

:::