---
id: get-uuid
title: User Get
sidebar_label: User Get
---


## Description 
Retrieve user's name and username from their UUID. 

## Making a request

 ### URL 
 ```
/users/{uuid}
 ```

### HTTP Method
```
GET
```

### Headers 
| Key          |                  Value |
| :----------- | ---------------------: |
| BDXAPI_NAME  | Bidlogix API Signature |
| Content-Type |       application/json |


### Path Parameters 
| Parameter |                                          Value |
| :-------- | ---------------------------------------------: |
| uuid      | The UUID ID of the user whose details you need |


### Call Details
[Click here to see our documentation](/apiDefinitions)

:::note Notes
This call will retrieve the user's username, forename and surname. 

:::