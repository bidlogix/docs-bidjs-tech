---
id: item-report
title: Items Report
sidebar_label: Items Report
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

## Description 
The auction Items Report call reports details on specific item(s) and the bids and offers made on them. This includes information such as the item's category, title and lot number, end time, etc. 

The API must be structured correctly and have the correct security in place. See our document **Troubleshooting**. 

See our documents **Status Codes** and **Common Data** for more information on results. 

:::note Please Note
Items Report are limited to a call size of around 6KB, or up to 1000 items per call.
:::

## Making a request

 ### URL 
 ```
https://your_bidlogix_domain/auction-mgt/bdxapi/reporting/auctionitem?ids=itemIds&clientId=your_bidlogix_client_id
 ```

### HTTP Method
```
GET
```

### Headers 
| Key          |                  Value |
| :----------- | ---------------------: |
| BDXAPI_NAME  | Bidlogix API Signature |
| Content-Type |       application/json |


### Path Parameters and Adaptations
| Parameter |                                          Value |
| :-------- | ---------------------------------------------: |
| itemIds | ID of the item(s) you wish to retrieve information for |
| clientId | your bidlogix Client ID |
| &includeImages | this is set to `true` or `false` to enable image URL retrieval |

Appending the parameter: `&includeImages=true` to the end of the request, will result in a response that includes URLs to the images of the items. This will include the cloudinary image URLs in the resulting report.

:::note Please Note
The returned `winner` field is the accepted offer, if there is one, otherwise it's the highest bid. 
:::

Also see [API Introduction](./general%20info/apiInfo.md).


## Examples 
<Tabs
  defaultValue="minimum"
  groupId='installation-type'
  values={[
    {label: 'Example 1 - no images', value: 'minimum'},
    {label: 'Example 2 - with images', value: 'withImages'}
  ]}>
<TabItem value="minimum">

Call
``` 
https://hove.eu-west-2.bidjs.com/auction-mgt/bdxapi/reporting/auctionitem?ids=390858&clientId=302
```
Response 
``` json
{
    "@class": ".DataPushPayload",
    "pushCode": "REPORTING_AUCTION",
    "models": {
        "auctionReport": {
            "@class": ".reporting.ItemsReportModel",
            "reportMeta": {
                "@class": ".ReportMetaModel",
                "reportType": 5,
                "reportObject": "ItemsReportModel",
                "reportMade": 1634735158983
            },
            "reportTitle": "Items Report false",
            "items": [
                {
                    "@class": ".ItemReportModel",
                    "category": {
                        "@class": ".CategoryModel",
                        "id": 546,
                        "name": "Demo Category - Cars",
                        "code": null,
                        "selected": false,
                        "treeName": "Bidlogix Demo Tree&reg;",
                        "treeId": 3,
                        "parentName": "Bidlogix Demo",
                        "parentId": 396,
                        "children": null
                    },
                    "id": 390859,
                    "title": "Car 2 &ndash; none, opening bid",
                    "lotNumber": "2",
                    "imageUrls": null,
                    "purchaseOption": "None",
                    "currencyId": 2,
                    "unitOfMeasurement": null,
                    "numberOfUnits": 1,
                    "endTime": 1638277320000,
                    "actualEndTime": null,
                    "status": 4,
                    "derivedStatus": 4,
                    "winner": {
                        "@class": ".RegistrantReportModel",
                        "bidOrOfferId": 2058918,
                        "type": "bid",
                        "username": "bidder3_demo",
                        "fullname": "test3 name3 test surname",
                        "email": "test@bidlogixtest.com",
                        "status": 1,
                        "paddleNumber": 5006,
                        "externalUserRef": "D232",
                        "userId": 196,
                        "amount": 850,
                        "reserve": false,
                        "commissionBid": false,
                        "placed": 1634656689000,
                        "numberOfUnitsWanted": null,
                        "shippingType": null,
                        "numberOfOffers": null
                    },
                    "losers": [
                        {
                            "@class": ".RegistrantReportModel",
                            "bidOrOfferId": 2029467,
                            "type": "bid",
                            "username": "bidder3_demo",
                            "fullname": "test3 name3 test surname",
                            "email": "test@bidlogixtest.com",
                            "status": 1,
                            "paddleNumber": 5006,
                            "externalUserRef": "D232",
                            "userId": 196,
                            "amount": 750,
                            "reserve": false,
                            "commissionBid": false,
                            "placed": 1633946218000,
                            "numberOfUnitsWanted": null,
                            "shippingType": null,
                            "numberOfOffers": null
                        },
                        {
                            "@class": ".RegistrantReportModel",
                            "bidOrOfferId": 2058911,
                            "type": "bid",
                            "username": "Bidder2_demo",
                            "fullname": "Bidder2 demo2",
                            "email": "bidder@bidlogix.net",
                            "status": 1,
                            "paddleNumber": 5002,
                            "externalUserRef": "",
                            "userId": 10,
                            "amount": 800,
                            "reserve": false,
                            "commissionBid": false,
                            "placed": 1634656574000,
                            "numberOfUnitsWanted": null,
                            "shippingType": null,
                            "numberOfOffers": null
                        }
                    ]
                }
            ]
        }
    },
    "webAppContext": "302"
}
```

</TabItem>

<TabItem value="withImages">

Call 
``` 
https://hove.eu-west-2.bidjs.com/auction-mgt/bdxapi/reporting/auctionitem?ids=390859&includeImages=true&clientId=302
```

Response 
``` json
{
    "@class": ".DataPushPayload",
    "pushCode": "REPORTING_AUCTION",
    "models": {
        "auctionReport": {
            "@class": ".reporting.ItemsReportModel",
            "reportMeta": {
                "@class": ".ReportMetaModel",
                "reportType": 5,
                "reportObject": "ItemsReportModel",
                "reportMade": 1634735221279
            },
            "reportTitle": "Items Report true",
            "items": [
                {
                    "@class": ".ItemReportModel",
                    "category": {
                        "@class": ".CategoryModel",
                        "id": 546,
                        "name": "Demo Category - Cars",
                        "code": null,
                        "selected": false,
                        "treeName": "Bidlogix Demo Tree&reg;",
                        "treeId": 3,
                        "parentName": "Bidlogix Demo",
                        "parentId": 396,
                        "children": null
                    },
                    "id": 390859,
                    "title": "Car 2 &ndash; none, opening bid",
                    "lotNumber": "2",
                    "imageUrls": [
                        "https://media.bidjs.com//image/upload/v1633351540/bdx/2_vitlip.jpg",
                        "https://media.bidjs.com//image/upload/v1633351540/bdx/2-2_o0cb5c.jpg"
                    ],
                    "purchaseOption": "None",
                    "currencyId": 2,
                    "unitOfMeasurement": null,
                    "numberOfUnits": 1,
                    "endTime": 1638277320000,
                    "actualEndTime": null,
                    "status": 4,
                    "derivedStatus": 4,
                    "winner": {
                        "@class": ".RegistrantReportModel",
                        "bidOrOfferId": 2058918,
                        "type": "bid",
                        "username": "bidder3_demo",
                        "fullname": "test3 name3 test surname",
                        "email": "test@bidlogixtest.com",
                        "status": 1,
                        "paddleNumber": 5006,
                        "externalUserRef": "D232",
                        "userId": 196,
                        "amount": 850,
                        "reserve": false,
                        "commissionBid": false,
                        "placed": 1634656689000,
                        "numberOfUnitsWanted": null,
                        "shippingType": null,
                        "numberOfOffers": null
                    },
                    "losers": [
                        {
                            "@class": ".RegistrantReportModel",
                            "bidOrOfferId": 2029467,
                            "type": "bid",
                            "username": "bidder3_demo",
                            "fullname": "test3 name3 test surname",
                            "email": "test@bidlogixtest.com",
                            "status": 1,
                            "paddleNumber": 5006,
                            "externalUserRef": "D232",
                            "userId": 196,
                            "amount": 750,
                            "reserve": false,
                            "commissionBid": false,
                            "placed": 1633946218000,
                            "numberOfUnitsWanted": null,
                            "shippingType": null,
                            "numberOfOffers": null
                        },
                        {
                            "@class": ".RegistrantReportModel",
                            "bidOrOfferId": 2058911,
                            "type": "bid",
                            "username": "Bidder2_demo",
                            "fullname": "Bidder2 demo2",
                            "email": "bidder@bidlogix.net",
                            "status": 1,
                            "paddleNumber": 5002,
                            "externalUserRef": "",
                            "userId": 10,
                            "amount": 800,
                            "reserve": false,
                            "commissionBid": false,
                            "placed": 1634656574000,
                            "numberOfUnitsWanted": null,
                            "shippingType": null,
                            "numberOfOffers": null
                        }
                    ]
                }
            ]
        }
    },
    "webAppContext": "302"
}
```
</TabItem>
</Tabs>

## Errors 
| code | example |
| :--- | -------: |
| 401 UNAUTHORIZED | \{ error : "Log in" \} |
| 422 Unprocessable Entry | \{ error : ["\<field_name\>"] \} |
| 400 BAD REQUEST | ``` json {"@class":".ErrorModel", "failed":true, "field":null, "code":"REPORTING_API", "message":"Invalid ids parameter - only numbers and commas allowed; actual value passed=invalid_argument", "loggedIn":false} ``` |

