---
id: category-report
title: Category Get
sidebar_label: Category Get
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

## Description 
The Category Get call reports on category reference data within the Bidlogix system, such as the category ID, parent, child, description, etc.

The API must be structured correctly and have the correct security in place. See our document **Troubleshooting**. 

See our documents **Status Codes** and **Common Data** for more information on results. 

## Making a request

 ### URL 
 ```
https://your_bidlogix_domain/auction-mgt/bdxapi/reporting/category/{treeId}?clientId=your_bidlogix_client_id
 ```

### HTTP Method
```
GET
```

### Headers 
| Key          |                  Value |
| :----------- | ---------------------: |
| BDXAPI_NAME  | Bidlogix API Signature |
| Content-Type |       application/json |


### Path Parameters and Adaptations
| Parameter |                                          Value |
| :-------- | ---------------------------------------------: |
| treeId | ID of the category tree you wish to retrieve information for |
| clientId | your bidlogix Client ID |

Also see [API Introduction](./general%20info/apiInfo.md).


## Examples 

Call
``` 
https://hove.eu-west-2.bidjs.com/auction-mgt/bdxapi/reporting/category/3?clientId=302
```
Response 
``` json
{
    "@class": ".DataPushPayload",
    "pushCode": "REPORTING_CATEGORY",
    "models": {
        "categoryReport": {
            "@class": ".reporting.CategoryReportModel",
            "reportMeta": {
                "@class": ".ReportMetaModel",
                "reportType": 3,
                "reportObject": "CategoryReportModel",
                "reportMade": 1634736522051
            },
            "reportTitle": "Category Report",
            "categoryTreeId": 3,
            "categoryTreeTitle": "Bidlogix Demo Tree&reg;",
            "categoryTreeDescription": "Bidlogix Demo Tree",
            "items": [
                {
                    "@class": ".CategoryModel",
                    "id": 396,
                    "name": "Bidlogix Demo",
                    "code": null,
                    "selected": false,
                    "treeName": "Bidlogix Demo Tree&reg;",
                    "treeId": 3,
                    "parentName": null,
                    "parentId": null,
                    "children": null
                },
                {
                    "@class": ".CategoryModel",
                    "id": 545,
                    "name": "Demo Category - Antiques",
                    "code": null,
                    "selected": false,
                    "treeName": "Bidlogix Demo Tree&reg;",
                    "treeId": 3,
                    "parentName": "Bidlogix Demo",
                    "parentId": 396,
                    "children": null
                },
                {
                    "@class": ".CategoryModel",
                    "id": 546,
                    "name": "Demo Category - Cars",
                    "code": null,
                    "selected": false,
                    "treeName": "Bidlogix Demo Tree&reg;",
                    "treeId": 3,
                    "parentName": "Bidlogix Demo",
                    "parentId": 396,
                    "children": null
                },
                {
                    "@class": ".CategoryModel",
                    "id": 397,
                    "name": "Demo Category - Instruments",
                    "code": null,
                    "selected": false,
                    "treeName": "Bidlogix Demo Tree&reg;",
                    "treeId": 3,
                    "parentName": "Bidlogix Demo",
                    "parentId": 396,
                    "children": null
                },
                {
                    "@class": ".CategoryModel",
                    "id": 1866,
                    "name": "Demo Category - Livestock",
                    "code": null,
                    "selected": false,
                    "treeName": "Bidlogix Demo Tree&reg;",
                    "treeId": 3,
                    "parentName": "Bidlogix Demo",
                    "parentId": 396,
                    "children": null
                },
                {
                    "@class": ".CategoryModel",
                    "id": 399,
                    "name": "Demo Category - Machinery",
                    "code": null,
                    "selected": false,
                    "treeName": "Bidlogix Demo Tree&reg;",
                    "treeId": 3,
                    "parentName": "Bidlogix Demo",
                    "parentId": 396,
                    "children": null
                },
                {
                    "@class": ".CategoryModel",
                    "id": 406,
                    "name": "Demo Category - Pocketwatches",
                    "code": null,
                    "selected": false,
                    "treeName": "Bidlogix Demo Tree&reg;",
                    "treeId": 3,
                    "parentName": "Bidlogix Demo",
                    "parentId": 396,
                    "children": null
                },
                {
                    "@class": ".CategoryModel",
                    "id": 407,
                    "name": "Demo Category - Property",
                    "code": null,
                    "selected": false,
                    "treeName": "Bidlogix Demo Tree&reg;",
                    "treeId": 3,
                    "parentName": "Bidlogix Demo",
                    "parentId": 396,
                    "children": null
                },
                {
                    "@class": ".CategoryModel",
                    "id": 398,
                    "name": "Demo Category - Silverware",
                    "code": null,
                    "selected": false,
                    "treeName": "Bidlogix Demo Tree&reg;",
                    "treeId": 3,
                    "parentName": "Bidlogix Demo",
                    "parentId": 396,
                    "children": null
                },
                {
                    "@class": ".CategoryModel",
                    "id": 408,
                    "name": "Demo Category - Tractors",
                    "code": null,
                    "selected": false,
                    "treeName": "Bidlogix Demo Tree&reg;",
                    "treeId": 3,
                    "parentName": "Bidlogix Demo",
                    "parentId": 396,
                    "children": null
                },
                {
                    "@class": ".CategoryModel",
                    "id": 400,
                    "name": "Bidlogix Marketplace Property",
                    "code": null,
                    "selected": false,
                    "treeName": "Bidlogix Demo Tree&reg;",
                    "treeId": 3,
                    "parentName": null,
                    "parentId": null,
                    "children": null
                },
                {
                    "@class": ".CategoryModel",
                    "id": 401,
                    "name": "Bidlogix demo comercial",
                    "code": null,
                    "selected": false,
                    "treeName": "Bidlogix Demo Tree&reg;",
                    "treeId": 3,
                    "parentName": "Bidlogix Marketplace Property",
                    "parentId": 400,
                    "children": null
                },
                {
                    "@class": ".CategoryModel",
                    "id": 402,
                    "name": "Bidlogix demo domestic",
                    "code": null,
                    "selected": false,
                    "treeName": "Bidlogix Demo Tree&reg;",
                    "treeId": 3,
                    "parentName": "Bidlogix Marketplace Property",
                    "parentId": 400,
                    "children": null
                },
                {
                    "@class": ".CategoryModel",
                    "id": 5245,
                    "name": "Bidlogix demo generic",
                    "code": null,
                    "selected": false,
                    "treeName": "Bidlogix Demo Tree&reg;",
                    "treeId": 3,
                    "parentName": "Bidlogix Marketplace Property",
                    "parentId": 400,
                    "children": null
                },
                {
                    "@class": ".CategoryModel",
                    "id": 403,
                    "name": "Bidlogix demo other",
                    "code": null,
                    "selected": false,
                    "treeName": "Bidlogix Demo Tree&reg;",
                    "treeId": 3,
                    "parentName": "Bidlogix Marketplace Property",
                    "parentId": 400,
                    "children": null
                },
                {
                    "@class": ".CategoryModel",
                    "id": 3178,
                    "name": "Bidlogix Marketplace Vehicles",
                    "code": null,
                    "selected": false,
                    "treeName": "Bidlogix Demo Tree&reg;",
                    "treeId": 3,
                    "parentName": null,
                    "parentId": null,
                    "children": null
                },
                {
                    "@class": ".CategoryModel",
                    "id": 5524,
                    "name": "Bidlogix demo  vintage cars",
                    "code": null,
                    "selected": false,
                    "treeName": "Bidlogix Demo Tree&reg;",
                    "treeId": 3,
                    "parentName": "Bidlogix Marketplace Vehicles",
                    "parentId": 3178,
                    "children": null
                },
                {
                    "@class": ".CategoryModel",
                    "id": 3179,
                    "name": "Bidlogix demo - cars",
                    "code": null,
                    "selected": false,
                    "treeName": "Bidlogix Demo Tree&reg;",
                    "treeId": 3,
                    "parentName": "Bidlogix Marketplace Vehicles",
                    "parentId": 3178,
                    "children": null
                },
                {
                    "@class": ".CategoryModel",
                    "id": 5525,
                    "name": "Bidlogix demo - saloon cars",
                    "code": null,
                    "selected": false,
                    "treeName": "Bidlogix Demo Tree&reg;",
                    "treeId": 3,
                    "parentName": "Bidlogix Marketplace Vehicles",
                    "parentId": 3178,
                    "children": null
                },
                {
                    "@class": ".CategoryModel",
                    "id": 5523,
                    "name": "Bidlogix demo - sports cars",
                    "code": null,
                    "selected": false,
                    "treeName": "Bidlogix Demo Tree&reg;",
                    "treeId": 3,
                    "parentName": "Bidlogix Marketplace Vehicles",
                    "parentId": 3178,
                    "children": null
                },
                {
                    "@class": ".CategoryModel",
                    "id": 3180,
                    "name": "Bidlogix demo - vans",
                    "code": null,
                    "selected": false,
                    "treeName": "Bidlogix Demo Tree&reg;",
                    "treeId": 3,
                    "parentName": "Bidlogix Marketplace Vehicles",
                    "parentId": 3178,
                    "children": null
                },
                {
                    "@class": ".CategoryModel",
                    "id": 3181,
                    "name": "Bidlogix demo scrap",
                    "code": null,
                    "selected": false,
                    "treeName": "Bidlogix Demo Tree&reg;",
                    "treeId": 3,
                    "parentName": "Bidlogix Marketplace Vehicles",
                    "parentId": 3178,
                    "children": null
                },
                {
                    "@class": ".CategoryModel",
                    "id": 3174,
                    "name": "Demo Marketplace Antiques",
                    "code": null,
                    "selected": false,
                    "treeName": "Bidlogix Demo Tree&reg;",
                    "treeId": 3,
                    "parentName": null,
                    "parentId": null,
                    "children": null
                },
                {
                    "@class": ".CategoryModel",
                    "id": 3176,
                    "name": "Bidlogix demo chairs",
                    "code": null,
                    "selected": false,
                    "treeName": "Bidlogix Demo Tree&reg;",
                    "treeId": 3,
                    "parentName": "Demo Marketplace Antiques",
                    "parentId": 3174,
                    "children": null
                },
                {
                    "@class": ".CategoryModel",
                    "id": 3177,
                    "name": "Bidlogix demo lamps",
                    "code": null,
                    "selected": false,
                    "treeName": "Bidlogix Demo Tree&reg;",
                    "treeId": 3,
                    "parentName": "Demo Marketplace Antiques",
                    "parentId": 3174,
                    "children": null
                },
                {
                    "@class": ".CategoryModel",
                    "id": 3175,
                    "name": "Bidlogix demo tables",
                    "code": null,
                    "selected": false,
                    "treeName": "Bidlogix Demo Tree&reg;",
                    "treeId": 3,
                    "parentName": "Demo Marketplace Antiques",
                    "parentId": 3174,
                    "children": null
                }
            ]
        }
    },
    "webAppContext": "302"
}
```

## Errors 
| code | example |
| :--- | -------: |
| 401 UNAUTHORIZED | \{ error : "Log in" \} |
| 422 Unprocessable Entry | \{ error : ["\<field_name\>"] \} |


