---
id: vendor-data
title: Vendors Report
sidebar_label: Vendors Report
---

## Description 
The Vendors Report call reports on Vendor reference data within the bidlogix system. This will return a list of vendors dependant on the parameters you enter into the call and will include information such as vendor activation status, administrators attached to this vendor, vendor ID, etc.

The API must be structured correctly and have the correct security in place. See our document **Troubleshooting**. 

See our documents **Status Codes** and **Common Data** for more information on results. 

## Making a request

 ### URL 
 ```
https://your_bidlogix_domain/auction-mgt/bdxapi/vendors?clientId=your_bidlogix_client_id
 ```
or, to specify the auctioneer, use the following: 
 ```
https://your_bidlogix_domain/auction-mgt/bdxapi/vendors/{auctioneerId}?clientId=your_bidlogix_client_id
 ```


### HTTP Method
```
GET
```

### Headers 
| Key          |                  Value |
| :----------- | ---------------------: |
| BDXAPI_NAME  | Bidlogix API Signature |
| Content-Type |       application/json |


### Path Parameters 
| Parameter |                                          Value |
| :-------- | ---------------------------------------------: |
| clientId | your bidlogix client ID |
| auctioneerId | your bidlogix auctioneer ID (optional) |

Also see [API Introduction](./general%20info/apiInfo.md).


## Examples

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

<Tabs
  defaultValue="clientid"
  groupId='installation-type'
  values={[
    {label: 'Example 1 - without auctioneer specified', value: 'clientid'},
    {label: 'Example 2 - with auctioneer specified', value: 'auctioneer'}
  ]}>
<TabItem value="clientid">


Call
``` 
https://hove.eu-west-2.bidjs.com/auction-mgt/bdxapi/vendors?clientId=302
```
Response 
``` json
{
    "@class": ".DataPushPayload",
    "pushCode": "VENDOR_REFERENCE",
    "models": {
        "vendorReferenceModel": {
            "@class": ".BackboneCollectionModel",
            "collection": [
                {
                    "@class": ".auction.VendorModel",
                    "id": 3,
                    "auctioneer": {
                        "@class": ".AuctioneerModel",
                        "webApp": null,
                        "clientOptions": {
                            "@class": ".ClientOptionsModel",
                            "showWinningBidAmount": true,
                            "showNumberOfBids": true,
                            "showBidIncrements": true,
                            "showBidStatusEndDate": true,
                            "showAutobidOnly": false,
                            "hideRelatedItems": true,
                            "maxOffersPerItem": 3
                        },
                        "auctioneerId": 3,
                        "name": "Demonstration Auctioneer",
                        "cloudyLogo": null,
                        "logo": null,
                        "facebookApplicationId": null,
                        "auctionEventCount": 0,
                        "auctionWebcastCount": 0,
                        "email": null,
                        "website": null,
                        "contactNumber": null,
                        "description": "An auctioneer that demonstrates",
                        "liveAuctionIds": []
                    },
                    "name": "Demonstration Vendor 1",
                    "companyContact": null,
                    "contacts": [],
                    "contactable": false,
                    "activated": false,
                    "summary": null,
                    "description": null,
                    "timeZone": {
                        "@class": ".TimeZoneModel",
                        "id": 316,
                        "name": "(GMT) (GMT) UTC",
                        "code": "UTC",
                        "selected": false,
                        "zone": null
                    },
                    "administrators": [
                        {
                            "@class": ".AdministratorModel",
                            "userId": 7,
                            "username": "Admin_demo"
                        },
                    ]
                },
                {
                    "@class": ".auction.VendorModel",
                    "id": 30,
                    "auctioneer": {
                        "@class": ".AuctioneerModel",
                        "webApp": null,
                        "clientOptions": {
                            "@class": ".ClientOptionsModel",
                            "showWinningBidAmount": true,
                            "showNumberOfBids": true,
                            "showBidIncrements": true,
                            "showBidStatusEndDate": true,
                            "showAutobidOnly": false,
                            "hideRelatedItems": true,
                            "maxOffersPerItem": 3
                        },
                        "auctioneerId": 3,
                        "name": "Demonstration Auctioneer",
                        "cloudyLogo": null,
                        "logo": null,
                        "facebookApplicationId": null,
                        "auctionEventCount": 0,
                        "auctionWebcastCount": 0,
                        "email": null,
                        "website": null,
                        "contactNumber": null,
                        "description": "An auctioneer that demonstrates",
                        "liveAuctionIds": []
                    },
                    "name": "Demonstration Vendor 2",
                    "companyContact": null,
                    "contacts": [],
                    "contactable": false,
                    "activated": false,
                    "summary": null,
                    "description": null,
                    "timeZone": {
                        "@class": ".TimeZoneModel",
                        "id": 316,
                        "name": "(GMT) (GMT) UTC",
                        "code": "UTC",
                        "selected": false,
                        "zone": null
                    },
                    "administrators": [
                        {
                            "@class": ".AdministratorModel",
                            "userId": 7,
                            "username": "Admin_demo"
                        }
                    ]
                },
                {
                    "@class": ".auction.VendorModel",
                    "id": 28,
                    "auctioneer": {
                        "@class": ".AuctioneerModel",
                        "webApp": null,
                        "clientOptions": {
                            "@class": ".ClientOptionsModel",
                            "showWinningBidAmount": true,
                            "showNumberOfBids": true,
                            "showBidIncrements": true,
                            "showBidStatusEndDate": true,
                            "showAutobidOnly": false,
                            "hideRelatedItems": true,
                            "maxOffersPerItem": 3
                        },
                        "auctioneerId": 3,
                        "name": "Demonstration Auctioneer",
                        "cloudyLogo": null,
                        "logo": null,
                        "facebookApplicationId": null,
                        "auctionEventCount": 0,
                        "auctionWebcastCount": 0,
                        "email": null,
                        "website": null,
                        "contactNumber": null,
                        "description": "An auctioneer that demonstrates",
                        "liveAuctionIds": []
                    },
                    "name": "Demonstration Vendor 3",
                    "companyContact": null,
                    "contacts": [],
                    "contactable": false,
                    "activated": false,
                    "summary": null,
                    "description": null,
                    "timeZone": {
                        "@class": ".TimeZoneModel",
                        "id": 316,
                        "name": "(GMT) (GMT) UTC",
                        "code": "UTC",
                        "selected": false,
                        "zone": null
                    },
                    "administrators": [
                        {
                            "@class": ".AdministratorModel",
                            "userId": 7,
                            "username": "Admin_demo"
                        },
                    ]
                }
            ]
        }
    },
    "webAppContext": "302"
}
```

</TabItem>

<TabItem value="auctioneer">

Call (for example purposes only, this call will not work in production)
``` 
https://hove.eu-west-2.bidjs.com/auction-mgt/bdxapi/vendors/3?clientId=302
```

Response 
``` json
{
    "@class": ".DataPushPayload",
    "pushCode": "VENDOR_REFERENCE",
    "models": {
        "vendorReferenceModel": {
            "@class": ".BackboneCollectionModel",
            "collection": [
                {
                    "@class": ".auction.VendorModel",
                    "id": 120,
                    "auctioneer": {
                        "@class": ".AuctioneerModel",
                        "webApp": null,
                        "clientOptions": {
                            "@class": ".ClientOptionsModel",
                            "showWinningBidAmount": true,
                            "showNumberOfBids": true,
                            "showBidIncrements": true,
                            "showBidStatusEndDate": true,
                            "showAutobidOnly": false,
                            "hideRelatedItems": true,
                            "maxOffersPerItem": 3
                        },
                        "auctioneerId": 3,
                        "name": "Demonstration Auctioneer",
                        "cloudyLogo": null,
                        "logo": null,
                        "facebookApplicationId": null,
                        "auctionEventCount": 0,
                        "auctionWebcastCount": 0,
                        "email": null,
                        "website": null,
                        "contactNumber": null,
                        "description": "An auctioneer that demonstrates",
                        "liveAuctionIds": []
                    },
                    "name": "Test Vendor 1",
                    "companyContact": null,
                    "contacts": [],
                    "contactable": false,
                    "activated": false,
                    "summary": null,
                    "description": null,
                    "timeZone": {
                        "@class": ".TimeZoneModel",
                        "id": 316,
                        "name": "(GMT) (GMT) UTC",
                        "code": "UTC",
                        "selected": false,
                        "zone": null
                    },
                    "administrators": [
                        {
                            "@class": ".AdministratorModel",
                            "userId": 123456,
                            "username": "Vendor_test_user"
                        },
                    ]
                }
            ]
        }
    },
    "webAppContext": "302"
}
```
</TabItem>
</Tabs>


## Errors 
| code | example |
| :--- | -------: |
| 401 UNAUTHORIZED | \{ error : "Log in" \} |
| 422 Unprocessable Entry | \{ error : ["\<field_name\>"] \} |

