---
id: delete-images
title: Images Delete
sidebar_label: Images Delete
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

## Description 
Delete images for a given item or items.

## Making a request

 ### URL 
 ```
/items/{itemId}/images
 ```

### HTTP Method
```
DELETE
```

### Headers 
| Key          |                  Value |
| :----------- | ---------------------: |
| BDXAPI_NAME  | Bidlogix API Signature |
| Content-Type |       application/json |


### Path Parameters 
| Parameter |                                          Value |
| :-------- | ---------------------------------------------: |
| itemId | ID of the item you wish to update details for |

Also see [API Introduction](./general%20info/apiInfo.md).

### Call Details
[Click here to see our documentation](/apiDefinitions)

:::note Notes
It is only possible to delete images using this method. Other media cannot be removed in this way.
:::