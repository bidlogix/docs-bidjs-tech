---
id: common-data
title: Common Data
sidebar_label: Common Data
---

This document defines some of the data structures that are common to all bidlogix api.

### AuctioneerApiModel
``` json
{ 
"@class":"com.bidlogix.scaffold.models.auctionapi.AuctioneerApiModel", //required field
"id":"LONG", //Primary key - used to match up data when using the bidlogix reference API to pull data.
"name":"STRING" //name of the auctioneer
}
```


### UrlAttachmentApiModel
``` json
{ 
"@class":"com.bidlogix.scaffold.models.auctionapi.UrlAttachmentApiModel", //required field
"url":"STRING", //required field. A URL for the resource - image, document. If the resource is a youtube video then this is set to just the youtube video ID rather then the whole URL.
"label":"STRING" //optional label
}
```


### AddressModel 
``` json
{ 
"@class":"com.bidlogix.scaffold.models.address.AddressModel", //required field
"id":"LONG", //	Primary key - used to match up data when using the bidlogix reference api to pull data.
"addressLine1":"STRING", //required field
"addressLine2":"STRING", //required field
"addressLine3":"STRING", //not required
"addressLine4":"STRING", //not required
"county":"STRING", //not required
"city":"STRING", //not required
"postcode":"STRING", //required field
"longitude":"STRING", //not required
"latitude":"STRING", //not required
"countryCode":"STRING" //required field. 2 digit country code uk, us, fr, etc. Defaults to 'uk' 
}
```

### CountryModel
``` json
{ 
"@class":"com.bidlogix.scaffold.models.address.CountryModel", //required field
"id":"LONG", //Primary key - used to match up data when using the bidlogix reference API to pull data.
"code":"STRING", //required field. Two character ISO code; us, uk, fr etc - or set the id using the bidlogix reference data API
"name":"STRING" //optional name
}
```

### CurrencyModel
``` json
{ 
"@class":"com.bidlogix.scaffold.models.reference.CurrencyModel", //required field
"id":"LONG", //Primary key - used to match up data when using the bidlogix reference API to pull data.
"code":"STRING", //required field
"name":"STRING" //optional name
}
```