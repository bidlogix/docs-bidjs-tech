---
id: api-security
title: Security
sidebar_label: Security
---

## Before You Start 

Before you get started with our APIs, you will need to generate a secure signature unique to your instance. 

In order to generate your signature, you must request the **API Key** and **Secret** from us along with the appropriate **Client ID**. If you need assistance with this, please contact us on [support@bidlogix.com](mailto:support@bidlogix.com). 

## Generating your Signature

A signature can be built following the example below. This signature must be created on the server but can be passed to JavaScript client code to make client side calls if desired.

Here is an example of how to generate your signature:

```  
MessageDigestPasswordEncoder encoder = new MessageDigestPasswordEncoder("SHA-1");StringBuilder sb = new StringBuilder();sb.append("bdxapikey").append("=").append(bdxApiKey).append("&");sb.append("bdxapiClientId").append("=").append(clientId).append("&");sb.append("bdxapisecret").append("=").append(bdxApiSecret);model.setSignature(encoder.encodePassword(sb.toString(), null));return model;
```
Where:
> bdxApiKey = the API Key given to you by Bidlogix 
> 
> bdxApiSecret = the API Secret give to you by Bidlogix 
> 
> clientId = the numerical reference given to your webapp / instance of Bidlogix 
> 


## Additional Header
An http header or cookie named **BDXAPI_NAME** must be added where the value is set to the signature generated above.

e.g. if calling from server side (java servlet): 
``` 
request.addHeader("BDXAPI_NAME", signature);  
```

Or if calling from javascript client code:                 
```
document.cookie = "BDXAPI_NAME=" + signature + "; path=/";
```

If you are using software for generating your API calls, you may be able to enter the signature into the header by defining the BDXAPI_NAME key using the signature value you have created above. 