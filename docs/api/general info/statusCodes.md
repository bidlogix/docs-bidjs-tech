---
id: status-codes
title: Status Codes
sidebar_label: Status Codes
---

This document lists the Status codes for various API call results.

### Offer Status Codes

| Code | Status |
| :--- | ---: |
|1 | PENDING |
|2 | ACCEPTED |
|3 | REJECTED |
|4 | TRANSFER |

### Bid Status Codes

| Code | Status |
| :--- | ---: |
|1 | LIVE |
|2 | SUSPENDED |
|3XX | CANCELLED (Various states) |
|4 | DOUBLE |

### Auction Status Codes

| Code | Status |
| :--- | ---: |
|1 | NEW |
|2 | DRAFT |
|3 | PREVIEW |
|4 | LIVE |
|5 | RECONCILIATION |
|6 | ARCHIVED |

### Registrant Status Codes

| Code | Status |
| :--- | ---: |
|1 | PENDING |
|2 | APPROVED |
|3 | SUSPENDED |
|4 | AWAITING_DEPOSIT |
|5 | NOT_REGISTERED |
|6 | INSUFFICIENT_DATA |
|7 | ISSUE_WITH_DEPOSIT |

### Item Status Codes

| Code | Status |
| :--- | ---: |
|1 | NEW |
|2 | DRAFT |
|3 | PREVIEW |
|4 | LIVE |
|5 | WITHDRAWN |
|6 | SOLD BIDDING |
|7 | SOLD_MAKE_OFFER |
|8 | UNSOLD |
|9 | SUSPENDED |
|10 | SOLD_BUY_NOW |
|11 | SOLD_BULK_BUY_FULL |
|12 | SOLD_BULK_BUY_PARTIAL |
|13 | OPEN_BULK_BUY_PARTIAL |
|14 | WITHDRAWN_ENDED |
|15 | TRANSFERRED |
|16 | TENDER ACCEPTED |


### Auction Registration Status Codes
| Code | Status | 
| :--- | ---: |
|1 | PENDING |
|2 | APPROVED |
|3 | SUSPENDED |
|4 | AWAITING_DEPOSIT |
|5 | NOT_REGISTERED |
|6 | INSUFFICIENT_DATA |
|7 | ISSUE_WITH_DEPOSIT |
