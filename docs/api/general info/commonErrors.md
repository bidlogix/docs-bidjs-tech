---
id: api-errors
title: Troubleshooting
sidebar_label: Troubleshooting
---

You may receive the following errors in various forms, depending on your call method/software:  

| Error                     | Notes and Possible Resolutions | 
| :-------                  | ------: |
| JBWEB000065: HTTP Status 500 - `InfrastructureCodes-FAILED_TO_CONVERT_STRING_TO_WHOLE_NUMBER` | This occurs when, for example, a value that is expected to be numerical is put in as letters. A key culprit is using the letter version of your ClientID rather than the numerical version. Contact support@bidlogix.net if you are not sure what your numerical ClientID is  | 
| `401 Unauthorized` response: `{"failed":true,"field":null,"code":"401","message":"Access Denied","loggedIn":false}` or `{"@class": ".ErrorModel","failed": true,"field": null,"code": "401","message": "Access Denied","loggedIn": false}`  | This usually occurs when there is an issue with your API signature or its placement in the header. See our document API Security - Generating your Signature. If you do not know your API Key and Secret, contact [support@bidlogix.com](mailto:support@bidlogix.com).  | 
| 400 Bad Request `"message": "Invalid ids parameter - only numbers and commas allowed; actual value passed=itemIds"` | This occurs when one of the changeable parameters has not be completed or has been completed incorrectly. In this case, "itemIds" should have been a numerical value reflecting the ID ref of a specific item. | 
| 422 Unprocessable Entry: `{ error : ["<field_name>"] }` | Ensure the correct substitution/query string parameter or substitution format is being used within the API URL. | 
| Unknown Host Error | Related to issues with url itself - it is basically saying that the URL you went to does not exist e.g. a typo in the URL or a substitution that is not possible, for example | 