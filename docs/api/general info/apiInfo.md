---
id: api-general
title: Introduction
sidebar_label: Introduction
slug: /api
---

## Types of API Call

Bidlogix offers three types of API along with the follow calls:

Auction & Item Management

* Auction Create (Live or Preview)
* Items Create
* Items (Live) Update
* Item Update
* Images Add
* Images Delete
* Registrants Update

Reference & Reporting Data

* Auction Get
* Auction Reports
* Auctions List
* Auctioneer List
* Categories List
* Category Get
* Items Reports
* Items Reports By UUID
* Reference Data List
* Registrants List
* Sales Value Report
* User Bidding Report
* User Get
* Vendors List
* Vendor Report Request

## Structure of API Calls

It is vital to structure API Calls properly to receive the correct information, retain security and minimise the chance or errors.

### API Signature in Header

All API calls must have the signature in the header. Please see the [API Security Document](./apiSignature.md)

### Substitutions / Query String Parameters

The following query string parameters apply to all API calls:

| string parameter          | substitution |
| :---------------------    | ---------:|
| your_bidlogix_domain      | your bidlogix stack domain |
| your_bidlogix_client_id   | your client ID or WebApp ID |
| auctionId                 | ID associated to a specific auction |
| auctionUuid               | UUID associated to a specific auction |
| auctioneerId              | ID associated to your auctioneer |
| auctioneerUuid            | UUID associated to your auctioneer |
| itemId                    | ID associated to an item | 
| itemUuid                  | UUID associated to an item | 
| registrantId              | ID associated to a registrant | 
| registrantUuid            | UUID associated to a registrant | 
| categoryId                | ID associated to a specific category | 
| categoryUuid              | UUID associated to a specific category | 
| itemId                    | ID associated to an item |
| registrantId              | ID associated to a registrant |
| categoryId                | ID associated to a specific category |
| type                      | type of auction i.e. timed, webcast, market |
| sorting                   | enter 'category' to sort by category |
| etc.                      |  |

:::note Specific Parameters
There are substitutions / query string parameters specific to many calls. These are specified in their particular document.

There are also slight adaptations for image related query string parameters, as described in the appropriate document.
:::

### Call Class

Every call has a `class` which must be included in the body of the call.  

Here is an example of a class:

```bash
"@class": "com.bidlogix.scaffold.models.auctionapi.ItemApiModel"
```

If a call requires a `class` to be set, it will be given per call in each specific document.

### URL Structure

You must substitute your domain into some calls. For example, an API Call would look like this in our documentation:

```bash
https://your_bidlogix_domain/auction-mgt/bdxapi/reporting/auction/{auctionId}/{sorting}?clientId=your_bidlogix_client_id
```

And this in practise (which shows the Auction Report API call for our demo auctioneer):

```bash
https://hove.eu-west-2.bidjs.com/auction-mgt/bdxapi/reporting/auction/1234/category?clientId=302
```

Another type of call does **not** require substitution into the URL, which looks like this:

```bash
https://api.eu-west-2.bidjs.com/auction-mgt/auctions/{auctionId}
```

In which case, the only substitution would be the auction ID.

## Responses

Example responses are given for each API within their specific document. You will find more information in our documents Common Data and Status Codes.

## Mime Types

All requests return json by default. The format of GET requests can be changed to CSV by adding the parameter `format`.
