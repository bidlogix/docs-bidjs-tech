---
id: upload-media
title: Images Add
sidebar_label: Images Add
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

## Description 
Upload images, videos and documents to items that already exist in the system. 

## Making a request

 ### URL 
 ```
https://your_bidlogix_domain/auction-mgt/bdxapi/items/images
 ```

### HTTP Method
```
POST
```

### Headers 
| Key          |                  Value |
| :----------- | ---------------------: |
| BDXAPI_NAME  | Bidlogix API Signature |
| Content-Type |       application/json |


### Path Parameters 
None. The ID of items to upload to are defined within the body of the call. See documentation link below. 


### Call Details
[Click here to see our documentation](https://app.swaggerhub.com/apis-docs/Bidlogix/adding_images_to_existing_items/1.0.5#/default/post_auction_mgt_bdxapi_items_images)

:::note Notes
Attachments must have a unique filename. If it is attempted to upload two attachment files with the same filename, the second will be ignored. The filename also determines the index order of the images, for example, by following alphanumeric ordering. 

:::