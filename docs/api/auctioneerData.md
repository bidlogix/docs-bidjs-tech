---
id: auctioneer-data
title: Auctioneers List
sidebar_label: Auctioneers List
---

## Description 
The Auctioneers List call reports on Auctioneer reference data within the bidlogix system. For example, this will provide a list of auctioneers within this webapp including information such as the IDs of the auctioneers and the IDs any live auctions under those auctioneers. 

The API must be structured correctly and have the correct security in place. See our document **Troubleshooting**. 

See our documents **Status Codes** and **Common Data** for more information on results. 

## Making a request

 ### URL 
 ```
https://your_bidlogix_domain/auction-mgt/bdxapi/auctioneers?clientId=your_bidlogix_client_id
 ```

### HTTP Method
```
GET
```

### Headers 
| Key          |                  Value |
| :----------- | ---------------------: |
| BDXAPI_NAME  | Bidlogix API Signature |
| Content-Type |       application/json |


### Path Parameters 
| Parameter |                                          Value |
| :-------- | ---------------------------------------------: |
| clientId | your bidlogix client ID |

Also see [API Introduction](./general%20info/apiInfo.md).


## Examples 

Call
``` 
https://hove.eu-west-2.bidjs.com/auction-mgt/bdxapi/auctioneers?clientId=302
```
Response 
``` json
{
    "@class": ".DataPushPayload",
    "pushCode": "AUCTIONEER_REFERENCE",
    "models": {
        "auctioneerReferenceModel": {
            "@class": ".BackboneCollectionModel",
            "collection": [
                {
                    "@class": ".auctionapi.AuctioneerApiModel",
                    "id": 3,
                    "name": "Demonstration Auctioneer",
                    "liveAuctionIds": [
                        5,
                        3769,
                        3861,
                        3876
                    ]
                }
            ]
        }
    },
    "webAppContext": "302"
}
```


## Errors 
| code | example |
| :--- | -------: |
| 401 UNAUTHORIZED | \{ error : "Log in" \} |
| 422 Unprocessable Entry  | \{ error : ["\<field_name\>"] \} |

