---
id: update-items-by-id
title: Items (Draft/Preview) Update 
sidebar_label: Items (Draft/Preview) Update
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

## Description 
Edit an item's title, summary and description when it is in `Draft` or `Preview` status.

## Making a request

 ### URL 
 ```
/items/{itemId}
 ```

### HTTP Method
```
PATCH
```

### Headers 
| Key          |                  Value |
| :----------- | ---------------------: |
| BDXAPI_NAME  | Bidlogix API Signature |
| Content-Type |       application/json |


### Path Parameters 
| Parameter |                                          Value |
| :-------- | ---------------------------------------------: |
| itemId | ID of the item you wish to update details for |

Also see [API Introduction](./general%20info/apiInfo.md).

### Call Details
[Click here to see our documentation](/apiDefinitions)

:::note Notes
Item's title is mandatory field for this API.
Item can be only updated if it is in a Draft or Preview state.

:::