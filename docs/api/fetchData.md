---
id: fetch-data
title: Reference Data List
sidebar_label: Reference Data List
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

## Description 
The Reference Data List call returns reference data objects from the bidlogix system - mainly useful in combination with other bidlogix api calls. For example, you could use this to call a list of countries used in the system, which would include information such as the country ID, it's full name and alphanumeric code, etc.

The API must be structured correctly and have the correct security in place. See our document **Troubleshooting**. 

See our documents **Status Codes** and **Common Data** for more information on results. 

## Making a request

 ### URL 
 ```
https://your_bidlogix_domain/auction-mgt/bdxapi/reference/{type_of_reference_data}?clientId=your_bidlogix_client_id
 ```

### HTTP Method
```
GET
```

### Headers 
| Key          |                  Value |
| :----------- | ---------------------: |
| BDXAPI_NAME  | Bidlogix API Signature |
| Content-Type |       application/json |


### Path Parameters 
| Parameter |                                          Value |
| :-------- | ---------------------------------------------: |
| clientID | ID of the client you wish to view data for |
| type_of_reference_data | include `countries`, `continents`, `currencies`, `categories` or `timezones` to return the relevant information. These parameters can be added in combination, separated by a commma |

Also see [API Introduction](./general%20info/apiInfo.md).


## Examples 
<Tabs
  defaultValue="continents"
  groupId='installation-type'
  values={[
    {label: 'Example 1 - continents request', value: 'continents'},
    {label: 'Example 2 - multiple parameter request', value: 'multiple'}
  ]}>
<TabItem value="continents">

Call
``` 
https://hove.eu-west-2.bidjs.com/auction-mgt/bdxapi/reference/continents?clientId=302
```
Response 
``` json
{
    "@class": ".DataPushPayload",
    "pushCode": "REFERENCEDATA",
    "models": {
        "referenceModel": {
            "@class": ".reference.ReferenceDataModel",
            "referenceItemModelMap": {
                "continents": [
                    {
                        "@class": "com.bidlogix.scaffold.models.address.ContinentModel",
                        "id": 5,
                        "name": "Africa",
                        "code": "AFR",
                        "selected": false
                    },
                    {
                        "@class": "com.bidlogix.scaffold.models.address.ContinentModel",
                        "id": 4,
                        "name": "Asia",
                        "code": "ASI",
                        "selected": false
                    },
                    {
                        "@class": "com.bidlogix.scaffold.models.address.ContinentModel",
                        "id": 1,
                        "name": "Europe",
                        "code": "EUR",
                        "selected": false
                    },
                    {
                        "@class": "com.bidlogix.scaffold.models.address.ContinentModel",
                        "id": 2,
                        "name": "North America",
                        "code": "NAM",
                        "selected": false
                    },
                    {
                        "@class": "com.bidlogix.scaffold.models.address.ContinentModel",
                        "id": 6,
                        "name": "Oceana",
                        "code": "OCE",
                        "selected": false
                    },
                    {
                        "@class": "com.bidlogix.scaffold.models.address.ContinentModel",
                        "id": 3,
                        "name": "South America",
                        "code": "SAM",
                        "selected": false
                    }
                ]
            }
        }
    },
    "webAppContext": "302"
}
```

</TabItem>

<TabItem value="multiple">

Call 
``` 
https://hove.eu-west-2.bidjs.com/auction-mgt/bdxapi/reference/continents,currencies?clientId=302
```

Response 
``` json
{
    "@class": ".DataPushPayload",
    "pushCode": "REFERENCEDATA",
    "models": {
        "referenceModel": {
            "@class": ".reference.ReferenceDataModel",
            "referenceItemModelMap": {
                "currencies": [
                    {
                        "@class": ".CurrencyModel",
                        "id": 1,
                        "name": "Australian Dollar",
                        "code": "AUD",
                        "selected": false,
                        "symbol": "&#36;"
                    },
                    {
                        "@class": ".CurrencyModel",
                        "id": 2,
                        "name": "British Pound Sterling",
                        "code": "GBP",
                        "selected": false,
                        "symbol": "&#163;"
                    },
                    {
                        "@class": ".CurrencyModel",
                        "id": 3,
                        "name": "Canadian Dollar",
                        "code": "CAD",
                        "selected": false,
                        "symbol": "&#36;"
                    },
                    {
                        "@class": ".CurrencyModel",
                        "id": 28,
                        "name": "Czech koruna",
                        "code": "CZK",
                        "selected": false,
                        "symbol": "&#75;&#269;"
                    },
                    {
                        "@class": ".CurrencyModel",
                        "id": 4,
                        "name": "Euro",
                        "code": "EUR",
                        "selected": false,
                        "symbol": "&#8364;"
                    },
                    {
                        "@class": ".CurrencyModel",
                        "id": 5,
                        "name": "Hong Kong Dollar",
                        "code": "HKD",
                        "selected": false,
                        "symbol": "&#36;"
                    },
                    {
                        "@class": ".CurrencyModel",
                        "id": 16,
                        "name": "Indian Rupee",
                        "code": "INR",
                        "selected": false,
                        "symbol": "&#8360;"
                    },
                    {
                        "@class": ".CurrencyModel",
                        "id": 6,
                        "name": "Indonesian Rupiah",
                        "code": "IDR",
                        "selected": false,
                        "symbol": "Rp"
                    },
                    {
                        "@class": ".CurrencyModel",
                        "id": 7,
                        "name": "Japanese Yen",
                        "code": "JPY",
                        "selected": false,
                        "symbol": "&#165;"
                    },
                    {
                        "@class": ".CurrencyModel",
                        "id": 21,
                        "name": "Lebanese Pound",
                        "code": "LBP",
                        "selected": false,
                        "symbol": "&#46;&#1604;&#46;&#1604;‎"
                    },
                    {
                        "@class": ".CurrencyModel",
                        "id": 8,
                        "name": "Mexican Peso",
                        "code": "MXN",
                        "selected": false,
                        "symbol": "&#36;"
                    },
                    {
                        "@class": ".CurrencyModel",
                        "id": 26,
                        "name": "Namibian Dollar",
                        "code": "NAD",
                        "selected": false,
                        "symbol": "N&#36;"
                    },
                    {
                        "@class": ".CurrencyModel",
                        "id": 9,
                        "name": "New Zealand Dollar",
                        "code": "NZD",
                        "selected": false,
                        "symbol": "&#36;"
                    },
                    {
                        "@class": ".CurrencyModel",
                        "id": 17,
                        "name": "Romania New Leu",
                        "code": "RON",
                        "selected": false,
                        "symbol": "Lei"
                    },
                    {
                        "@class": ".CurrencyModel",
                        "id": 10,
                        "name": "Russian Rouble",
                        "code": "RUB",
                        "selected": false,
                        "symbol": "&#1088;&#1091;&#1073;"
                    },
                    {
                        "@class": ".CurrencyModel",
                        "id": 24,
                        "name": "Saudi Arabian Riyal",
                        "code": "SAR",
                        "selected": false,
                        "symbol": "&#65020;"
                    },
                    {
                        "@class": ".CurrencyModel",
                        "id": 11,
                        "name": "Singapore Dollar",
                        "code": "SGD",
                        "selected": false,
                        "symbol": "&#36;"
                    },
                    {
                        "@class": ".CurrencyModel",
                        "id": 15,
                        "name": "South African Rand",
                        "code": "ZAR",
                        "selected": false,
                        "symbol": "&#82;"
                    },
                    {
                        "@class": ".CurrencyModel",
                        "id": 12,
                        "name": "Swedish Krona",
                        "code": "SEK",
                        "selected": false,
                        "symbol": "&#107;&#114;"
                    },
                    {
                        "@class": ".CurrencyModel",
                        "id": 13,
                        "name": "Swiss Franc",
                        "code": "CHF",
                        "selected": false,
                        "symbol": "Fr"
                    },
                    {
                        "@class": ".CurrencyModel",
                        "id": 25,
                        "name": "Turkish Lira",
                        "code": "TRY",
                        "selected": false,
                        "symbol": "&#8378;"
                    },
                    {
                        "@class": ".CurrencyModel",
                        "id": 14,
                        "name": "US Dollar",
                        "code": "USD",
                        "selected": false,
                        "symbol": "&#36;"
                    },
                    {
                        "@class": ".CurrencyModel",
                        "id": 27,
                        "name": "United Arab Emirates dirham",
                        "code": "AED",
                        "selected": false,
                        "symbol": "&#1583;.&#1573;"
                    },
                    {
                        "@class": ".CurrencyModel",
                        "id": 22,
                        "name": "Yuan Renminbi",
                        "code": "CNY",
                        "selected": false,
                        "symbol": "&#165;‎"
                    },
                    {
                        "@class": ".CurrencyModel",
                        "id": 23,
                        "name": "Zimbabwean dollar",
                        "code": "ZWL",
                        "selected": false,
                        "symbol": "&#90;&#36;"
                    }
                ],
                "continents": [
                    {
                        "@class": "com.bidlogix.scaffold.models.address.ContinentModel",
                        "id": 5,
                        "name": "Africa",
                        "code": "AFR",
                        "selected": false
                    },
                    {
                        "@class": "com.bidlogix.scaffold.models.address.ContinentModel",
                        "id": 4,
                        "name": "Asia",
                        "code": "ASI",
                        "selected": false
                    },
                    {
                        "@class": "com.bidlogix.scaffold.models.address.ContinentModel",
                        "id": 1,
                        "name": "Europe",
                        "code": "EUR",
                        "selected": false
                    },
                    {
                        "@class": "com.bidlogix.scaffold.models.address.ContinentModel",
                        "id": 2,
                        "name": "North America",
                        "code": "NAM",
                        "selected": false
                    },
                    {
                        "@class": "com.bidlogix.scaffold.models.address.ContinentModel",
                        "id": 6,
                        "name": "Oceana",
                        "code": "OCE",
                        "selected": false
                    },
                    {
                        "@class": "com.bidlogix.scaffold.models.address.ContinentModel",
                        "id": 3,
                        "name": "South America",
                        "code": "SAM",
                        "selected": false
                    }
                ]
            }
        }
    },
    "webAppContext": "302"
}
```
</TabItem>
</Tabs>

## Errors 

| code | example |
| :--- | -------: |
| 401 UNAUTHORIZED | \{ error : "Log in" \} |
| 422 Unprocessable Entry | \{ error : ["\<field_name\>"] \} |