---
id: introduction
title: Introduction
sidebar_label: Introduction
slug: /
---

**Thank you for choosing BidJS!**

This document aims to prepare you for installing and maintaining your BidJS software, along with explaining how elements of the software work.

## What is BidJS

BidJS provides a way to plug comprehensive auction functionality into almost any website.

With constant development, a proven track record, and the ability to handle auctions at scale, it allows auctioneers to dramatically increase their audience and buyer base.

## Website Requirements

BidJS is a plugin, which means you will need to have a website and the ability to do the following during the installation process:

* Adjust your websites HTML code
* Edit the `HEAD` of your website, to include the BidJS `script` tags

Additionally, if you want to add your own custom styling to BidJS, you will need to adjust your website CSS.

## BidJS Technologies

We use the following technologies. It's always an advantage if you or your technical team has knowledge of the following:

* Javascript
* HTML
* CSS (Bootstrap)

### What if I don't have a website or the knowledge to make these changes?

We are partnered with [The Juniper Studio](https://www.thejuniperstudio.com/), who are specialists at providing beautiful and affordable sites and solutions.

If you need someone to handle this, we recommend them highly.

## Your details

In the next section, we'll be covering the [installation](developer/gettingStarted/installation) of BidJS

In order to complete this, you'll need several details that we provide you with upon signing up. If you don't have these, or haven't signed up yet, please [contact us](https://bidjs.com/#contact) and we'll get this sorted for you.
