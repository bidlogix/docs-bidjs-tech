---
id: navigation
title: Navigation & Hooks
sidebar_label: Navigation & Hooks
---

## Navigation

BidJS comes with no navigation built in. As the application is being placed on your website, it's preferable to hook in to your existing navigation.

We explain how to do that in this section.

:::info
All examples given are assuming that BidJS is on the root page of your domain. If this is not the case, the given URLs will need to be adjusted accordingly.
:::

### Create Account
The create account / signup page (not required with SSO)
```html {5}
<ul class="nav navbar-nav navbar-right">
  <li>...</li>
  <li>...</li>
  <li class="x-bidlogix--authenticated-hide hidden">
    <a href='/#!/account'>Create Account</a>
  </li>
</ul>
```

### Invoices
You may not be using this part of the application, so may not require this functionality.

```html {5}
<ul class="nav navbar-nav navbar-right">
  <li>...</li>
  <li>...</li>
  <li class="x-bidlogix--authenticated-show hidden">
    <a href='/#!/invoices'>Invoices</a>
  </li>
</ul>
```

### Login
The page for users to authenticate

```html {5}
<ul class="nav navbar-nav navbar-right">
  <li>...</li>
  <li>...</li>
  <li class="x-bidlogix--authenticated-hide hidden">
    <a href='/#!/login'>Log In</a>
  </li>
</ul>
```

### Logout
To log out the user

```html {5}
<ul class="nav navbar-nav navbar-right">
  <li>...</li>
  <li>...</li>
  <li class="x-bidlogix--authenticated-show hidden">
    <a href='/#!/logout'>Logout</a>
  </li>
</ul>
```

### My Sales
If you are selling on behalf of a vendor this provides high level insights to all items being sold for that vendor, without the need to grant full admin access. See [customer documentation](https://support.bidlogix.net/support/solutions/folders/44000607835) for more information.

```html {5}
<ul class="nav navbar-nav navbar-right">
  <li>...</li>
  <li>...</li>
  <li class="x-bidlogix--vendor-show hidden">
    <a href='/#!/mySales'>My Sales</a>
  </li>
</ul>
```

### My Settings
Where users visit to change their account setting

```html {5}
<ul class="nav navbar-nav navbar-right">
  <li>...</li>
  <li>...</li>
  <li class="x-bidlogix--authenticated-show hidden">
    <a href='/#!/account'>My Settings</a>
  </li>
</ul>
```

### Past Auctions
Past Auctions allow you to display all auctions that have ended and have been Reconciled and / or Archived. Auctioneers can choose to display the winning bid values or not - please get in touch with our support team to advise the option you would prefer.

```html {5}
<ul class="nav navbar-nav navbar-right">
  <li>...</li>
  <li>...</li>
  <li class="nav__item--archived">
    <a href='#!/auctionsArchived'>Past Auctions</a>
  </li>
</ul>
```


## Hooks

Sometimes there's a need for BidJS to have an effect on elements outside of the BidJS Container, such as navigation.

Fortunately, we cater for this. We'll explain how to implement each "Hook" below.

:::caution
We use a css class of `.hidden` to hide elements. If your site doesn't have Bootstrap installed, then you'll need to add the following CSS rule to ensure this functionality works
```css
.hidden {
  display: none;
}
```
:::

### Login state
You're able to show or hide elements on your site, based on whether the user is logged in.

This is particularly useful for "Login" or "Create Account" items in your navigation.

#### Show for authenticated users
You can attach the `.x-bidlogix--authenticated-show` class to any element to show it for authenticated users.
```html {4}
<ul class="nav navbar-nav navbar-right">
  <li>...</li>
  <li>...</li>
  <li class="x-bidlogix--authenticated-show hidden">
    <a href="/#!/account">My Account</a>
  </li>
</ul>
```

#### Hide for authenticated users
You can attach the `.x-bidlogix--authenticated-hide` class to any element to hide it for authenticated users.
```html {4}
<ul class="nav navbar-nav navbar-right">
  <li>...</li>
  <li>...</li>
  <li class="x-bidlogix--authenticated-hide hidden">
    <a href="/#!/login">Log In</a>
  </li>
</ul>
```

#### Welcome message
Should you wish, you can add the display name of the authenticated user to your page, such as in your navigation.

BidJS will insert the display name in any element with the class `.x-bidlogix--templated-user`

```html {2}
<p class="x-bidlogix--authenticated-show hidden">
  Welcome, <span class="x-bidlogix--templated-user"></span>
</p>
```

### User Permissions
There are some navigation items that you may want to show or hide based on user permissions

#### Admin users
You can attach the `.x-bidlogix--administrator-show` class to any element to show it for administrator users only.
```html {4}
<ul class="nav navbar-nav navbar-right">
  <li>...</li>
  <li>...</li>
  <li class="x-bidlogix--administrator-show hidden">
    <a target="_blank" rel="noopener" href="#">Admin Login</a>
  </li>
</ul>
```
:::info
You may note that the `<a>` tag in the example has an empty `href` attribute.

When this item is triggered, the `href` is automatically adjusted to point to the appropriate administration URL.
:::

#### Vendors
Should you have vendors who will be accessing your website, you can restrict the element on your site to being displayed only for users with that permission

```html {4}
<ul class="nav navbar-nav navbar-right">
  <li>...</li>
  <li>...</li>
  <li class="x-bidlogix--vendor-show hidden">
    <a href='/#!/mySales'>My Sales</a>
  </li>
</ul>
```
