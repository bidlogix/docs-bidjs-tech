---
id: analytics
title: Analytics
sidebar_label: Google Analytics
---
You can configure BidJS to send tracking to your own Google Analytics account, as instructed in our [Options](../options.md##google-analytics-tracking) document.

## Dimensions
:::info
Available in v4.0+
:::

With all tracking we also send two further bits of information.

### User UUID
We track the user's UUID (unique identifier) for any tracking events where the user is logged in.
This is recorded under the `userId` analytics dimension.

### BidJS Version
So that we're able to accurately measure what version clients and users are seeing, we also track the BidJS version being used.
This is recorded under the `dimension1` [Custom Dimension](https://support.google.com/analytics/answer/2709828?hl=en).

To see this within your own analytics information you will need to create a Custom Dimension, and ensure it is at index `1`.

## Page Navigation
As standard, we'll send any Page Navigation events to Google Analytics. Page navigation is when the main part of the URL changes.

### Tracked Pages
#### Auction
* Auction List
* Auction Calendar
* Auction Details
* Item Details
* Webcast (Live)
* Marketplace
* Archived Auctions

#### User
* Login
* Logout
* Create Account
* My Settings
* Forgotten Password
* Change Password
* Account Activation

#### Other
* Search
* My Bids
* My Sales
* Invoicing
* Not Found

### Modal Opens

:::info
Available in v4.0+
:::

Modals are also sent as page navigations

#### Auction Timed / Prebid
  * Auction Terms and Conditions
  * Autobid Confirm
  * Bid Confirm
  * Bid History
  * Buy Confirm
  * Contact Seller
  * Listing Image Gallery
  * Offer Confirm
  * Sale Information
  * Tender Confirm
  * Transfer Confirm

#### Auction Webcast
  * Collection Details
  * Payment Information
  * Removal Information
  * Contact Auction Information

#### Other
  * Privacy
  * Terms and Conditions

## Events
:::info
Available in v4.0+
:::

Certain Events are also tracked, which do not cause a page change, nor are they modal opens.

Google Analytics [Events](https://support.google.com/analytics/answer/1033068?hl=en) follow the format: `(Category, Label, Action, Value)`.

All Events contain `Category` and `Label`, whilst some also contain `Action`, and a few contain `Value` (which must be a number).

The currently tracked Events are mapped below. If there is a particular Event that you believe should be tracked, please be sure to [contact support](https://support.bidlogix.net/) with your feedback.

### Category: Auction
** Action ** / ** Label ** / ** Value **
* `Filter Category` / `Auction Uuid`
* `Filter Country` / `Auction Uuid`
* `Filter Open` / `Auction Uuid`
* `Filter Search` / `Auction Uuid`
* `Filter Starred` / `Auction Uuid`
* `Register` / `Auction Uuid`
* `Toggle Video` / `isHidden`
* `View Page` / `Auction Uuid` / `pageNumber`

### Category: Invoice
** Action ** / ** Label **
* `View` / `Invoice ID`

### Category: Listing
** Action ** / ** Label ** / ** Value **
* `AutoBid` / `Listing Uuid` / `amount`
* `Bid` / `Listing Uuid` / `amount`
* `Bid Confirm Opt Out` / `Listing Uuid`
* `Browse Related` / `Listing Uuid`
* `Buy` / `Listing Uuid`
* `Contact Seller` / `Listing Uuid`
* `Offer` / `Listing Uuid`
* `Tender` / `Listing Uuid`
* `Tender PCT` / `Listing Uuid`
* `Transfer` / `Listing Uuid`
* `Star` / `Listing Uuid`

### Category: SEARCH
** Action ** / ** Label **
* `Search Input` / `Search term`

### Category: User
** Action **
* `Login`
* `Create Account`
* `Logout`
* `Login Failed`
