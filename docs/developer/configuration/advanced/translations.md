---
id: translations
title: Translations
sidebar_label: Translations
---
BidJS works with [Weglot](https://weglot.com/?fp_ref=bidlogix) to allow automatic translations across your site and your auction plugin.

## Configuration
There are many options available when configuring your Weglot integration, which are all listed within the [Weglot setup guides](https://weglot.com/documentation/setup-guides/?fp_ref=bidlogix). There you will find steps to get set up that are specific to your website, however there are certain configuration options available that will ensure the best compatibility with BidJS.

Here’s an example of how your JavaScript implementation might look with our BidJS compatibility suggestions in place:
```html
<script type='text/javascript' src='https://cdn.weglot.com/weglot.min.js' />
<script>
  Weglot.initialize({
    api_key: '<<YOUR_API_KEY>>',
    auto_switch: true,
    auto_switch_fallback: 'en',
    cache: true,
    dynamic: '.bidlogix-app'
  });
</script>
}}
```

### auto_switch
This will switch the language depending on the user's browser language.

### auto_switch_fallback
If setting `auto_switch` above, this is the language to fall back to, if the user's language isn't supported.

### cache
Setting `cache` to true will drastically improve the performance of Weglot on your website, reducing the visible impact any translations may have on load times.

### dynamic
By default, Weglot performs translations on the page load. However, as BidJS is a "Single Page Application", translations will need to be updated as the user navigates around. By setting `dynamic: '.bidlogix-app'`, this should be taken care of.

Please see Weglot’s documentation for details on any further configuration options, having optimised your integration with BidJS.
