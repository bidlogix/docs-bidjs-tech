---
id: oauth
title: OAuth
sidebar_label: OAuth
---
We support the usage of other OAuth servers, both for users with existing authentication and for those without any existing OAuth setup.

## Requirements

* You have your OAuth server set*up
* The OAuth server must have an endpoint to return user details based on the token (commonly referred to as a `/user` endpoint). Below is an example of how the user endpoint URL might look:

* The OAuth server authenticates the user onto the same domain as the website on which BidJS is located
* The Authentication token is published as either a cookie or localStorage item onto the client browser
* If using a cookie, the cookie domain and path should allow access on the domain on which BidJS is located

```javascript
'https://<YOUR_AUTH0_DOMAIN>/userinfo'
```

### User Endpoint Requirements

The user endpoint should return the following information to ensure your users don't need to supply this upon logging in:

- Email address
- Forename
- Surname
- Address Line 1
- City
- Postcode
- Country
- Tel (Daytime)

Additionally, we accept the following information, which will be inserted into the user account if supplied:

* Username
* ExternalRef
* Company Name
* Address Line 2
* County

We also accept the following Boolean for user outbid email configuration:

* `outbidEmailsOk` - If the property is set to `true`, outbid notification emails will be sent to the user. If set to `false`, they will not. If omitted or set to an empty string, the web application outbid emails configuration will be used instead.

## Scenario With an Existing Authentication

If you have an existing OAuth server for authentication, you can integrate it with BidJS as follows:

* Ensure that your existing OAuth server is set up to authenticate users on the same domain as the website on which BidJS is located.
* Make sure that the OAuth server issues an authentication token that can be published either as a **cookie** or a **localStorage** item on the client browser.
* The token name (`tokenName`) should match the name of your existing cookie or localStorage item.
* The OAuth server must have an endpoint to return user details based on the token (often referred to as a `/user` endpoint).

## Scenario Without an Existing Authentication

If you do not have an existing authentication, you can still integrate OAuth with BidJS by setting up an OAuth server and obtaining an authentication token.

* The `tokenName` can be anything you prefer if you don't have an existing token name. A common value is `authToken`.
* You will need to store the token in either a **cookie** or **localStorage** for the client browser to access.
* The `loginUrl` should include the `scope` parameter, which should contain `openid profile email` to ensure that all necessary user information is gathered.

### Example Login URL

Your `loginUrl` should look something like this:

```javascript
loginUrl: 'https://www.yourdomain.com/auth?redirect_to=<<REDIRECT>>&scope=openid%20profile%20email'
```



In this scenario, it's common to create a new page called **`/auth`** to handle the authentication response and extract the access token from the URL.

### Example Auth Page to Store the Token

Below is an example of how you can create an `/auth` page to extract the `access_token` from the URL and store it either in a **cookie** or **localStorage**.

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Auth Page</title>
</head>
<body>
  <script>
    // Extract the access token from the URL hash
    const hash = window.location.hash.substring(1);
    const params = new URLSearchParams(hash);
    const accessToken = params.get("access_token");

    if (accessToken) {
      // Store the token in localStorage
      localStorage.setItem("authToken", accessToken);
      // Optionally, store the token as a cookie
      document.cookie = "authToken=" + accessToken + "; path=/";

      // Redirect to the main page after storing the token
      window.location.href = "/";
    } else {
      console.error("Access token not found in URL");
    }
  </script>
</body>
</html>
```

## Setup

1. Contact `support@bidlogix.net`, supplying us with the URL, HTTP Method (e.g., GET / POST), and an example response for your user endpoint. We will then update this on your account.
2. Add the OAuth options to your BidJS configuration, as below:

```javascript
  window.bidjs = {  
    config: {    
      ...
    },  
    modules: {    
      ...
    },  
    options: {
      oAuth: {
        isTokenLocalStorage: true,  // Set to false if using a cookie
        loginUrl: 'https://www.yourdomain.com/auth?redirect_to=<<REDIRECT>>',
        logoutUrl: 'https://www.yourdomain.com/logout',
        tokenName: 'authToken'  // This can be any name you choose
      }
    }
  }
```

If `isTokenLocalStorage` is `false`, then we assume your OAuth credentials are stored as a cookie.

If you don't have an existing authentication, then you need to add the `scope` parameter to the `loginUrl`.

`<<REDIRECT>>` will be automatically replaced with the current page URL. But if you want to specify a different URL, you can do so.

The `tokenName` is either the name of the cookie or the localStorage item.

&#x20;
