---
id: search
title: Search
sidebar_label: Search
---
:::info
Available in v5.0+
:::

BidJS provides a Search component, which can be placed anywhere on your website.

## Example
An example of the Search component can be seen on our [Search Demo Site](https://bidjs.com/demoSearch).

In this example, the Search has been implemented on a completely separate page to the rest of the BidJS app. Where you choose to implement this is entirely up to you.

## Implementation
### Dependencies
The dependencies and configuration for the Search component are the same as detailed in the [Installation Guide](../../gettingStarted/installation.md#step-1-dependencies-and-configuration) for the main BidJS application.

### HTML Snippet
Once the above dependencies are on your page, you can place the following HTML snippet where you would like your Search bar to appear.

```html
<div class='container bidjs-app' id='bidjs__search'></div>
```
The Search bar should now show in the "Vanilla" unstyled form.

### Styling
You can then apply your own CSS to fit the rest of your website.

## Configuration
### Options
Please see [Search Options](../options.md#search) for documentation on the configuration of the Search application.

### Standalone
If you are using the Search application as a standalone component (on a page separate to the remainder of BidJS) you will need to configure the BidJS options so that users clicking on a search result are sent to the page with BidJS installed.

This can be configured by defining the location of your BidJS installation as per the following example:
```javascript {6}
  window.bidjs = {  
    config: {    
      ...
    },
    modules: {
      lotDetails: '/my-separate-page/#!/auctions/%AUCTION_ID%/listings/%ITEM_ID%',
      marketplaceLotDetails: '/my-separate-marketplace-page/#!/marketplace/listings/%ITEM_ID%'
    }
  }
```
If the above configuration is set on the page that your Search Application is installed on, then when a user clicks on a search result, they will be taken to the URL defined above. The `%AUCTION_ID%` and `%ITEM_ID%` references will be replaced appropriately.

Note that the `marketplaceLotDetails` property is only relevant if you are using the Marketplace functionality, and creating Marketplace listings.
