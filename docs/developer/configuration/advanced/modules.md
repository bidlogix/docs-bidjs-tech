---
id: modules
title: Modules
sidebar_label: Modules
---
BidJS provides you with the ability to disable or enable certain parts of the application.

## Disabling Modules
BidJS is currently in the process of moving to "Modules". This means we give you the ability to utilise the parts of the application that you need, and to turn off parts which you don't. This results in a more tailored, better user experience for your customers.

As covered in the [installation](/developer/gettingStarted/installation.md) guide, the current modules definition is as follows:

```javascript {5-14}
  window.bidjs = {  
    config: {    
      ...
    },  
    modules: {    
      auctionDetails: '#!/auctions/%AUCTION_ID%',
      auctionsArchived: true,
      invoices: true,
      lotDetails: '#!/auctions/%AUCTION_ID%/listings/%ITEM_ID%',
      mySales: true,
      login: true,
      register: true
    },  
    options: {
      ...
    }
  }
```

In most cases, it's safe to leave this definition as-is. However, if you know you will never use (for example) the invoicing part of our application, it's safe to disable this. This will prevent other parts of the application sending users to the invoicing module.

```javascript {8}
  window.bidjs = {  
    config: {    
      ...
    },  
    modules: {    
      auctionDetails: '#!/auctions/%AUCTION_ID%',
      auctionsArchived: true,
      invoices: false,
      lotDetails: '#!/auctions/%AUCTION_ID%/listings/%ITEM_ID%',
      mySales: true,
      login: true,
      register: true
    },  
    options: {
      ...
    }
  }
```

## Default Module

You are also able to set a module as the default module to load on a page, instead of the home.

This might be useful if you wanted to display one module on another completely independent page on your site.

### Standalone Module

The below example would show the `auctionsArchived` module only, on this page.
By adjusting the URLs of the `auctions` page to point to the page on the site where the other modules are, it ensures that you're still able to link through to view the auction.

```javascript {6-7,16}
  window.bidjs = {  
    config: {    
      ...
    },  
    modules: {    
      auctionDetails: '//mysite.com/myauctionpage/#!/auctions/%AUCTION_ID%',
      auctionsArchived: true,
      invoices: false,
      lotDetails: false,
      mySales: false,
      webcast: false,
      login: false,
      register: false
    },  
    options: {
      defaultModule: 'auctionsArchived'
    }
  }
```

### Empty Module

There may also be cases where you wish to have the BidJS installation running, but hidden.
This may be desirable if you still want to show the [navigation](/developer/configuration/navigation.md) statuses on other pages of your site without auctions.

To do this, you can disable all the modules, and set the defaultModule

```javascript {6-13,16}
  window.bidjs = {  
    config: {    
      ...
    },  
    modules: {    
      auctionDetails: false,
      auctionsArchived: false,
      invoices: false,
      lotDetails: false,
      mySales: false,
      webcast: false,
      login: false,
      register: false
    },  
    options: {
      defaultModule: 'empty'
    }
  }
```
