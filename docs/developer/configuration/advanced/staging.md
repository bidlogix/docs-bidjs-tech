---
id: staging
title: Staging Setup
sidebar_label: Staging Setup
---
In some cases we may provide clients with temporary access to "staging" instances, to test alpha versions or unreleased features on isolated stacks. In this case, you will need to adjust the [installation configuration](../../gettingStarted/installation.md) as follows.

## Configuration
For connecting to the staging stack, you will have to add one more configuration setting, as below

```html {8}"
<script>
  window.bidjs = {  
    config: {
      clientId: '<<YOUR_CLIENT_ID>>', // e.g. demonstration
      googleMapsApiKey: '<<YOUR_KEY>>',
      region: '<<YOUR_SERVER_REGION>>', // e.g. eu-west-2
      server: '<<YOUR_SERVER_NAME>>', // e.g. hove
      isStaging: true
    }
  }
</script>
```

## Application URL
Any reference to `<<APPLICATION_URL>>` in the [installation](../../gettingStarted/installation.md) will instead be in the format of the format of `//{Server Name}.{Server Region}.staging.bidjs.com`.
