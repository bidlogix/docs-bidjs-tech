---
id: callback
title: Callback
sidebar_label: Callback
---

## BidJS Events
Some websites require functionality beyond that which BidJS requires.

In order to help support this, we expose many of the events which happen within BidJS, so that third party developers can react to them.

### Listening to Events

We provide a `callback` option within the BidJS configuration. This can be overwritten with your own functionality

```javascript {9-11}
  window.bidjs = {  
    config: {    
      ...
    },  
    modules: {    
      ...
    },  
    options: {
    ...
    }, 
    callback: function(event) {
      // you can have any of your own logic here
    }
  }
```

### Event format
We have a standardised format for all events, so you can write your own functionality to handle multiple types of event.

Your callback method will always be called with an event in the following format
```json
{
  "action": "BIDJS_EVENT_NAME",
  "data": {
    // any related information (if any) will be inserted here
  }
}
```

### Events
There are numerous events that are broadcast.

#### General Functionality

* `BIDJS_INITIALISED` / `BIDJS_MODULES_INITIALISED` - BidJS Scripts first begin running
* `BIDJS_LOADED` / `BIDJS_MODULES_LOADED` - BidJS has finished loading
* `BIDJS_NAVIGATED` / `BIDJS_MODULES_NAVIGATED` - The user has changed page on BidJS
* `BIDJS_AUTHENTICATED` / `BIDJS_UNAUTHENTICATED` - The user login state has changed

#### Auction Functionality

* `AUCTION_SUBSCRIBED` - The user is receiving live updates for an auction
* `BID_PLACED` / `BID_CANCELLED` / `BID_REINSTATED` - Informs of a change in the bid, or a new bid for a listing
* `REGISTRANT_UPDATED` - Registration state has changed for a user
* `SALE_ADDED` / `SALE_WITHDRAWN` - A listing has been added or removed on the Auction
* `SALE_COMPLETED` / `SALE_STARTED` - An item has been completed (sold, passed etc), or been started (re-listed, re-offered etc)
* `SALE_INCREMENT_SET` / `SALE_NEXT_BID_SET` - The increment or next bid value of a listing has changed
* `SALE_UNIT_BIDDING_TYPE_SET` - Advises of a change in how the units for a listing are being priced
* `WEBCAST_FEED_SET` - The webcast video / audio feed has changed properties
* `WEBCAST_INPLAY_SET` - The webcast inplay lot has changed
* `WEBCAST_INPLAY_STATE_SET` - The webcast has been paused / resumed
* `WEBCAST_INPLAY_WARNING` - The sale warning for inplay item has updated
* `WEBCAST_MESSAGE_SET` - A webcast message has changed
