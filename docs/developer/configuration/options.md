---
id: options
title: Options
sidebar_label: Options
---

We provide several options for the customisation of the BidJS functionality.

:::info
All options are defined within the BidJS configuration you added in the [installation](developer/gettingStarted/installation.md) process.
:::

## General Behaviour

### Custom Scroll Position on navigation
Some of our customers prefer the page to scroll to a certain position as the user navigates.

By default, BidJS will scroll to the top of the BidJS container on navigation between the different BidJS pages.

Should you wish to adjust this, you can pass in the ID of any element on your site, which you wish the page to scroll to.
E.g. if your navigation has an ID of `#main-navigation` and you wish to scroll to this, the following configuration may be applied

```javascript {6}
  window.bidjs = {  
    config: {    
      ...
    },
    options: {
      scrollOnNavigationElementId: 'main-navigation'
    }
  }
```

### Default tab on Home page
By default, if you have both Marketplace and Auctions, two tabs will show on the homepage, defaulting to Auctions.

You can use the following option to set this to Marketplace, where preferred.

```javascript {6}
  window.bidjs = {  
    config: {    
      ...
    },
    options: {
      setMarketplaceDefault: true
    }
  }
```

### Image Display Options
It's possible to customise how imagery is displayed on your BidJS installation.

#### Cropping
You are able to change how images are cropped / resized

```javascript {6}
  window.bidjs = {  
    config: {    
      ...
    },
    options: {
      imageCropMode: 'fill'
    }
  }
```

**Available options:**

* `fill` - (Default) The image fills the space, and crops any part of the image which won't fit in this space
* `fit` - The image fills the space as much as possible without any cropping. This will mean images could appear inconsistently sized when next to each other
* `pad` - Similar to `fit`, the image fills the space as much as possible. However, any empty space is "padded" with colour, to ensure all images are sized consistently.
* `scale` - Stretches / squashes the image to fit


#### Pad Color

Note that with `pad` you are also able to define the `imagePadColor`, as below:
```javascript {6-7}
  window.bidjs = {  
    config: {    
      ...
    },
    options: {
      imageCropMode: 'pad',
      imagePadColor: 'rgb:3c3c3c'
    }
  }
```

#### Gravity

This only has an effect when using `fill` cropMode, and has a default of `auto`.

When used in combination with `fill` this intelligently crops the image to try and focus on the "most interesting" part.

Below is an example of how setting `imageGravity: 'auto'` adjusts how an image may be cropped.

**Gravity Center**

<img src='https://res.cloudinary.com/bidlogix-test/image/upload/ar_4:3,b_rgb:3c3c3c,c_fill,dpr_1,f_auto,q_auto,w_400/v1601390759/live_bdx/bdx/6_zlm9s2.jpg' />

**Gravity Auto**

<img src='https://res.cloudinary.com/bidlogix-test/image/upload/ar_4:3,b_rgb:3c3c3c,c_fill,g_auto,dpr_1,f_auto,q_auto,w_400/v1601390759/live_bdx/bdx/6_zlm9s2.jpg' />

Should you wish to opt out of this behaviour, you can set `imageGravity: 'center'`.

```javascript {6-7}
  window.bidjs = {  
    config: {    
      ...
    },
    options: {
      imageCropMode: 'fill',
      imageGravity: 'auto' // note that these are the default settings
    }
  }
```

### Unsupported Browser Message
By default, we display the following message to unsupported browsers
> Your browser is not supported. Please use a browser such as Google Chrome or Microsoft Edge to ensure full functionality

Should you wish, you can adjust this by adding the following
```javascript {6}
  window.bidjs = {  
    config: {    
      ...
    },
    options: {
      unsupportedBrowserMessage: '<<your text here>>' // e.g. "We do not support your browser. Please ensure your browser is up to date."
    }
  }
```

### BidJS Branding
By default, some BidJS branding (e.g. "Auction Software by Bidlogix") appears at the bottom of the application. This helps us grow as a business!

If you want to remove this branding, use the following option.

```javascript {6}
  window.bidjs = {  
    config: {    
      ...
    },
    options: {
      disableBranding: true
    }
  }
```

## SEO

### Dynamic Page Title & Meta Description
BidJS can update the page title and meta descriptions dynamically, if you allow it.

This is recommended, and may result in your items being ranked higher with Google Search results.

```javascript {6-7}
  window.bidjs = {  
    config: {    
      ...
    },
    options: {
      allowMetaDescriptionChange: true,
      allowTitleChange: true
    }
  }
```

### Google Analytics Tracking
If you have Google Analytics on your site, you can plug this in, to pass more information on BidJS usage to your analytics account.
```javascript {6}
  window.bidjs = {  
    config: {    
      ...
    },
    options: {
      analyticsCode: 'UA-1234567-1'
    }
  }
```

## Timed / Pre-bidding Pages

### Disable Bid History
This option disables the Bid History modal

If this option is set to `true`, a count of bids will show, but no details as to the previous bidders or amounts

```javascript {6}
  window.bidjs = {  
    config: {    
      ...
    },
    options: {
      disableBidHistory: true
    }
  }
```

:::info
Please note that irrespective of this setting, Bid History does not show for Marketplace auctions
:::

### Disable Consecutive Bids
This option disables users from bidding against themselves, once reserve has been reached.

If this option is set to `true`, then once an item hits reserve, the highest bidder will not be able to bid any higher, until they've been outbid.

This can prevent cases of a user bidding against themselves accidentally, or otherwise inflating the sale price of an item.

```javascript {6}
  window.bidjs = {  
    config: {    
      ...
    },
    options: {
      disableConsecutiveBidding: true
    }
  }
```

### Items Per Page
#### v4.1+
This option allows you to set the default number of lots that will show on a timed auction or webcast pre-bidding page. The user also has the ability to select a different number of lots to show, depending on their preferences.

Below we show the configuration for the current defaults. You can change these as desired.

:::warning
We recommend against setting the default or options too high. A page displaying too many lots can cause performance issues on slower devices, such as mobiles
:::

```javascript {6-7}
  window.bidjs = {  
    config: {    
      ...
    },
    options: {
      itemsPerPageDefault: 30,
      itemsPerPageOptions: [18, 24, 30, 48, 60, 90]
    }
  }
```

### Pagination controls at the top
#### v5.0+
This option when enabled will display pagination controls at the top of the timed view.  Pagination controls will also still appear at the bottom of the view.

```javascript {6}
  window.bidjs = {  
    config: {    
      ...
    },
    options: {
      enablePaginationTop: true
    }
  }
```
Here is an example of the auction details view with pagination controls at the top.
![alt text](/img/features/paginationTop.png "Pagination controls above the fold")

### Social Links

As of v4, timed and prebidding pages have optional social links, for your users to share your auctions and listings, increasing user engagement.

You can either opt out of this behaviour, or customise which links are shown.

**By default, the following social buttons are displayed:**
* email
* messenger
* whatsapp
* facebook
* linkedin
* twitter
* pinterest (listing page only)

#### Opt out

```javascript {6}
  window.bidjs = {  
    config: {    
      ...
    },
    options: {
      socialLinks: false
    }
  }
```

#### Add / remove individual social links

```javascript {6-26}
  window.bidjs = {  
    config: {    
      ...
    },
    options: {
      socialLinks: {
        facebook: true,
        messenger: false,
        twitter: false,
        telegram: true,
        whatsapp: false,
        pinterest: false,
        vk: false,
        odnoklassniki: true,
        reddit: false,
        tumblr: true,
        mailru: true,
        livejournal: false,
        viber: true,
        workplace: false,
        line: true,
        weibo: true,
        pocket: false,
        instapaper: true,
        email: true
      }
    }
  }
```

### Reserve Label Visibility

Some of our clients have requested the ability to only show the reserve status labels (e.g. `Below Reserve`, `Reserve Met`, `No Reserve`) to users who have already bid on the lot. This functionality can be enabled with the below option.

```javascript {6}
  window.bidjs = {  
    config: {    
      ...
    },
    options: {
      biddingRequiredForReserve: true // default: false
    }
  }
```

## Webcast Pages

### Image Carousel Autoplay
If you desire your webcast image carousels to automatically cycle through the images, this can be configured as follows.

```javascript {6}
  window.bidjs = {  
    config: {    
      ...
    },
    options: {
      webcastGalleryAutoplayInterval: 3000 // the time in milliseconds between slides
    }
  }
```

If the user interacts with the carousel (e.g. selecting an image or a thumbnail) the autoplay will stop for that user on the item.

### Video-Centric Webcast View
Some webcast auctioneers use the video as their primary way of displaying the listings.

In this case, it can be useful to enable this option, to prioritise the webcast video over the listing imagery.

```javascript {6}
  window.bidjs = {  
    config: {    
      ...
    },
    options: {
      isWebcastVideoCentric: true
    }
  }
```
Here is an example of this view
![alt text](/img/features/videoCentricView.png "VideoCentric view")

## Search
The [Search](developer/configuration/advanced/search.md) component has several configuration options of note.

### Search Input Placeholder
You can define your own content for the placeholder in the Search Input. This can be defined as either a `string` or a `function` that returns a `string`.


```javascript {6} title="String Example"
  window.bidjs = {  
    config: {    
      ...
    },
    options: {
      searchPlaceholder: 'Search our Machinery'
    }
  }
```

```javascript {6} title="Function Example"
  window.bidjs = {  
    config: {    
      ...
    },
    options: {
      searchResultsNone: function() {
        // Whatever logic you need here
        // e.g. determining correct string based on language
        return 'Search our Machinery'
      }
    }
  }
```

### "No Results" Message
You can define your own content for the "No Results" message when Searching. This can be defined as either a `string` or a `function` which returns a `string`.

If the value contains `{0}` this will be replaced with the term being searched for.


```javascript {6}
  window.bidjs = {  
    config: {    
      ...
    },
    options: {
      searchResultsNone: 'There are no results for the search term "{0}"'
    }
  }
```

```javascript {6}
  window.bidjs = {  
    config: {    
      ...
    },
    options: {
      searchResultsNone: function() {
        // Whatever logic you need here
        // e.g. determining correct string based on language
        return 'There are no results for the search term "{0}"'
      }
    }
  }
```
