---
id: 'v4.3'
title: 'v4.3 Upgrade Guide'
sidebar_label: 'v4.3 Upgrade Guide'
---

The following changes may be required for upgrading to v4.3.

:::info
Please note if you are using [Loose Versioning](./versioning.md#loose-versioning) and are on v4 you will receive this upgrade automatically.
:::

**Estimated Time to Upgrade: 0-2 hours**

## 1 - "Awaiting Image" Placeholders
Lots and Listings in Webcast Prebidding and Timed auctions without any imagery will now show a placeholder.

<img src='https://res.cloudinary.com/bidlogix-staging/image/upload/b_rgb:3c3c3c,c_fill,g_auto,dpr_1,f_auto,q_auto,w_400/
v1640087428/live_bdx/bdx/Screenshot_from_2021-12-21_11-50-09.png' />

This has the potential to conflict with any custom placeholder implementations, though is unlikely to affect most installations.

## 2 - List Layout View on Auctions
Users now have the ability to switch between the standard "Grid" layout and a new "List" layout within Webcast Prebidding and Timed auctions.

<img src='https://res.cloudinary.com/bidlogix-staging/image/upload/ar_4:3,b_rgb:3c3c3c,c_fill,g_auto,dpr_1,f_auto,q_auto,w_400/v1640087072/live_bdx/bdx/Screenshot_from_2021-12-21_11-44-14.png' />

We recommend verifying that any existing style customisations you have made will continue to work as expected with this layout.
