---
id: 'versioning'
title: 'Versioning'
sidebar_label: 'Versioning'
---

BidJS follows [semantic versioning](https://semver.org/).

This means versions look like the following: `v1.2.3`.

* We change the first part of the version when we do a major, breaking change. This normally means that some effort is required to implement the upgrade
* We change the second part of the version when we do a minor change, usually in a backwards-compatible manner. This could be a new feature.
* We change the third part of the version when we do bugfixes. In reality, we don't expose this third number, so you'll always get bugfixes for your version as soon as we push them.

## Versioning Options
In your setup, you will need to put in a version to your BidJS scripts.

You have a choice in how you subscribe to versions

### Loose Versioning
If the current version is `5.X.X`, you can set your version as `5`.

This means you'll get any bugfixes or new features within `5.x.x` as soon as we ship them.

**We recommend using loose versioning for most installations**, as it means you're more likely to be up to date, getting all of our new functionality.

### Strict Versioning
If the current version is `5.X.X`, you can simply put `5.X` as all of your versions.

This means that you'll get any bugfixes for `5.X.X` as soon as we ship them.

However, it also means that you'll need to make a manual change once the next `5.X` version comes out.

**We recommend using strict versioning if you have many customisations**, such as heavy CSS changes, or reliance on the BidJS Events as mentioned in our [Advanced Configuration](../configuration/advanced/callback.md).
