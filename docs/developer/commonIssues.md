---
id: commonIssues
title: Common Issues
sidebar_label: FAQs & Common Issues
---

## FAQs

### Getting Started

#### What browsers and devices are supported?

BidJS supports all moden major browsers and devices, as detailed in [our Supported Browsers document](https://support.bidlogix.net/support/solutions/articles/44000457256-supported-browsers).


#### Where do I get my configuration details from?

Your configuration details are sent to you once you have a signed contract with us. Until then, you're able to use the [demonstration details](./gettingStarted/installation.md#demonstration-details).


#### Do I need to make any server-side changes to implement BidJS?

No, all the routing between different pages is handled on the client with "hashbang" URLs, which require no configuration changes.


#### Do I need to make any Mail / DNS changes to ensure mail delivery?

When we provide you with your configuration details, we'll also provide some `DKIM` and `SPIF` details to add to your `MX records` for the domain you'll be wanting the emails to come from. This will help prevent the emails being treated as spam by the recipient.


#### Can I use an iframe for BidJS?

Whilst BidJS may work with an iframe, it is not something that we are able to offer support with.

:::warning
We cannot support you with any issues you encounter while using an iframe
:::


### Common Issues


#### After installing BidJS on my site, I only see a loading spinner

**This is usually an issue with configuration.**

Please note any javascript / console errors that may be thrown, and see if any of the other issues mentioned on this page match.

Otherwise, please verify the details you've entered and refresh the page.


#### The page is loading properly, but the layout is not correct

The BidJS CSS (styles) should be isolated, and so shouldn't affect any of the layout or styling on your site. However, BidJS can be affected by your sites CSS.

Commonly used classes such as `container` are used throughout BidJS, and if you have specific styles attached to this or similar classes associated with Bootstrap, then you may have some styling conflicts.

We recommend adjusting your styles to make them as specific to your site as possible, or in a way that they will enhance the BidJS client, rather than completely override.


#### How do I make navigation / buttons appear and work on pages other than the BidJS page?

Please see our [Advanced Configuration](./configuration/advanced/modules.md#empty-module) where we explain how you can have an "empty" BidJS installation, still affecting your navigation.


#### I'm seeing an error related to non-secure pages

We do not support usage on unsecured pages (HTTP) and require a secure page (HTTPS). This is for the safety of your customers, as they will be inputting usernames and passwords.

It is worth noting that secure pages will also have advantages in terms of Search Engine Optimisation.
