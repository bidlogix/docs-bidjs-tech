---
id: installation
title: Installation
sidebar_label: Installation
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

## Your Details

**To get BidJS installed on your site, you'll first need to ensure you have the following information:**
* Your Server Name
* Your Server Region
* Your Client Identifier

We'll also be referencing `<<VERSION>>` throughout this document. The latest version and instructions on upgrading between versions can be found in our Release Notes section.

If you don't have this, or just want to try and get implementation working with demo data, you can use the following:

### Demonstration Installation Details

* **Server Name:** `hove`
* **Server Region:** `eu-west-2`
* **Client ID:** `demonstration`

At the time of writing, the current `<<VERSION>>` is `5`. Depending on your installation, you may wish to use our strict versioning. Please contact us to find out the best version for you and discover more about [Loose or Strict Versioning by clicking here](../upgrading/versioning.md).

#### How to formulate your Application URL

In addition, this installation guide will reference `<<APPLICATION_URL>>`. This should be replaced with a URL constructed of the abovementioned configuration, in the format of `//{Server Name}.{Server Region}.bidjs.com`.

E.g. with the above demonstration details, `<<APPLICATION_URL>>` would equal `//hove.eu-west-2.bidjs.com`.

:::info
If you are given access to a "staging" environment for testing unreleased features, please see the [staging documentation](../configuration/advanced/staging.md) to supplement the above details
:::

## Step 1: Dependencies and Configuration

The following will need to be added to the `<head>` of the page(s) you want BidJS installed on.

**This is your configuration for the BidJS application.**

Following the instructions in this section will enable the basic installation. Please see [options](../configuration/options.md) documentation for further customisation of features.

For the below configuration, please replace any values in the format of `<<X>>` with [your details](#your-details) and [application URL](#how-to-formulate-your-application-url).

:::info
For WordPress installation, add this code to the Header section. N.B. You may need to replace existing header code
:::

<Tabs
  defaultValue="full"
  groupId='installation-type'
  values={[
    {label: 'My website doesn\'t have Bootstrap', value: 'full'},
    {label: 'My website has Bootstrap', value: 'minimal'}
  ]}>
<TabItem value="full">

```html
<script>
  window.bidjs = {  
    config: {
      clientId: '<<YOUR_CLIENT_ID>>', // e.g. demonstration
      googleMapsApiKey: '<<YOUR_KEY>>',
      region: '<<YOUR_SERVER_REGION>>', // e.g. eu-west-2
      server: '<<YOUR_SERVER_NAME>>' // e.g. hove
    }
  }
</script>
<script defer src="https://static.bidjs.com/<<VERSION>>/bootstrap3/js/bidjs.min.js"></script>
<noscript><link href="https://static.bidjs.com/<<VERSION>>/bootstrap3/css/bidjs--full.min.css" rel="stylesheet" type="text/css"></noscript>
<link rel="preload" href="https://static.bidjs.com/<<VERSION>>/bootstrap3/css/bidjs--full.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
```

</TabItem>
  <TabItem value="minimal">

```html
<script>
  window.bidjs = {  
    config: {
      clientId: '<<YOUR_CLIENT_ID>>', // e.g. demonstration
      googleMapsApiKey: '<<YOUR_KEY>>',
      region: '<<YOUR_SERVER_REGION>>', // e.g. eu-west-2
      server: '<<YOUR_SERVER_NAME>>' // e.g. hove
    }
  }
</script>
<script defer src="https://static.bidjs.com/<<VERSION>>/bootstrap3/js/bidjs.min.js"></script>
<noscript><link href="https://static.bidjs.com/<<VERSION>>/bootstrap3/css/bidjs.min.css" rel="stylesheet" type="text/css"></noscript>
<link rel="preload" href="https://static.bidjs.com/<<VERSION>>/bootstrap3/css/bidjs.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
```

  </TabItem>
</Tabs>

:::caution
Please make sure to change all of the parts in CAPITAL LETTERS within the script sources, and the `config` (not `modules`) object to match your details
:::

## Step 2: HTML Container

You should place the following HTML within your `<body>` tags, where you want the BidJS application to show on your page.

```html
<div class="bidlogix-app bidjs-app">
  <div id="bidlogix-modal"></div>
</div>

<div class="container bidlogix-app">
  <div id="bidjs" class="bidjs-app"></div>
</div>
```

:::info
For WordPress installation use a code snippet and enter the above snippet into the Body section
:::

## Success & Next Steps
With the above steps followed, a basic implementation of BidJS should now be on your site

There are numerous options and configurations, to further customise BidJS. It's recommended to browse these on our Configuration section
