---
id: wordpress
title: Wordpress
sidebar_label: Wordpress
---

## Required Plugins

Wordpress doesn't give the ability to adjust HTML and scripts, by default.

However, by installing plugins, this can be achieved.

:::caution
Please note, we are not affiliated with these plugins, and offer no guarantee for them. You may find other plugins better suit your personal requirements
:::

### Ability to adjust Header / Footer code

We've found the [head-footer-code](https://wordpress.org/plugins/header-and-footer-scripts/) plugin to work well for us.

Please use plugin this to insert the [Dependencies](./installation.md#step-1-dependencies-and-configuration) into the header.

### Ability to add HTML to the body

We've found the [XYZ HTML Snippet Generator Plugin](https://wordpress.org/plugins/insert-html-snippet/) plugin to work well, though there are alternatives.

Please use this to insert the [HTML containers](installation.md#step-2-html-container) into the body.
