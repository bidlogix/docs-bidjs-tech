module.exports = {
  developerSidebar: [
    {
      type: 'doc',
      id: 'developer/introduction'
    },
    {
      type: 'category',
      label: 'Getting Started',
      items: [
        'developer/gettingStarted/installation',
        'developer/gettingStarted/wordpress'
      ]
    },
    {
      type: 'category',
      label: 'Configuration',
      items: [
        'developer/configuration/navigation',
        'developer/configuration/options',
        {
          type: 'category',
          label: 'Advanced',
          items: [
            'developer/configuration/advanced/analytics',
            'developer/configuration/advanced/callback',
            'developer/configuration/advanced/modules',
            'developer/configuration/advanced/oauth',
            'developer/configuration/advanced/search',
            'developer/configuration/advanced/staging',
            'developer/configuration/advanced/translations'
          ]
        }
      ]
    },
    {
      type: 'category',
      label: 'Upgrading',
      items: [
        'developer/upgrading/versioning',
        'developer/upgrading/v5.0',
        'developer/upgrading/v4.3',
        'developer/upgrading/v4.1',
        'developer/upgrading/v4.0',
        'developer/upgrading/v3.0',
        {
          type: 'link',
          label: 'Changelog',
          href: 'https://app.getbeamer.com/bidlogix/en'
        }
      ]
    },
    {
      type: 'doc',
      id: 'developer/commonIssues'
    },
    {
      type: 'link',
      label: 'Support',
      href: 'https://support.bidlogix.net' // The target URL (string).
    }
  ],
  apiSidebar: [
    'api/general info/api-general',
    'api/general info/api-security',
    'api/general info/common-data',
    'api/general info/status-codes',
    {
      type: 'category',
      label: 'API Calls',
      items: [
        {
          type: 'link',
          label: 'OpenAPI Documentation',
          href: '/apiDefinitions' // The target URL (string).
        },
        'api/create-auction',
        'api/get-auction-by-id',
        'api/get-auction-by-uuid',
        'api/auction-report',
        'api/auction-data',
        'api/auctioneer-data',
        'api/category-data',
        'api/category-report',
        'api/upload-media',
        'api/delete-images',
        'api/update-items-by-id',
        'api/update-items-by-uuid',
        'api/create-items',
        'api/update-live-items',
        'api/item-report',
        'api/item-uuid-report',
        'api/fetch-data',
        'api/get-registrants',
        'api/update-registrant-status',
        'api/get-sales-values',
        'api/user-report',
        'api/get-uuid',
        'api/get-vendors',
        'api/vendor-data'
      ]
    },
    'api/general info/api-errors'
  ],
  websocketSidebar: [
    {
      type: 'doc',
      id: 'websocket/introduction'
    },
    {
      type: 'doc',
      id: 'websocket/messages'
    },
    {
      type: 'doc',
      id: 'websocket/models'
    }
  ],
  webhookSidebar: [
    {
      type: 'doc',
      id: 'webhooks/introduction'
    },
    {
      type: 'category',
      label: 'Events',
      items: [
        'webhooks/events/auctionStatus',
        'webhooks/events/itemStatus',
        'webhooks/events/registration'
      ]
    }
  ]
}
