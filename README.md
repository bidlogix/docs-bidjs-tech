# Website

This website is built using [Docusaurus 2](https://v2.docusaurus.io/), a modern static website generator.

## Installation

```bash
yarn
```

## Local Development

```bash
yarn start
```

This command starts a local development server and open up a browser window. Most changes are reflected live without having to restart the server.

## Build

```bash
yarn build
```

This command generates static content into the `build` directory and can be served using any static contents hosting service.

## Deployment

```bash
GIT_USER=<Your GitHub username> USE_SSH=true yarn deploy
```

If you are using GitHub pages for hosting, this command is a convenient way to build the website and push to the `gh-pages` branch.

## Testing new Swagger docs

You are able to run this repo locally, to test updates to the API docs.

**Perform the following steps:**

* run the repo as per instructions above (Local Development)
* Find the bucket URL at the end of the logs after running ms-proxy pipeline deploy swagger step. E.g. `s3://bdx-api-swagger-template-staging-eu-west-2/bid-1936/swagger.yaml`
* Update `src/components/swaggerDoc/swaggerDoc.jsx` and point the above mentioned URL to your new docs.
* Chrome will block localhost CORs requests, so instead use `http://lvh.me:3000/api`

