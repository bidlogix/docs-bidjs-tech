const webpack = require('webpack')
const branch = process.env.REACT_APP_BRANCH

module.exports = {
  title: 'BidJS Documentation',
  tagline: 'Industrial Strength Auction Software',
  url: `https://${branch === 'master' ? '' : 'staging.'}docs.bidjs.com`,
  baseUrl: (!branch || branch === 'master') ? '/' : `/${branch}/`,
  favicon: 'img/favicon.ico',
  organizationName: 'bidlogix',
  projectName: 'BidJS',
  themeConfig: {
    navbar: {
      title: 'Docs',
      logo: {
        alt: 'BidJS Logo',
        src: 'img/logo.png',
      },
      items: [
        {
          to: 'https://bidjs.com',
          label: '« to bidJS.com',
          position: 'left',
        },
        {
          to: '/',
          activeBaseRegex: '/(?!(websocket|api))',
          label: 'BidJS Plugin',
          position: 'left',
        },
        {
          to: 'api',
          label: 'API Docs',
          position: 'left',
          activeBaseRegex: '/(api)'
        },
        {
          to: 'websocket',
          label: 'Websocket Docs',
          position: 'left',
          activeBaseRegex: '/websocket'
        },
        {
          to: 'webhooks',
          label: 'Webhooks Docs',
          position: 'left',
          activeBaseRegex: '/(webhooks | webhooks/introduction)'
        }
      ]
    },
    footer: {
      style: 'dark',
      copyright: `Copyright © ${new Date().getFullYear()} Bidlogix. Built with Docusaurus.`,
    },
    algolia: {
      apiKey: '523ba8e41f48e4be583a2efa82acf3b5',
      appId: 'GL2U81ECDA',
      indexName: 'docs_bidjs',
      contextualSearch: false
    },
  },
  plugins: [
    function buffer(context, options) {
      return {
        name: "buffer",
        configureWebpack(config, isServer, utils) {
          return {
            plugins: [...config.plugins, (new webpack.ProvidePlugin({
              Buffer: ['buffer', 'Buffer'],
            }))]
          }
        }
      };
    }
  ],
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          // It is recommended to set document id as docs home page (`docs/` path).
          routeBasePath: '/',
          sidebarPath: require.resolve('./sidebars.js')
        },
        gtag: {
          // You can also use your "G-" Measurement ID here.
          trackingID: 'GTM-PSDGBCM'
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ]
};
