import React from 'react'
import PropTypes from 'prop-types'
import loadable from '@loadable/component'
const SwaggerUI = loadable(() => import('swagger-ui-react'))
loadable(() => import('swagger-ui-react/swagger-ui.css'))

const SwaggerDoc = (props) => (
  <SwaggerUI
    docExpansion='list'
    url={`https://s3.${props.region}.amazonaws.com/bdx-api-swagger-template-${props.region}/master/swagger.yaml`}
  />
)

SwaggerDoc.propTypes = {
  region: PropTypes.string.isRequired
}

SwaggerDoc.defaultProps = {}

export default SwaggerDoc
