import React, { Component } from 'react';
import Layout from '@theme/Layout';
import SwaggerDoc from '../components/swaggerDoc'

const REGIONS = ['eu-west-2', 'eu-central-1', 'ap-southeast-2']

class APIDocs extends Component {
  constructor () {
    super()

    this.state = {
      region: 'eu-west-2'
    }
  }

  render () {
    return (
      <Layout
        title={`API Docs`}
        description={`Open API Reference Docs for the API`}
      >
        <div className='api__region'>
          <label className='api__region--label'>Region</label>
          <select className='api__region--input' onChange={(e) => {
            this.setState({
              region: e.target.value
            })
          }}>
            {REGIONS.map(region => (
              <option key={region} value={region}>{region}</option>
            ))}
          </select>
        </div>
        <SwaggerDoc region={this.state.region} />
      </Layout>
    );
  }
}

export default APIDocs;
